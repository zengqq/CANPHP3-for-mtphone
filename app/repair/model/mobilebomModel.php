<?php
namespace app\repair\model;
/* *
 * 维修使用的BOM
 * */ 
class mobilebomModel extends \app\base\model\AdminModel{
    protected $table = 'phone_mobile_bom';
   

    //维修手机使用了那些配件
    public function select_lists($condition = array()){
        return $this->table('phone_bom as A')
              ->join('{pre}phone_mobile_bom as B ON A.id = B.bom_id')
              ->field('A.*,A.id as bom_id,B.phone_id,B.id as id')
              ->where($condition)
              ->order('A.id desc')
              ->select();
    }

    //添加BOM
    public function mobilebom_insert($data = array()){
        return $this->data($data)->insert();
    }
}