<?php
namespace app\repair\model;
/* *
 * 收件入库
 * */
class mobileModel extends \app\base\model\AdminModel{
    protected $table = 'phone_mobile';

    //包括列表
    public function select_package_lists($condition = array()){
        return $this->table('phone_mobile_package as A')
              ->join('{pre}phone_supplier as B ON A.supplier_id = B.id')
              ->field('A.*,B.name as supplier_name')
              ->where($condition)
              ->order('A.id desc')
              ->select();
    }

    //包括下面的手机列表
    public function select_package_iphone_lists($condition = array()){
        return $this->table('phone_mobile as A')
              ->join('{pre}phone_manufacturer as B ON A.manufacturer_id = B.id','left')
              ->join('{pre}phone_producttype as C ON A.producttype_id = C.id','left')
              ->join('{pre}phone_supplier as D ON A.supplier_id = D.id','left')
              ->field('A.*,B.name as manufacturer_name,C.name as producttype_name,D.name as supplier_name')
              ->where($condition)
              ->order('A.id desc')
              ->select();
    }


     //门店号供应商
    public function select_supplier_lists($condition = array()){
        return $this->table('phone_mobile as A')
              ->join('{pre}phone_supplier as B ON A.supplier_id = B.id')
              ->field('A.*,B.name as supplier_name')
              ->where($condition)
              ->order('A.id desc')
              ->select();
    }
    
     //删除上传文件
    public function DeleteFile($path){
        if($path){
            $ImagesPath = ROOT_PATH.$path;
            if(file_exists($ImagesPath)){
                if(!unlink($ImagesPath)){
                    return FALSE;
                }else{
                    $this->delete($condition);
                    return TRUE;
                }
            }else{
                $this->delete($condition);
                return TRUE;
            } 
        }
        return FALSE;
    } 
}