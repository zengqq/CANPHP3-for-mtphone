<?php
namespace app\repair\controller;
use framework\ext\Form;
/* *
 * 维修报表
 * */ 
class TestingController extends \app\base\controller\AdminController{

    private $mobile;
    private $faulttype; 
    private $mobilebom;
    private $bom;
    private $supplier;      //门店号管理
    private $producttype;   //产品类型
    private $place;         //库位
    private $manufacturer;  //厂商

    public function __construct() {
        parent::__construct();
        $this->mobile       = obj('mobile');
        $this->mobilebom    = obj('mobilebom');
        $this->faulttype    = obj('warehouse/faulttype');
        $this->bom          = obj('warehouse/bom');
        $this->producttype  = obj('warehouse/producttype');
        $this->supplier     = obj('warehouse/supplier');
        $this->place        = obj('warehouse/place');
        $this->manufacturer = obj('warehouse/manufacturer');
    } 

    //列表
    public function index(){
        $condition = array();
        $condition['status'] = 3;
        if($this->isPost()){
            $this->new_imei   = $this->request('post.new_imei');
            $this->imei       = $this->request('post.imei');
            $this->start_time = $this->request('post.start_time');
            $this->end_time   = $this->request('post.end_time');
            $this->is_urgent   = $this->request('post.is_urgent');
        }else{
            $this->new_imei   = $this->request('get.new_imei');
            $this->imei       = $this->request('get.imei');
            $this->start_time = $this->request('get.start_time');
            $this->end_time   = $this->request('get.end_time');
            $this->is_urgent  = $this->request('get.is_urgent');
        }
        if($this->new_imei){
            $condition['new_imei'] = $this->new_imei;
            $search['new_imei']    = $this->new_imei;
        } 
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei']    = $this->imei;
        } 
        if($this->start_time){
            $condition['1']       =  "service_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2']     = "service_time <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }
        if($this->is_urgent){
            $condition['is_urgent'] = $this->is_urgent == 1 ? 1 : 0;
            $search['is_urgent']    = $this->is_urgent;
        } 
        $page        = $this->request('get.page',intval);
        $this->list  = $this->mobile->pager($page,20)->select_package_iphone_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        $this->display();
    }


    //编辑
    public function edit(){
        if($this->isPost()){
            $data = array();
            $form = new Form('post',array('id'));
            $data = $form->getVal();
            $status = $this->request('post.status',intval);;
            if($status){
                $data['status'] = 4;
            }else{
                $data['status'] = 2;
            }
            $data['testing_time'] = time();
            $data['testing_admin_name'] = $this->userInfo['username'];
            $data['testing_admin_id'] = $this->userInfo['id'];
            $data['is_testing'] = 1;
            $id = $this->request('post.id');
            $condition['id'] = $id;
            $rel = $this->mobile->where($condition)->data($data)->update();
            if($rel){
                $testing['moblie_id'] = $id;
                $testing['testing_time'] = time();
                $testing['testing_admin_name'] = $this->userInfo['username'];
                $testing['testing_admin_id'] = $this->userInfo['id'];
                $testing['is_pass'] = $status ? 1 : 0;
                $testing['testing_text'] = $form->getVal('testing_text');;
                obj('testing_logs')->data($testing)->insert();
                $this->jsonMsg('检验通过,请安排发货,',1,url('testing/index'));
            }else{
                $this->jsonMsg('检验失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $this->info = $this->mobile->where(array('id' =>$this->id))->find();
            //产品名称
            $producttype_info  = $this->producttype->select();
            foreach ($producttype_info as $key => $value) {
                $producttype_lists[$value['id']] = $value['name'];
            }
            $this->producttype_lists =   $producttype_lists;
            $this->bom_list = $this->mobilebom->select_lists(array('phone_id' =>$this->id));
            $this->display();
        }
    } 

    //质检历史
    public function logs(){
        $is_excel = $this->request('get.is_excel');
        $condition = array();
        //$condition[0] = 'status >= 4';
        $condition['is_testing'] = 1;
        if($this->isPost()){
            $this->new_imei   = $this->request('post.new_imei');
            $this->imei       = $this->request('post.imei');
            $this->start_time = $this->request('post.start_time');
            $this->end_time   = $this->request('post.end_time');
        }else{
            $this->new_imei   = $this->request('get.new_imei');
            $this->imei       = $this->request('get.imei');
            $this->start_time = $this->request('get.start_time');
            $this->end_time   = $this->request('get.end_time');
        }
        if($this->new_imei){
            $condition['new_imei'] = $this->new_imei;
            $search['new_imei']    = $this->new_imei;
        } 
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei']    = $this->imei;
        } 
        if($this->start_time){
            $condition['1'] =  "testing_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "testing_time <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }
        $page = $this->request('get.page',intval);
        $this->list = $this->mobile->pager($page,20)->select_package_iphone_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        if($is_excel){
            $this->header_excel('Testing_');
            $this->layout ='';
            $this->display('app/repair/view/testing/excel');
        }else{
            $search['is_excel'] = 1;
            $this->assign('search',$search);
            $this->display();
        }
    }
    
    //查看详情
    public function reads(){
        $this->id = $this->request('get.id',intval);
        $info = $this->mobile->where(array('id' =>$this->id))->find();
        if($info){
            $producttype = $this->producttype->where(['id' => $info['producttype_id']])->find();
            $info['producttype_name'] = $producttype['name'];
            $manufacturer = $this->manufacturer->where(['id' => $info['manufacturer_id']])->find();
            $info['manufacturer_name'] = $manufacturer['name'];
            $supplier = $this->supplier->where(['id' => $info['supplier_id']])->find();
            $info['supplier_name'] = $supplier['name'];
        }
        $this->info = $info;
        $this->list = $this->mobilebom->select_lists(array('phone_id' =>$this->id));
        $this->testing_logs = obj('testing_logs')->where(['moblie_id' => $this->id])->select();
        $this->display();
    }

    //查看详情
    public function istesting(){
        $this->id = $this->request('get.id',intval);
        $this->testing_logs = obj('testing_logs')->where(['moblie_id' => $this->id])->select();
        $this->display();
    }

    //编辑
    public function excel(){
        $this->header_excel('testing_');
        $this->id = $this->request('get.id',intval);
        $this->info = $this->mobile->where(array('id' =>$this->id))->find();
        $this->list = $this->mobilebom->where(array('phone_id' =>$this->id))->select_lists(array('phone_id' =>$this->id));
        if($this->info['supplier_id']){
            $this->supplier_info = $this->supplier->where(array('id' =>$this->info['supplier_id']))->find();
        }
        $this->producttype_lists = $this->producttype->select();
        $this->display();
    }         
}