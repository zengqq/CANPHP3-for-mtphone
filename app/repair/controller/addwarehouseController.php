<?php
namespace app\repair\controller;
use framework\ext\Form;
/* * 
 * 确认入库
 * */
class AddwarehouseController extends \app\base\controller\AdminController{
    
    private $mobile;
    private $producttype;  //产品类型
    private $place;   //库位
    private $manufacturer;  //厂商
    private $unicom;   //运营商
    private $supplier;   //运营商

    public function __construct() {
        parent::__construct();
        $this->mobile       = obj('mobile');
        $this->producttype  = obj('warehouse/producttype');
        $this->place        = obj('warehouse/place');
        $this->manufacturer = obj('warehouse/manufacturer');
        $this->unicom       = obj('warehouse/unicom');
        $this->supplier     = obj('warehouse/supplier');
    } 

    //列表
    public function index(){
        $condition = array();
        $condition['status']     = 0;  //待入库
        $condition['package_id'] = 0;  //包裹ID
        $is_search = 0;
        if($this->isPost()){
            $this->imei            = $this->request('post.imei');
            $is_search             = 1;
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($is_search){
            $page        = $this->request('get.page',intval);
            $this->list  = $this->mobile->pager($page,20)->select_package_iphone_lists($condition);
            $this->pager = $this->getPage($this->mobile->pager,$search);
        }
        $this->producttype_lists  = $this->producttype->select();
        $this->manufacturer_lists = $this->manufacturer->select();
        $this->unicom_lists       = $this->unicom->select();
        $this->supplier_lists     = $this->supplier->select();
        $this->display();
    }

    //列表
    public function logs(){
        $is_excel = $this->request('get.is_excel');
        //搜索条件
        $condition = array();
        if($this->isPost()){
            $this->imei            = $this->request('post.imei');
            $this->supplier_id     = $this->request('post.supplier_id');
            $this->number          = $this->request('post.number');
            $this->ems_sn          = $this->request('post.ems_sn');
            $this->producttype_id  = $this->request('post.producttype_id');
            $this->manufacturer_id = $this->request('post.manufacturer_id');
            $this->start_time      = $this->request('post.start_time');
            $this->end_time        = $this->request('post.end_time');
        }else{
            $this->imei            = $this->request('get.imei');
            $this->number          = $this->request('get.number');
            $this->ems_sn          = $this->request('get.ems_sn');
            $this->supplier_id     = $this->request('get.supplier_id');
            $this->producttype_id  = $this->request('get.producttype_id');
            $this->manufacturer_id = $this->request('get.manufacturer_id');
            $this->start_time      = $this->request('get.start_time');
            $this->end_time        = $this->request('get.end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['number'] = $this->number;
        }
        if($this->ems_sn){
            $condition['ems_sn'] = $this->ems_sn;
            $search['ems_sn'] = $this->ems_sn;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
            $search['manufacturer_id'] = $this->manufacturer_id;
        }
        if($this->start_time){
            $condition['1'] =  "diacrisis_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "diacrisis_time <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }  
        //列表
        $condition[0] = 'status >= 1';
        $page        = $this->request('get.page',intval);
        $this->list  = $this->mobile->pager($page,20)->select_package_iphone_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        if($is_excel){
            header("Content-type:application/octet-stream");
            header("Accept-Ranges:bytes");
            header("Content-type:application/vnd.ms-excel");  
            header("Content-Disposition:attachment;filename=".date('Ymdhis').".xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            $this->layout ='';
            $this->display('app/repair/view/addwarehouse/excel');
        }else{
            $this->place_lists        = $this->place->select();
            $this->manufacturer_lists = $this->manufacturer->select();
            $this->unicom_lists       = $this->unicom->select();
            $this->supplier_lists     = $this->supplier->select();
            $this->producttype_lists  = $this->producttype->select();
            $search['is_excel'] = 1;
            $this->assign('search',$search);
            $this->display();
        }
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $data = array();
            $form = new Form('post',array('id','shop_time'));
            $data = $form->getVal();
            $id = $this->request('post.id');
            $condition['id'] = $id;
            $info = $this->mobile->where($condition)->find();
            $data['status'] = 1;
            $data['product_count'] = 1;
            $data['addwarehouse_admin_id']   = $this->userInfo['id'];
            $data['addwarehouse_admin_name'] = $this->userInfo['username'];
            $data['addwarehouse_time'] = time();
            $data['shop_time'] = strtotime($form->getVal('shop_time'));
            if($data['shop_time'] >= $info['accept_time']){
                $this->jsonMsg('购买日期不能小于门店受理日期');
            }
            $overtime = $info['accept_time'] - $data['shop_time'];  //门店受理日期-购买时间
            if($overtime >= 31104000){
                $data['overtime'] = '否';  //过保
            }else{
                $data['overtime'] = '是';  //包内
            }
            $is_urgent = time() - $info['accept_time'];  //系统当前时间-门店受理日期
            if($is_urgent >= 172800){  //大于2天就紧急
                $data['is_urgent'] = 0;
            }else{
                $data['is_urgent'] = 1;
            }
            $rel = $this->mobile->where($condition)->data($data)->update();
            if($rel){
                //S - 进入仓库
                $warehouse_data['producttype_id']  = $info['producttype_id'];
                $warehouse_data['manufacturer_id'] = $info['manufacturer_id'];
                $warehouse_data['place_id']        = $form->getVal('place_id');
                $warehouse_data['code']            = $info['number'];
                $warehouse_data['amount']          = $data['product_count'];
                $warehouse_data['types']           = 1;
                //产品
                if($info['manufacturer_id']){
                    $manufacturer_info  = $this->manufacturer->where(['id' => $info['manufacturer_id']])->field('id,name')->find();
                }
                $warehouse_data['manufacturer'] = $manufacturer_info['name'];
                //产品名称
                if($info['producttype_id']){
                    $producttype_info  = $this->producttype->where(['id' => $info['producttype_id']])->field('id,name')->find();
                }
                $warehouse_data['product']   = $producttype_info['name'];
                $warehouse_data['name']      = $producttype_info['name'];
                $warehouse_data['parameter'] = $producttype_info['name'];
                 //库位
                if($data['place_id']){
                    $place_info  = $this->place->where(['id' => $data['place_id']])->field('id,name')->find();
                }
                $warehouse_data['place'] = $place_info['name'];
                $warehouse_data['warning'] = 1;
                $warehouse_data['uptime'] = time();
                $warehouse_rel = obj('warehouse/bom')->data($warehouse_data)->insert();
                //增加日志
                if($warehouse_rel){
                    $bomlogs['types'] = 1;
                    $bomlogs['uptime'] = time();
                    $bomlogs['bom_id'] = $warehouse_rel;
                    $bomlogs['imei']   = $info['imei'];;
                    $bomlogs['amount'] = $data['product_count'];;
                    $bomlogs['admin_name'] = $this->userInfo['username'];
                    $bomlogs['place_id'] = $form->getVal('place_id');;
                    obj('warehouse/bomlogs')->data($bomlogs)->insert();
                }
                //E - 进入仓库
                $this->jsonMsg('入库成功',1,url('addwarehouse/index'));
            }else{
                $this->jsonMsg('入库失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $condition['id'] = $this->id;
            $info = $this->mobile->where($condition)->find();
            if($info){
                $producttype = $this->producttype->where(['id' => $info['producttype_id']])->find();
                $info['producttype_name'] = $producttype['name'];
                $manufacturer = $this->manufacturer->where(['id' => $info['manufacturer_id']])->find();
                $info['manufacturer_name'] = $manufacturer['name'];
                $supplier = $this->supplier->where(['id' => $info['supplier_id']])->find();
                $info['supplier_name'] = $supplier['name'];
            }
            $this->place_lists = $this->place->select();
            $this->assign('info',$info);
            $this->display();
        }
    }
}