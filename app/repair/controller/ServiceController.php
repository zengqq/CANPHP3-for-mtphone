<?php
namespace app\repair\controller;
use framework\ext\Form;
/* * 
 * 维修管理
 * */
class ServiceController extends \app\base\controller\AdminController{
    private $mobile = null;
    private $faulttype = null;  //故障类别
    private $mobilebom = null;  //故障类别
    private $bom = null;  //故障类别
    private $producttype = null;  //产品类型
    private $place = null;   //库位
    private $manufacturer = null;  //厂商
    private $unicom = null;   //运营商
    private $supplier = null;   //门店
    public function __construct() {
        parent::__construct();
        $this->mobile = obj('mobile');
        $this->mobilebom = obj('mobilebom');
        $this->faulttype = obj('warehouse/faulttype');
        $this->bom = obj('warehouse/bom');
        $this->producttype = obj('warehouse/producttype');
        $this->place = obj('warehouse/place');
        $this->manufacturer = obj('warehouse/manufacturer');
        $this->unicom = obj('warehouse/unicom');
        $this->supplier = obj('warehouse/supplier');
    } 

    //列表
    public function index(){
        $condition = array();
        $condition['status'] = 2;
        $is_search = 0;
        if($this->isPost()){
            $this->imei      = $this->request('post.imei') ;
            $this->is_urgent = $this->request('post.is_urgent');
        }
        $this->imei = $this->isPost() ? $this->request('post.imei') : $this->request('get.imei');
        $this->is_urgent = $this->isPost() ? $this->request('post.is_urgent') : $this->request('get.is_urgent');
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei']    = $this->imei;
            $is_search         = 1;
        }
        if($this->is_urgent){
            $condition['is_urgent'] = $this->is_urgent == 1 ? 1 : 0;
            $search['is_urgent']    = $this->is_urgent;
            $is_search         = 1;
        }         
        if($is_search){
            $this->list = $this->mobile->select_package_iphone_lists($condition);
        }
        $this->display();
    }

    //查看详情
    public function reads(){
        $this->id = $this->request('get.id',intval);
        $info = $this->mobile->where(array('id' =>$this->id))->find();
        if($info){
            $producttype = $this->producttype->where(['id' => $info['producttype_id']])->find();
            $info['producttype_name'] = $producttype['name'];
            $manufacturer = $this->manufacturer->where(['id' => $info['manufacturer_id']])->find();
            $info['manufacturer_name'] = $manufacturer['name'];
            $supplier = $this->supplier->where(['id' => $info['supplier_id']])->find();
            $info['supplier_name'] = $supplier['name'];
        }
        $this->info = $info;
        $this->list = $this->mobilebom->where(array('phone_id' =>$this->id))->select_lists(array('phone_id' =>$this->id));
        $this->display();
    }

    //列表
    public function logs(){
        $is_excel = $this->request('get.is_excel');
        $condition = array();
        $condition[0] =  'status >= 3';
        if($this->isPost()){
            $this->new_imei = $this->request('post.new_imei');
            $this->imei = $this->request('post.imei');
            $this->number = $this->request('post.number');
            $this->overtime = $this->request('post.overtime');
            $this->start_time = $this->request('post.start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->new_imei = $this->request('get.new_imei');
            $this->imei = $this->request('get.imei');
            $this->number = $this->request('get.number');
            $this->overtime = $this->request('get.overtime');
            $this->start_time = $this->request('get.start_time');
            $this->end_time = $this->request('get.end_time');
        }
        if($this->new_imei){
            $condition['new_imei'] = $this->new_imei;
            $search['new_imei']    = $this->new_imei;
        }        
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei']    = $this->imei;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['name']      = $this->number;
        }
        if($this->overtime){
            $condition['overtime'] = $this->overtime;
            $search['overtime']    = $this->overtime;
        } 
        if($this->start_time){
            $condition['1'] =  "service_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "service_time <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }    
        $page = $this->request('get.page',intval);
        $this->list = $this->mobile->pager($page,20)->select_package_iphone_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        if($is_excel){
            $this->header_excel('service_');
            $this->layout ='';
            $this->display('app/repair/view/service/excel');
        }else{
            $search['is_excel'] = 1;
            $this->assign('search',$search);
            $this->display();
        }
    }   

    //过保
    public function overtime(){
        if($this->isPost()){
            $form = new Form('post',array('id'));
            $id          = intval($form->getVal('id'));
            $is_overtime = intval($form->getVal('is_overtime'));
            $is_rmb      = $form->getVal('is_rmb');
            $shop_time   = strtotime($form->getVal('shop_time'));
            $condition['id'] = $id;
            if($is_overtime == 1){  //重新判断包内
                $info = $this->mobile->where($condition)->field('accept_time')->find();
                $overtime = $info['accept_time'] - $shop_time;
                if($overtime >= 31104000){
                    $this->jsonMsg('系统判断输入的激活日期还是保外,请重新操作',0,url('service/overtime',['id'=>$id]));
                }else{
                    $data['overtime']  = '是';  //包内
                    $data['shop_time'] = $shop_time;  //新的购机日期
                }
            }else{
                if($is_rmb == 1){
                    $data['is_rmb'] = 1;  //付费维修
                }else{
                    $data['status'] = 5;  //直接出库
                }
            }
            $rel = $this->mobile->where($condition)->data($data)->update();
            if($rel){
                if($data['status'] == 5){
                    $this->jsonMsg('成功设置当前设备,不维修。',1,url('service/index'));
                }else{
                    $this->jsonMsg('重新激活成功,确认进入维修流程',1,url('service/index'));
                }
            }else{
                $this->jsonMsg('重新激活失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $this->info = $this->mobile->where(array('id' =>$this->id))->find();
            $this->display(); 
        }
    }

    //故障再次判断
    public function reset(){
        if($this->isPost()){
            $data = array();
            $form = new Form('post',array('id'));
            $data['failure_cause'] = $form->getVal('failure_cause');
            $condition['id'] = $this->request('post.id');
            $rel = $this->mobile->where($condition)->data($data)->update();
            if($rel){
                $this->jsonMsg('修改实际故障成功,下一步选择维修物料',1,url('service/edit',['id'=>$this->request('post.id')]));
            }else{
                $this->jsonMsg('修改实际故障失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $info = $this->mobile->where(array('id' =>$this->id))->find();
            if($info['overtime'] == '否' && $info['is_rmb'] == '0'){
                $this->redirect(url('service/overtime',['id'=>$this->id]));
            }
            if($info){
                $producttype = $this->producttype->where(['id' => $info['producttype_id']])->find();
                $info['producttype_name'] = $producttype['name'];
                $manufacturer = $this->manufacturer->where(['id' => $info['manufacturer_id']])->find();
                $info['manufacturer_name'] = $manufacturer['name'];
                $supplier = $this->supplier->where(['id' => $info['supplier_id']])->find();
                $info['supplier_name'] = $supplier['name'];
            }
            $this->info = $info;
            $this->display();
        }
    }

    //编辑
    public function edit(){
        $is_excel = $this->request('get.is_excel');
        $print = $this->request('get.print');
        $this->id = $this->request('get.id',intval);
        $info = $this->mobile->where(array('id' =>$this->id))->find();
        if($info['overtime'] == '否' && $info['is_rmb'] == '0'){
            $this->redirect(url('service/overtime',['id'=>$this->id]));
        }
        if($info){
            $producttype = $this->producttype->where(['id' => $info['producttype_id']])->find();
            $info['producttype_name'] = $producttype['name'];
            $manufacturer = $this->manufacturer->where(['id' => $info['manufacturer_id']])->find();
            $info['manufacturer_name'] = $manufacturer['name'];
            $supplier = $this->supplier->where(['id' => $info['supplier_id']])->find();
            $info['supplier_name'] = $supplier['name'];
        }
        $this->info = $info;
        $this->list = $this->mobilebom->where(array('phone_id' =>$this->id))->select_lists(array('phone_id' =>$this->id));
        if($is_excel){
            header("Content-type:application/octet-stream");
            header("Accept-Ranges:bytes");
            header("Content-type:application/vnd.ms-excel");  
            header("Content-Disposition:attachment;filename=".date('Ymdhis').".xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            $this->layout ='';
            $this->display('app/repair/view/service/edit_excel');
        }else{
            if($print){
                $this->display('app/repair/view/service/edit_print');
            }else{
                $this->display();
            } 
        } 
    }

    //编辑
    public function bom(){
        if($this->isPost()){
            $form = new Form('post',array('id','faulttype','old_imei','producttype_name','manufacturer_name','unicom_name'));
            $data = $form->getVal();
            $id        = intval($form->getVal('id'));
            $faulttype = $form->getVal('faulttype');
            if(is_array($faulttype)){
                if($faulttype){
                    foreach ($faulttype as $key => $value) {
                        $faulttype_id[] = $key;
                        $str[] = implode(',',$value);
                    }
                    if(is_array($faulttype_id)){
                        $data['faulttype_id'] = implode(',',$faulttype_id);
                    }
                    if(is_array($str)){
                        $data['faulttype'] = implode(',',$str);
                    }
                }     
            }else{
                $this->jsonMsg('故障类型必选,请选择您的故障类型!');
            }
            $is_news_phone = intval($form->getVal('is_news_phone'));
            $new_imei = $form->getVal('new_imei');
            if($is_news_phone == 1){
                if(empty($new_imei)){
                    $this->jsonMsg('换新必须填写新机IMEI码!');
                }
            }
            $data['service_time'] = time();
            $data['service_admin_name'] = $this->userInfo['username'];
            $data['service_admin_id'] = $this->userInfo['id'];
            $data['status'] = 3;
            $condition['id'] = $id;
            $rel = $this->mobile->where($condition)->data($data)->update();
            if($rel){
                //把IMEI号填写到IMEI库
                $imei['new_imei']          = $form->getVal('new_imei');
                $imei['old_imei']          = $form->getVal('old_imei');
                $imei['producttype_name']  = $form->getVal('producttype_name');
                $imei['manufacturer_name'] = $form->getVal('manufacturer_name');
                $imei['unicom_name']       = $form->getVal('unicom_name');
                obj('warehouse/imei')->add_imei($imei);
                $this->jsonMsg('成功提交维修单,待质量审核',1,url('service/index'));

            }else{
                $this->jsonMsg('维修单提交失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $info = $this->mobile->where(array('id' =>$this->id))->find();
            if($info){
                $producttype = $this->producttype->where(['id' => $info['producttype_id']])->find();
                $info['producttype_name'] = $producttype['name'];
                $manufacturer = $this->manufacturer->where(['id' => $info['manufacturer_id']])->find();
                $info['manufacturer_name'] = $manufacturer['name'];
                $unicom = $this->unicom->where(['id' => $info['unicom_id']])->find();
                $info['unicom_name'] = $unicom['name'];
            }
            $this->info = $info;
            $this->faulttype_id = array();
            if($this->info['faulttype']){
                $this->faulttype_id = explode(',',$this->info['faulttype']);
            } 
            $this->faulttype_lists = $this->faulttype->select_all();
            $this->display();
        }
    }

    //列表
    public function add_bom(){
        $this->id = $this->request('get.id',intval);
        $page = $this->request('get.page',intval);
        $condition['types'] = 0;
        if($this->isPost()){
            $this->product = $this->request('post.product');
            $this->name = $this->request('post.name');
            $this->code = $this->request('post.code');
        }else{
            $this->product = $this->request('post.product');
            $this->name = $this->request('post.name');
            $this->code = $this->request('get.code');
        }
        if($this->product){
            $condition['1'] = "product LIKE '%".$this->product."%'";
            $search['product'] = $this->product;
        }
        if($this->name){
            $condition['0'] = "name LIKE '%".$this->name."%'";
            $search['name'] = $this->name;
        }
        if($this->code){
            $condition['code'] = $this->code;
            $search['code'] = $this->code;
        }
        $condition[] = 'amount > 0';
        $search['id'] = $this->id;
        $this->list = $this->bom->where($condition)->pager($page,20)->select();
        $this->pager = $this->getPage($this->bom->pager,$search);
        $this->display();
    }
    //列表
    public function add_bom_save(){
        $condition = array();
        if($this->isPost()){
            $bom_id = $this->request('post.bom_id');
            $id = $this->request('post.id');
            if (is_array($bom_id)) {
                $counts = count($bom_id['id']);
                $i = 0;
                foreach ($bom_id as $value) {
                    $result = $this->mobilebom->data(array('phone_id'=>$id,'bom_id'=>$value))->insert();
                    if($result){
                        $i++;
                        /* 
                        //屏蔽减少库存
                        $rel = $this->bom->where(array('id'=>$value))->field('amount')->find();
                        $amount = $rel['amount']-1;
                        $this->bom->data(array('amount' =>$amount))->where(array('id'=>$value))->update();
                        */
                    }
                }
                $this->jsonMsg('成功添加了'.$i.'条',1,url('service/edit',array('id'=>$id)));
            }else{
                $this->jsonMsg('你没有选择任何内容');
            }
        } 
    } 

    //列表
    public function del(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $bom_id = $this->request('get.bom_id',intval);
            $result = $this->mobilebom->where(array('id' =>$id))->delete();
            if($result){
                /*
                //屏蔽换库存
                $rel = $this->bom->where(array('id'=>$bom_id))->field('amount')->find();
                $amount = $rel['amount']+1;
                $this->bom->data(array('amount' =>$amount))->where(array('id'=>$bom_id))->update();
                */
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }
    }
}