<?php
namespace app\repair\controller;
use framework\ext\Form;
use framework\ext\Check;

/* * 
 * 收件入库
 * */
class MobileController extends \app\base\controller\AdminController{
    private $mobile = null;
    private $producttype = null;  //产品类型
    private $manufacturer = null;  //厂商
    private $unicom = null;   //运营商
    private $supplier = null;   //运营商

    public function __construct() {
        parent::__construct();
        $this->mobile = obj('mobile');
        $this->producttype = obj('warehouse/producttype');
        $this->manufacturer = obj('warehouse/manufacturer');
        $this->unicom = obj('warehouse/unicom');
        $this->supplier = obj('warehouse/supplier');
    } 

    //列表
    public function index(){
        $condition = array();
        $condition['status'] = 0;  //预入库
        $condition['package_id'] = 0;
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->ems_sn = $this->request('post.ems_sn');
            $this->mobile_name = $this->request('post.mobile_name');
            $this->producttype_id = $this->request('post.producttype_id');
            $this->manufacturer_id = $this->request('post.manufacturer_id');
            $this->unicom_id = $this->request('post.unicom_id');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->ems_sn = $this->request('get.ems_sn');
            $this->mobile_name = $this->request('get.mobile_name');
            $this->producttype_id = $this->request('get.producttype_id');
            $this->manufacturer_id = $this->request('get.manufacturer_id');
            $this->unicom_id = $this->request('get.unicom_id');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['name'] = $this->number;
        }
        if($this->ems_sn){
            $condition['ems_sn'] = $this->ems_sn;
            $search['ems_sn'] = $this->ems_sn;
        }
        if($this->mobile_name){
            $condition['0'] = "mobile_name LIKE '%".$this->mobile_name."%'";
            $search['mobile_name'] = $this->mobile_name;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
            $search['manufacturer_id'] = $this->manufacturer_id;
        }
        if($this->unicom_id){
            $condition['unicom_id'] = $this->unicom_id;
            $search['unicom_id'] = $this->unicom_id;
        }
        $page = $this->request('get.page',intval);
        $this->list = $this->mobile->pager($page,20)->select_package_iphone_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        $this->producttype_lists = $this->producttype->select();
        foreach ($this->producttype_lists as $key => $value) {
            $producttype_name[$value['id']] = $value['name'];
        }
        $this->producttype_lists =   $producttype_lists;
        $this->manufacturer_lists = $this->manufacturer->select();
        $this->unicom_lists = $this->unicom->select();
        $this->supplier_lists = $this->supplier->select();
        $this->producttype_name = $producttype_name;
        $this->display();
    }

    //编辑
    public function reads(){
        //产品名称
        $producttype_info  = $this->producttype->select();
        foreach ($producttype_info as $key => $value) {
            $producttype_lists[$value['id']] = $value['name'];
        }
        $this->producttype_lists =   $producttype_lists;
        //厂商
        $manufacturer_info = $this->manufacturer->select();
        foreach ($manufacturer_info as $key => $value) {
            $manufacturer_lists[$value['id']] = $value['name'];
        }
        $this->manufacturer_lists =   $manufacturer_lists;

        //运营商
        $unicom_info      = $this->unicom->select();
        foreach ($unicom_info as $key => $value) {
            $unicom_lists[$value['id']] = $value['name'];
        }
        $this->unicom_lists =   $unicom_lists;
        //门店号
        $supplier_info = $this->supplier->select();
        foreach ($supplier_info as $key => $value) {
            $supplier_lists[$value['id']] = $value['name'];
        }
        $this->supplier_lists =   $supplier_lists;

        $this->id   = $this->request('get.id',intval);
        $this->info = $this->mobile->where(array('id' =>$this->id))->find();
        self::qrcode($this->info['number']);
        $this->display();
    }

    //二维码
    protected function qrcode($number){
        //二维码
        vendor("phpqrcode");
        $QRcode = new \QRcode();
        //纠错级别：L、M、Q、H
        $level = 'M';
        //点的大小:1到10,用于手机端4就可以了
        $size = 8;
        $fileName = ROOT_PATH.'uploads/qrcode/qrcode.png';
        $QRcode->png($number,$fileName,$level,$size,1);
    }

    //添加
    public function add(){
        if($this->isPost()){
            $form = new Form('post',array('ems_time','ems_take_time','accept_time','product_count'));
            $data = $form->getVal();
            $data['uptime']         = time();
            $data['add_time']       = time();
            $data['add_admin_name'] = $this->userInfo['username'];
            $data['add_admin_id']   = $this->userInfo['id'];
            $data['ems_time']       = strtotime($form->getVal('ems_time'));
            $data['ems_take_time']  = strtotime($form->getVal('ems_take_time'));
            $data['accept_time']    = strtotime($form->getVal('accept_time'));
            $data['product_count'] = 1;
            if($data['accept_time'] >= time()){
                $this->jsonMsg('门店受理日期必须小于当前时间');
            }
            $data['number']         = unique_str();
            $data['product_count']  = (int)$form->getVal('product_count');
            $rel = $this->mobile->data($data)->insert();
            if($rel){
                $this->jsonMsg('设置成功',1,url('mobile/index'));
            }else{
                $this->jsonMsg('设置失败');
            }
        }else{
            $this->producttype_lists  = $this->producttype->select();
            $this->manufacturer_lists = $this->manufacturer->select();
            $this->unicom_lists       = $this->unicom->select();
            $this->supplier_lists     = $this->supplier->select();
            $this->display();
        }
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $form = new Form('post',array('id','ems_time','ems_take_time','accept_time'));
            $data = $form->getVal();
            $data['status']       = 0;
            $data['uptime']       = time();
            $data['ems_time']     = strtotime($form->getVal('ems_time'));
            $data['ems_take_time']= strtotime($form->getVal('ems_take_time'));
            $data['accept_time']  = strtotime($form->getVal('accept_time'));
            $data['product_count'] = 1;
            $id = intval($form->getVal('id'));
            $condition['id'] = $id;
            $rel = $this->mobile->where($condition)->data($data)->update();
            if($rel){
                $this->jsonMsg('设置成功',1,url('mobile/index'));
            }else{
                $this->jsonMsg('设置失败');
            }
        }else{
            $this->producttype_lists  = $this->producttype->select();
            $this->manufacturer_lists = $this->manufacturer->select();
            $this->unicom_lists       = $this->unicom->select();
            $this->supplier_lists     = $this->supplier->select();
            $this->id                 = $this->request('get.id',intval);
            $this->info               = $this->mobile->where(array('id' =>$this->id))->find();
            $this->display();
        }
    }

    //列表
    public function del(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $result = $this->mobile->where(array('id' =>$id))->delete();
            if($result){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }
    }

    //上传表单
    public function excel(){
        $this->display();
    }

    //上传模板
    public function uploads(){
        if($this->isPost()){
            $config = config('repair');
            echo $this->Files($_FILES,$config);
        }else{
            $this->frameid = $this->request('get.id');
            $this->display();
        }
    }

    //批量入库
    public function exceltodb(){
        if($this->isPost()){
            $excel = $this->request('post.mobile');;
            $msg = Check::rule(array(check::must($excel),'导入表格文件不存在或已删除,请重新上传。'));
            if(true !== $msg){
                $this->jsonMsg($msg);
            }
            //引入第三方类库
            vendor("PHPExcel");
            $PHPExcel=new \PHPExcel();  //创建PHPExcel对象，注意，不能少了\
            //vendor("PHPExcel/Reader/Excel5");
            //$PHPReader=new \PHPExcel_Reader_Excel5();
            vendor("PHPExcel/Reader/Excel2007");
            $PHPReader = new \PHPExcel_Reader_Excel2007();
            $Shared_Date = new \PHPExcel_Shared_Date();  //表给日期格式化
            $PHPExcel = $PHPReader->load(BASE_PATH.$excel);  //载入文件
            $currentSheet = $PHPExcel->getSheet(0);   //文件中第几个工作表
            $allColumn = $currentSheet->getHighestColumn(); //获取总列数
            $allRow = $currentSheet->getHighestRow();  //获取总行数
            //循环获取表中的数据，$currentRow表示当前行，从哪行开始读取数据，索引值从0开始
            for($currentRow = 4;$currentRow<=$allRow;$currentRow++){
                for($currentColumn='A';$currentColumn<=$allColumn;$currentColumn++){//从哪列开始，A表示第一列
                    $address=$currentColumn.$currentRow;//数据坐标
                    $arr[$currentRow][$currentColumn]=$currentSheet->getCell($address)->getValue();//读取到的数据，保存到数组$arr中
                }
            }
            $supplier = $this->supplier->field('id,name')->select(); //门店号
            $supplier_names = array();
            if($supplier){
                foreach ($supplier  as $key => $value) {
                    $supplier_names[$key]['name'] = $value['name'];
                    $supplier_names[$key]['id'] = $value['id'];
                } 
            }              
            $manufacturer = $this->manufacturer->field('id,name')->select(); //厂商
            $manufacturer_names = array();
            if($manufacturer){
                foreach ($manufacturer  as $key => $value) {
                    $manufacturer_names[$key]['name'] = $value['name'];
                    $manufacturer_names[$key]['id'] = $value['id'];
                } 
            }            
            $unicom = $this->unicom->field('id,name')->select(); //运营商:
            $unicom_names = array();
            if($unicom){
                foreach ($unicom  as $key => $value) {
                    $unicom_names[$key]['name'] = $value['name'];
                    $unicom_names[$key]['id'] = $value['id'];
                } 
            }
            $producttype = $this->producttype->field('id,name')->select(); //产品名称
            $producttype_names = array();
            if($producttype){
                foreach ($producttype  as $key => $value) {
                    $producttype_names[$key]['name'] = $value['name'];
                    $producttype_names[$key]['id'] = $value['id'];
                } 
            }
            if(empty($arr)){
                $this->jsonMsg('数据模块读取不成功');
            }
            $i = 3;
            foreach ($arr as $key => $value) {
                $i++;
                //门店
                $is_supplier = 0;
                if(empty($value['H'])){
                    foreach ($supplier as $k => $v) {
                        if(strcasecmp(trim($value['G']),trim($v['name'])) == 0){
                            $is_supplier = 1;
                        }                  
                    }
                }else{
                    foreach ($supplier_names as $k => $v) {
                        if(intval($value['H']) == $v['id']){
                            $is_supplier = 1;
                        }elseif(strcasecmp(trim($value['G']),trim($v['name'])) == 0){
                            $is_supplier = 1;
                        }                  
                    }
                } 
                if(!$is_supplier){
                    $this->mobile->DeleteFile($excel);
                    $this->jsonMsg('数据检测不通过,第'.$i.'行输入的门店号系统不存在');
                }  
                //厂商
                $is_manufacturer = 0;
                if($value['J']){
                    foreach ($manufacturer_names as $k => $v) {
                        if(intval($value['J']) == $v['id']){
                            $is_manufacturer = 1;
                        }elseif(strcasecmp(trim($value['I']),trim($v['name'])) == 0){
                            $is_manufacturer = 1;
                        }                  
                    }
                }else{
                    foreach ($manufacturer_names as $k => $v) {
                        if(strcasecmp(trim($value['I']),trim($v['name'])) == 0){
                            $is_manufacturer = 1;
                        }                  
                    }
                }
                if(!$is_manufacturer){
                    $this->mobile->DeleteFile($excel);
                    $this->jsonMsg('数据检测不通过,第'.$i.'行输入的厂商系统不存在');
                } 
                //运营商
                $is_unicom = 0;
                if($value['L']){
                    foreach ($unicom_names as $k => $v) {
                        if(intval($value['L']) == $v['id']){
                            $is_unicom = 1;
                        }elseif(strcasecmp(trim($value['K']),trim($v['name'])) == 0){
                            $is_unicom = 1;
                        }                  
                    }
                }else{
                    foreach ($unicom as $k => $v) {
                        if(strcasecmp(trim($value['K']),trim($v['name'])) == 0){
                            $is_unicom = 1;
                        }                  
                    }
                }
                if(!$is_unicom){
                    $this->mobile->DeleteFile($excel);
                    $this->jsonMsg('数据检测不通过,第'.$i.'行输入的运营商系统不存在');
                } 
                //产品名称
                $is_producttype = 0;
                if($value['N']){
                    foreach ($producttype_names as $k => $v) {
                        if(intval($value['N']) == $v['id']){
                            $is_producttype = 1;
                        }elseif(strcasecmp(trim($value['M']),trim($v['name'])) == 0){
                            $is_producttype = 1;
                        }                  
                    }
                }else{
                    foreach ($producttype_names as $k => $v) {
                        if(strcasecmp(trim($value['M']),trim($v['name'])) == 0){
                            $is_producttype = 1;
                        }                  
                    }
                }
                if(!$is_producttype){
                    $this->mobile->DeleteFile($excel);
                    $this->jsonMsg('数据检测不通过,第'.$i.'行输入的产品名称系统不存在');
                }
            }
            $data['add_admin_name'] = $this->userInfo['username'];
            $data['add_admin_id']   = $this->userInfo['id'];
            $data['status']         = 0;
            $data['product_count']  = 1;
            foreach ($arr as $key => $value) {
                $data['imei'] = $value['A'];
                if($value['B']){
                    $data['ems_time'] = strtotime(gmdate("Y-m-d H:i:s", $Shared_Date->ExcelToPHP($value['B'])));
                }
                if($value['C']){
                    $data['ems_take_time'] = strtotime(gmdate("Y-m-d H:i:s", $Shared_Date->ExcelToPHP($value['C'])));
                }
               if($value['D']){
                    $data['accept_time'] = strtotime(gmdate("Y-m-d H:i:s", $Shared_Date->ExcelToPHP($value['D'])));
                }  
                $data['ems_sn'] = $value['E'];
                $data['supplier_code'] = $value['F'];
                //门店
                if($value['H']){
                    foreach ($supplier_names as $k => $v) {
                        if(intval($value['H']) == $v['id']){
                            $data['supplier_id'] = $v['id'];
                        }elseif(strcasecmp(trim($value['G']),trim($v['name'])) == 0){
                            $data['supplier_id'] = $v['id'];
                        }                  
                    }
                }else{
                    foreach ($supplier as $k => $v) {
                        if(strcasecmp(trim($value['G']),trim($v['name'])) == 0){
                            $data['supplier_id'] = $v['id'];
                        }                  
                    }
                }   
                //厂商
                if($value['J']){
                    foreach ($manufacturer_names as $k => $v) {
                        if(intval($value['J']) == $v['id']){
                            $data['manufacturer_id'] = $v['id'];
                        }elseif(strcasecmp(trim($value['I']),trim($v['name'])) == 0){
                            $data['manufacturer_id'] = $v['id'];
                        }                  
                    }
                }else{
                    foreach ($manufacturer_names as $k => $v) {
                        if(strcasecmp(trim($value['I']),trim($v['name'])) == 0){
                            $data['manufacturer_id'] = $v['id'];
                        }                  
                    }
                }
                //运营商
                if($value['L']){
                    foreach ($unicom_names as $k => $v) {
                        if(intval($value['L']) == $v['id']){
                            $data['unicom_id'] = $v['id'];
                        }elseif(strcasecmp(trim($value['K']),trim($v['name'])) == 0){
                            $data['unicom_id'] = $v['id'];
                        }                  
                    }
                }else{
                    foreach ($unicom as $k => $v) {
                        if(strcasecmp(trim($value['K']),trim($v['name'])) == 0){
                            $data['unicom_id'] = $v['id'];
                        }                  
                    }
                }
                //产品名称
                if($value['N']){
                    foreach ($producttype_names as $k => $v) {
                        if(intval($value['N']) == $v['id']){
                            $data['producttype_id'] = $v['id'];
                        }elseif(strcasecmp(trim($value['M']),trim($v['name'])) == 0){
                            $data['producttype_id'] = $v['id'];
                        }                  
                    }
                }else{
                    foreach ($producttype_names as $k => $v) {
                        if(strcasecmp(trim($value['M']),trim($v['name'])) == 0){
                            $data['producttype_id'] = $v['id'];
                        }                  
                    }
                }
                $data['fault_why'] = $value['O'];
                $data['number'] = unique_str();
                $data['uptime'] = time();
                $data['add_time'] = time();

                $this->mobile->data($data)->insert();
            }
            $this->mobile->DeleteFile($excel);
            $this->jsonMsg('已全部导入,导入是否正确请自行检查!',1,url('repair/mobile/index'));
        }
    }
}