<?php
namespace app\repair\controller;
use framework\ext\Form;
/* * 
 * 诊断管理
 * */
class DiacrisisController extends \app\base\controller\AdminController{
    
    private $mobile;
    private $producttype;  //产品类型
    private $place;   //库位
    private $manufacturer;  //厂商
    private $unicom;   //运营商
    private $supplier;   //运营商

    public function __construct() {
        parent::__construct();
        $this->mobile       = obj('mobile');
        $this->producttype  = obj('warehouse/producttype');
        $this->place        = obj('warehouse/place');
        $this->manufacturer = obj('warehouse/manufacturer');
        $this->unicom       = obj('warehouse/unicom');
        $this->supplier     = obj('warehouse/supplier');
    } 

    //列表
    public function index(){
        $condition = [];
        $condition['status'] = 1;  //待入库
        if($this->isPost()){
            $this->imei            = $this->request('post.imei');
            $this->supplier_id     = $this->request('post.supplier_id');
            $this->number          = $this->request('post.number');
            $this->ems_sn          = $this->request('post.ems_sn');
            $this->producttype_id  = $this->request('post.producttype_id');
            $this->manufacturer_id = $this->request('post.manufacturer_id');
            $this->start_time      = $this->request('post.start_time');
            $this->end_time        = $this->request('post.end_time');
            $this->is_urgent       =  $this->request('post.is_urgent');
        }else{
            $this->imei            = $this->request('get.imei');
            $this->number          = $this->request('get.number');
            $this->ems_sn          = $this->request('get.ems_sn');
            $this->supplier_id     = $this->request('get.supplier_id');
            $this->producttype_id  = $this->request('get.producttype_id');
            $this->manufacturer_id = $this->request('get.manufacturer_id');
            $this->start_time      = $this->request('get.start_time');
            $this->end_time        = $this->request('get.end_time');
            $this->is_urgent       = $this->request('get.is_urgent');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['number'] = $this->number;
        }
        if($this->ems_sn){
            $condition['ems_sn'] = $this->ems_sn;
            $search['ems_sn'] = $this->ems_sn;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
            $search['manufacturer_id'] = $this->manufacturer_id;
        }
        if($this->start_time){
            $condition['1'] =  "addwarehouse_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "addwarehouse_time <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }
        if($this->is_urgent){
            $condition['is_urgent'] = $this->is_urgent == 1 ? 1 : 0;
            $search['is_urgent']    = $this->is_urgent;
        }  
        //列表
        $page        = $this->request('get.page',intval);
        $this->list  = $this->mobile->pager($page,20)->select_package_iphone_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);

        $this->producttype_lists  = $this->producttype->select();
        $this->manufacturer_lists = $this->manufacturer->select();
        $this->unicom_lists       = $this->unicom->select();
        $this->supplier_lists     = $this->supplier->select();
        $this->display();
    }

    //列表
    public function logs(){
        $is_excel = $this->request('get.is_excel');
        //搜索条件
        $condition = array();
        if($this->isPost()){
            $this->imei            = $this->request('post.imei');
            $this->supplier_id     = $this->request('post.supplier_id');
            $this->number          = $this->request('post.number');
            $this->ems_sn          = $this->request('post.ems_sn');
            $this->producttype_id  = $this->request('post.producttype_id');
            $this->manufacturer_id = $this->request('post.manufacturer_id');
            $this->start_time      = $this->request('post.start_time');
            $this->end_time        = $this->request('post.end_time');
        }else{
            $this->imei            = $this->request('get.imei');
            $this->number          = $this->request('get.number');
            $this->ems_sn          = $this->request('get.ems_sn');
            $this->supplier_id     = $this->request('get.supplier_id');
            $this->producttype_id  = $this->request('get.producttype_id');
            $this->manufacturer_id = $this->request('get.manufacturer_id');
            $this->start_time      = $this->request('get.start_time');
            $this->end_time        = $this->request('get.end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['number'] = $this->number;
        }
        if($this->ems_sn){
            $condition['ems_sn'] = $this->ems_sn;
            $search['ems_sn'] = $this->ems_sn;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
            $search['manufacturer_id'] = $this->manufacturer_id;
        }
        if($this->start_time){
            $condition['1'] =  "addwarehouse_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "addwarehouse_time <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }  
        //列表
        $condition[0] = 'status >= 2';
        $page        = $this->request('get.page',intval);
        $this->list  = $this->mobile->pager($page,20)->select_package_iphone_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        if($is_excel){
            header("Content-type:application/octet-stream");
            header("Accept-Ranges:bytes");
            header("Content-type:application/vnd.ms-excel");  
            header("Content-Disposition:attachment;filename=".date('Ymdhis').".xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            $this->layout ='';
            $this->display('app/repair/view/diacrisis/excel');
        }else{
            $this->place_lists        = $this->place->select();
            $this->manufacturer_lists = $this->manufacturer->select();
            $this->unicom_lists       = $this->unicom->select();
            $this->supplier_lists     = $this->supplier->select();
            $this->producttype_lists  = $this->producttype->select();
            $search['is_excel'] = 1;
            $this->assign('search',$search);
            $this->display();
        }
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $data = array();
            $form = new Form('post',array('id'));
            $data = $form->getVal();
            $data['status']               = 2;
            $data['diacrisis_admin_id']   = $this->userInfo['id'];
            $data['diacrisis_admin_name'] = $this->userInfo['username'];
            $data['diacrisis_time']       = time();
            if(is_array($data['service_mode'])){
                $data['service_mode'] = implode(',',$data['service_mode']);
            }
            $condition['id'] = $this->request('post.id');;
            $rel = $this->mobile->where($condition)->data($data)->update();
            if($rel){
                $this->jsonMsg('诊断结果提交成功',1,url('diacrisis/index'));
            }else{
                $this->jsonMsg('诊断结果提交败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $condition['id'] = $this->id;
            $info = $this->mobile->where($condition)->find();
            if($info){
                $producttype = $this->producttype->where(['id' => $info['producttype_id']])->find();
                $info['producttype_name'] = $producttype['name'];
                $manufacturer = $this->manufacturer->where(['id' => $info['manufacturer_id']])->find();
                $info['manufacturer_name'] = $manufacturer['name'];
                $supplier = $this->supplier->where(['id' => $info['supplier_id']])->find();
                $info['supplier_name'] = $supplier['name'];
            }
            $this->assign('info',$info);
            $this->display();
        }
    } 

    //显示
    public function reads(){
        $this->id = $this->request('get.id',intval);
        $info = $this->mobile->where(array('id' =>$this->id))->find();
        if($info){
            $producttype = $this->producttype->where(['id' => $info['producttype_id']])->find();
            $info['producttype_name'] = $producttype['name'];
            $manufacturer = $this->manufacturer->where(['id' => $info['manufacturer_id']])->find();
            $info['manufacturer_name'] = $manufacturer['name'];
            $supplier = $this->supplier->where(['id' => $info['supplier_id']])->find();
            $info['supplier_name'] = $supplier['name'];
        }
        $this->info = $info;
        $this->display();
    }

    //检查是否重复维修
    public function is_repeat(){
        if($this->isPost()){
            $imei = trim($this->request('post.imei'));
            if(!$imei) exit('查询失败');
            $condition[] = "new_imei = '{$imei}' or old_imei = '{$imei}'";
            $rel = obj('warehouse/imei')->where($condition)->find();
            if($rel){
                $info = $this->mobile->where(['imei'=>$imei])->find();
                if($info){
                    $times = $info['uptime'] - $rel['uptime'];
                    if($times >= 2592000){
                        exit('<span class="red">重复维修</span>');
                    }
                }
                exit('未重复');
            }else{
                exit('未重复');
            }
        } 
    }
}