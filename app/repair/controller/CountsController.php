<?php
namespace app\repair\controller;
use framework\ext\Form;
/* * 
 * 统计分析
 * */
class countsController extends \app\base\controller\AdminController{
    private $mobile = null;
    private $mobilebom = null;
    private $producttype = null;  //产品类型
    private $place = null;   //库位
    private $manufacturer = null;  //厂商
    private $unicom = null;   //运营商
    private $supplier = null;   //门店号
    private $faulttype = null;   //运营商

    public function __construct() {
        parent::__construct();
        $this->mobile = obj('mobile');
        $this->mobilebom = obj('mobilebom');
        $this->producttype = obj('warehouse/producttype');
        $this->place = obj('warehouse/place');
        $this->manufacturer = obj('warehouse/manufacturer');
        $this->unicom = obj('warehouse/unicom');
        $this->supplier = obj('warehouse/supplier');
        $this->faulttype = obj('warehouse/faulttype');
    } 

    //列表
    public function index(){
        $condition = array();
        if($this->isPost()){
            $this->start_time = $this->request('post.start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->start_time = $this->request('get.start_time');
            $this->end_time = $this->request('get.end_time');
        }
        if($this->start_time){
            $condition['0'] =  "add_time >=".strtotime($this->start_time);
        }
        if($this->end_time){
            $condition['1'] = "out_time <=".strtotime($this->end_time);
        }
        $this->list = $this->mobile->where($condition)->field('id,status')->select();
        $counts = array(0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0);
        if($this->list){
            foreach ($this->list as $key => $rs) {
                $counts[0] = $counts[0]+1;
                if($rs['status'] == 0){  //预入库
                    $counts[1] = $counts[1] + 1;
                }
                if($rs['status'] == 1){  //新入库
                    $counts[2] = $counts[2] + 1;
                }
                if($rs['status'] == 2){  //维修中
                    $counts[3] = $counts[3] + 1;
                }
                if($rs['status'] == 3){  //质检中
                    $counts[4] = $counts[4] + 1;
                }
                if($rs['status'] == 4){  //待出库
                    $counts[5] = $counts[5] + 1;
                }
                if($rs['status'] == 5){  //已出库
                    $counts[6] = $counts[6] + 1;
                }
            }
        }
        $this->counts = $counts;
        $this->display();
    }

    //报表导出
    public function excel(){
        $condition = array();
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->mobile_name = $this->request('post.mobile_name');
            $this->start_time = $this->request('post.start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->mobile_name = $this->request('get.mobile_name');
            $this->start_time = $this->request('get.start_time');
            $this->end_time = $this->request('get.end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['name'] = $this->number;
        }
        if($this->mobile_name){
            $condition['0'] = "mobile_name LIKE '%".$this->mobile_name."%'";
            $search['mobile_name'] = $this->mobile_name;
        }
        if($this->start_time){
            $condition['1'] =  "add_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "out_time <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        } 
        $this->search = $search;
        $page = $this->request('get.page',intval);
        $this->list = $this->mobile->pager($page,20)->select_supplier_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        $this->supplier_lists = $this->supplier->select();
        $this->display();
    }

    //汇总表
    public function all_excel(){
        $this->header_excel('all_excel_');
        $this->id = $this->request('get.id',intval);
        $this->info = $this->mobile->where(array('id' =>$this->id))->find();
        $this->list = $this->mobilebom->where(array('phone_id' =>$this->id))->select_lists(array('phone_id' =>$this->id));
        if($this->info['place_id']){
            $this->place_info = $this->place->where(array('id' =>$this->info['place_id']))->find();
        }
        if($this->info['supplier_id']){
            $this->supplier_info = $this->supplier->where(array('id' =>$this->info['supplier_id']))->find();
        }
        $this->display();
    }

    //报表
    public function toexcel_a(){
        $this->header_excel('toexcel_a_');
        $condition = array();
        $condition['status'] = 1;
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->mobile_name = $this->request('post.mobile_name');
            $this->start_time = $this->request('post.start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->mobile_name = $this->request('get.mobile_name');
            $this->start_time = $this->request('get.start_time');
            $this->end_time = $this->request('get.end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['name'] = $this->number;
        }
        if($this->mobile_name){
            $condition['0'] = "mobile_name LIKE '%".$this->mobile_name."%'";
            $search['mobile_name'] = $this->mobile_name;
        }
        if($this->start_time){
            $condition['1'] =  "add_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "out_time <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        } 
        $this->list = $this->mobile->select_supplier_lists($condition);
        $this->supplier_lists = $this->supplier->select();  //门店
        $this->unicom_lists = $this->unicom->select();   //运营商
        $this->manufacturer_lists = $this->manufacturer ->select();   //厂商  
        $this->display();  
    }

//报表
    public function toexcel_b(){
        $this->header_excel('toexcel_b_');
        $condition = array();
        $condition['status'] = 5;
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->mobile_name = $this->request('post.mobile_name');
            $this->start_time = $this->request('post.start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->mobile_name = $this->request('get.mobile_name');
            $this->start_time = $this->request('get.start_time');
            $this->end_time = $this->request('get.end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['name'] = $this->number;
        }
        if($this->mobile_name){
            $condition['0'] = "mobile_name LIKE '%".$this->mobile_name."%'";
            $search['mobile_name'] = $this->mobile_name;
        }
        if($this->start_time){
            $condition['1'] =  "add_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "out_time <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        } 
        $this->list = $this->mobile->select_supplier_lists($condition);
        $this->supplier_lists = $this->supplier->select();  //门店
        $this->unicom_lists = $this->unicom->select();   //运营商
        $this->manufacturer_lists = $this->manufacturer ->select();   //厂商  
        $this->display();  
    }

//报表
    public function toexcel_c(){
        $this->header_excel('toexcel_c_');
        $condition = array();
        $condition['status'] = 3;
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->mobile_name = $this->request('post.mobile_name');
            $this->start_time = $this->request('post.start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->mobile_name = $this->request('get.mobile_name');
            $this->start_time = $this->request('get.start_time');
            $this->end_time = $this->request('get.end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['name'] = $this->number;
        }
        if($this->mobile_name){
            $condition['0'] = "mobile_name LIKE '%".$this->mobile_name."%'";
            $search['mobile_name'] = $this->mobile_name;
        }
        if($this->start_time){
            $condition['1'] =  "add_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "out_time <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        } 
        $info = $this->mobile->select_supplier_lists($condition);
        $list = array();
        if($info){
            foreach ($info as $key => $value) {
                $list[$key] = $value;
                if($value['faulttype_id']){
                   $faulttype_id = explode(',',$value['faulttype_id']);
                   foreach ($faulttype_id as $k => $id) {
                       $faulttype_info = $this->faulttype->where(array('id'=>$id))->find();
                       if($faulttype_info){
                            $list[$key]['faulttype_big'][$id]['faulttype_code'] = $faulttype_info['name'];
                            $list[$key]['faulttype_big'][$id]['faulttype_name'] = $faulttype_info['about'];
                       }
                   }
                }
                if($value['faulttype']){
                   $faulttype = explode(',',$value['faulttype']);
                   foreach ($faulttype as $k => $faulttype_id) {
                       $faulttype_info = $this->faulttype->where(array('id'=>$faulttype_id))->find();
                       if($faulttype_info){
                            $list[$key]['faulttype_big'][$faulttype_info['pid']]['faulttype_smalle'][$k]['faulttype_code'] = $faulttype_info['name'];
                            $list[$key]['faulttype_big'][$faulttype_info['pid']]['faulttype_smalle'][$k]['faulttype_name'] = $faulttype_info['about'];            
                       }
                   }
                }
               $mobile_bom = $this->mobilebom->select_lists(array('phone_id' =>$value['id']));
               if($mobile_bom){
                    $list[$key]['mobile_bom'] = $mobile_bom;
               }
            }
        }
        $this->list = $list;
        $this->supplier_lists = $this->supplier->select();  //门店
        $this->unicom_lists = $this->unicom->select();   //运营商
        $this->manufacturer_lists = $this->manufacturer ->select();   //厂商  
        $this->display();  
    }

    //报表导出
    public function toexcel_lists(){
        $this->header_excel('toexcel_lists_');
        $condition = array();
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->mobile_name = $this->request('post.mobile_name');
            $this->start_time = $this->request('post.start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->mobile_name = $this->request('get.mobile_name');
            $this->start_time = $this->request('get.start_time');
            $this->end_time = $this->request('get.end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
        }
        if($this->mobile_name){
            $condition['0'] = "mobile_name LIKE '%".$this->mobile_name."%'";
        }
        if($this->start_time){
            $condition['1'] =  "add_time >=".strtotime($this->start_time);
        }
        if($this->end_time){
            $condition['2'] = "out_time <=".strtotime($this->end_time);
        } 
        $info = $this->mobile->select_supplier_lists($condition);
        $this->supplier_lists = $this->supplier->select();  //门店
        $this->unicom_lists = $this->unicom->select();   //运营商
        $list = array();
        if($info){
            foreach ($info as $key => $value) {
                $list[$key] = $value;
                if($value['faulttype_id']){
                   $faulttype_id = explode(',',$value['faulttype_id']);
                   foreach ($faulttype_id as $k => $id) {
                       $faulttype_info = $this->faulttype->where(array('id'=>$id))->find();
                       if($faulttype_info){
                            $list[$key]['faulttype_big'][$id]['faulttype_code'] = $faulttype_info['name'];
                            $list[$key]['faulttype_big'][$id]['faulttype_name'] = $faulttype_info['about'];
                       }
                   }
                }
                if($value['faulttype']){
                   $faulttype = explode(',',$value['faulttype']);
                   foreach ($faulttype as $k => $faulttype_id) {
                       $faulttype_info = $this->faulttype->where(array('id'=>$faulttype_id))->find();
                       if($faulttype_info){
                            $list[$key]['faulttype_big'][$faulttype_info['pid']]['faulttype_smalle'][$k]['faulttype_code'] = $faulttype_info['name'];
                            $list[$key]['faulttype_big'][$faulttype_info['pid']]['faulttype_smalle'][$k]['faulttype_name'] = $faulttype_info['about'];            
                       }
                   }
                }
               $mobile_bom = $this->mobilebom->select_lists(array('phone_id' =>$value['id']));
               if($mobile_bom){
                    $list[$key]['mobile_bom'] = $mobile_bom;
               }
            }
        }
        $this->list = $list;
        $this->display();
    }
}
