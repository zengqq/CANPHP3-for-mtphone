<?php
namespace app\repair\controller;
use framework\ext\Form;
/* * 
 * 维修报表
 * */
class OutwarehouseController extends \app\base\controller\AdminController{

    private $mobile;
    private $package;
    private $supplier;
    private $producttype;
    private $manufacturer;
    private $mobilebom;
    private $unicom;
    private $place;

    public function __construct() {
        parent::__construct();
        $this->mobile       = obj('mobile');
        $this->package      = obj('package/package');
        $this->supplier     = obj('warehouse/supplier');
        $this->producttype  = obj('warehouse/producttype');
        $this->manufacturer = obj('warehouse/manufacturer');
        $this->unicom       = obj('warehouse/unicom');
        $this->mobilebom    = obj('mobilebom');
        $this->place        = obj('warehouse/place');
    } 

    //列表
    public function index(){
        $condition = array();
        $condition['0'] = '(status= 4 or status=5)';
        if($this->isPost()){
            $this->new_imei   = $this->request('post.new_imei');
            $this->imei       = $this->request('post.imei');
            $this->start_time = $this->request('post.start_time');
            $this->end_time   = $this->request('post.end_time');
            $this->is_urgent   = $this->request('post.is_urgent');
        }else{
            $this->new_imei   = $this->request('get.new_imei');
            $this->imei       = $this->request('get.imei');
            $this->start_time = $this->request('get.start_time');
            $this->end_time   = $this->request('get.end_time');
            $this->is_urgent  = $this->request('get.is_urgent');
        }
        if($this->new_imei){
            $condition['new_imei'] = $this->new_imei;
            $search['new_imei']    = $this->new_imei;
        } 
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei']    = $this->imei;
        } 
        if($this->start_time){
            $condition['1'] =  "testing_time >= ".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "testing_time <= ".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }  
        if($this->is_urgent){
            $condition['is_urgent'] = $this->is_urgent == 1 ? 1 : 0;
            $search['is_urgent']    = $this->is_urgent;
        } 
        $page = $this->request('get.page',intval);
        $this->list = $this->mobile->pager($page,20)->select_package_iphone_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        $this->supplier_lists = $this->supplier->select();
        $this->display();
    }

    //列表
    public function logs(){
        $is_excel = $this->request('get.is_excel');
        $condition = array();
        $condition['status'] = 6;
        if($this->isPost()){
            $this->new_imei   = $this->request('post.new_imei');
            $this->imei       = $this->request('post.imei');
            $this->start_time = $this->request('post.start_time');
            $this->end_time   = $this->request('post.end_time');
        }else{
            $this->new_imei   = $this->request('get.new_imei');
            $this->imei       = $this->request('get.imei');
            $this->start_time = $this->request('get.start_time');
            $this->end_time   = $this->request('get.end_time');
        }
        if($this->new_imei){
            $condition['new_imei'] = $this->new_imei;
            $search['new_imei']    = $this->new_imei;
        } 
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei']    = $this->imei;
        } 
        if($this->start_time){
            $condition['1'] =  "out_time >= ".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "out_time <= ".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        } 
        $page = $this->request('get.page',intval);
        $this->list  = $this->mobile->pager($page,20)->select_package_iphone_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        if($is_excel){
            header("Content-type:application/octet-stream");
            header("Accept-Ranges:bytes");
            header("Content-type:application/vnd.ms-excel");  
            header("Content-Disposition:attachment;filename=".date('Ymdhis').".xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            $this->layout ='';
            $this->display('app/repair/view/outwarehouse/excel');
        }else{
            $search['is_excel'] = 1;
            $this->assign('search',$search);
            $this->display();
        }
    }

    //查看详情
    public function reads(){
        $is_excel = $this->request('get.is_excel');
        $this->id = $this->request('get.id',intval);
        $info = $this->mobile->where(array('id' =>$this->id))->find();
        if($info){
            $producttype = $this->producttype->where(['id' => $info['producttype_id']])->find();
            $info['producttype_name'] = $producttype['name'];
            $manufacturer = $this->manufacturer->where(['id' => $info['manufacturer_id']])->find();
            $info['manufacturer_name'] = $manufacturer['name'];
            $supplier = $this->supplier->where(['id' => $info['supplier_id']])->find();
            $info['supplier_name'] = $supplier['name'];
        }
        $this->info = $info;
        $this->list = $this->mobilebom->where(array('phone_id' =>$this->id))->select_lists(array('phone_id' =>$this->id));
        if($is_excel){
            header("Content-type:application/octet-stream");
            header("Accept-Ranges:bytes");
            header("Content-type:application/vnd.ms-excel");  
            header("Content-Disposition:attachment;filename=".date('Ymdhis').".xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            $this->layout ='';
            $this->display('app/repair/view/outwarehouse/reads_excel');
        }else{
            $this->display();
        }
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $data = array();
            $form = new Form('post',array('id','imei'));
            $data = $form->getVal();
            $data['status'] = 6;
            $data['out_time'] = time();
            $data['outwarehouse_admin_name'] = $this->userInfo['username'];
            $data['outwarehouse_admin_id'] = $this->userInfo['id'];
            $id = $this->request('post.id');
            $condition['id'] = $id;
            $rel = $this->mobile->where($condition)->data($data)->update();
            if($rel){
                //出库操作
                $info = $this->mobile->where($condition)->find();
                $imei = $info['is_news_phone'] ? $info['new_imei'] : $info['imei'];
                $ishouse = obj('warehouse/bomlogs')->where(['imei' =>$imei,'types'=>1])->find();
                if(!$ishouse){
                    $this->jsonMsg('当前IMEI:'.$imei.'产品在仓库中未找到');
                }else{
                    $warehouse_rel = obj('warehouse/bom')->where(['id' =>$ishouse['bom_id']])->find();
                    if($warehouse_rel['amount'] <= 0){
                        $this->jsonMsg('当前IMEI:'.$imei.' 产品在仓库中数量是“0”');
                    }
                }
                $amount = $warehouse_rel['amount']-1;
                obj('warehouse/bom')->data(['amount' => $amount])->where(['id' =>$ishouse['bom_id']])->update();
                //增加日志
                $bomlogs['types']      = 0;
                $bomlogs['uptime']     = time();
                $bomlogs['bom_id']     = $warehouse_rel['id'];
                $bomlogs['imei']       = $imei;
                $bomlogs['amount']     = 1;
                $bomlogs['admin_name'] = $this->userInfo['username'];
                $bomlogs['down_name']  = $this->userInfo['username'];
                $bomlogs['place_id']   = $warehouse_rel['place_id'];
                obj('warehouse/bomlogs')->data($bomlogs)->insert();
                //END
                $this->jsonMsg('提交发货单成功',1,url('outwarehouse/index'));
            }else{
                $this->jsonMsg('提交发货单失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $info = $this->mobile->where(array('id' =>$this->id))->find();
            if($info){
                $producttype = $this->producttype->where(['id' => $info['producttype_id']])->find();
                $info['producttype_name'] = $producttype['name'];
                $manufacturer = $this->manufacturer->where(['id' => $info['manufacturer_id']])->find();
                $info['manufacturer_name'] = $manufacturer['name'];
                $supplier = $this->supplier->where(['id' => $info['producttype_id']])->find();
                $info['supplier_name'] = $supplier['name'];
                $unicom = $this->unicom->where(['id' => $info['manufacturer_id']])->find();
                $info['unicom_name'] = $unicom['name'];
            }
            $this->info        = $info;
            $this->display();
        }
    } 
}