<?php
namespace app\fornew\controller;
use framework\ext\Form;
/* *
 * 确认入库
 * */
class addwarehouseController extends \app\base\controller\AdminController{
    private $fornew = null;
    private $place = null;   //库位

    public function __construct() {
        parent::__construct();
        $this->fornew = obj('fornew');
        $this->supplier = obj('warehouse/supplier');
        $this->place = obj('warehouse/place');
    } 

    //列表
    public function index(){
        //搜索表单数据
        $this->supplier_lists = $this->supplier->select();
        //搜索条件
        $condition = array();
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->producttype_id = $this->request('post.producttype_id');
            $this->ems_start_time = $this->request('post.ems_start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->producttype_id = $this->request('get.producttype_id');
            $this->ems_start_time = $this->request('get.ems_start_time');
            $this->ems_end_time = $this->request('get.ems_end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['number'] = $this->number;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->start_time){
            $condition['1'] =  "ems_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->ems_end_time){
            $condition['2'] = "ems_time <=".strtotime($this->ems_end_time);
            $search['ems_end_time'] = $this->ems_end_time;
        }  
        //列表
        $condition['status'] = 1;
        $page = $this->request('get.page',intval);
        $this->list = $this->fornew->pager($page,20)->select_lists($condition);
        $this->pager = $this->getPage($this->fornew->pager,$search);
        $this->display();
    }

    //列表
    public function logs(){
       //搜索表单数据
        $this->supplier_lists = $this->supplier->select();
        //搜索条件
        $condition = array();
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->producttype_id = $this->request('post.producttype_id');
            $this->ems_start_time = $this->request('post.ems_start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->producttype_id = $this->request('get.producttype_id');
            $this->ems_start_time = $this->request('get.ems_start_time');
            $this->ems_end_time = $this->request('get.ems_end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['number'] = $this->number;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->start_time){
            $condition['1'] =  "ems_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->ems_end_time){
            $condition['2'] = "ems_time <=".strtotime($this->ems_end_time);
            $search['ems_end_time'] = $this->ems_end_time;
        }  
        //列表
        $condition['status'] = 2;
        $page = $this->request('get.page',intval);
        $this->list = $this->fornew->pager($page,20)->select_lists($condition);
        $this->pager = $this->getPage($this->fornew->pager,$search);
        $this->display();
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $data = array();
            $form = new Form('post',array('id','shop_time','ems_take_time'));
            $data = $form->getVal();
            $data['status'] = 2;
            $data['addwarehouse_admin_id']   = $this->userInfo['id'];
            $data['addwarehouse_admin_name'] = $this->userInfo['username'];
            $data['addwarehouse_time'] = time();
            $data['shop_time'] = strtotime($form->getVal('shop_time'));
            $data['ems_take_time'] = strtotime($form->getVal('ems_take_time'));
            $id = $this->request('post.id');
            $condition['id'] = $id;
            $rel = $this->fornew->where($condition)->data($data)->update();
            if($rel){
                $this->jsonMsg('提交入库成功',1,url('addwarehouse/index'));
            }else{
                $this->jsonMsg('提交入库失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $condition['id'] = $this->id;
            $this->info = $this->fornew->where($condition)->find();
            $this->place_lists = $this->place->select();
            $this->display();
        }
    } 
}