<?php
namespace app\fornew\controller;
use framework\ext\Form;
use framework\ext\Check;
/* *
 * 数据导入
 * */
class excelController extends \app\base\controller\AdminController{
    private $fornew = null;
    private $producttype = null;  //产品类型
    private $manufacturer = null;  //厂商
    private $unicom = null;   //运营商
    private $supplier = null;   //门店号

    public function __construct() {
        parent::__construct();
        $this->fornew = obj('fornew');
        $this->producttype = obj('warehouse/producttype');
        $this->manufacturer = obj('warehouse/manufacturer');
        $this->unicom = obj('warehouse/unicom');
        $this->supplier = obj('warehouse/supplier');
    } 

    //列表
    public function index(){
        //搜索表单数据
        $this->supplier_lists = $this->supplier->select();
        //搜索条件
        $condition = array();
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->producttype_id = $this->request('post.producttype_id');
            $this->ems_start_time = $this->request('post.ems_start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->producttype_id = $this->request('get.producttype_id');
            $this->ems_start_time = $this->request('get.ems_start_time');
            $this->ems_end_time = $this->request('get.ems_end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['number'] = $this->number;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->start_time){
            $condition['1'] =  "ems_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->ems_end_time){
            $condition['2'] = "ems_time <=".strtotime($this->ems_end_time);
            $search['ems_end_time'] = $this->ems_end_time;
        }  
        //列表
        $condition['status'] = 0;
        $page = $this->request('get.page',intval);
        $this->list = $this->fornew->pager($page,20)->select_lists($condition);
        $this->pager = $this->getPage($this->fornew->pager,$search);
        $this->display();
    }

    //列表
    public function del(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $result = $this->fornew->where(array('id' =>$id,'status'=>0))->delete();
            if($result){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }
    }

    //编辑
    public function reads(){
        $this->producttype_lists = $this->producttype->select();
        $this->manufacturer_lists = $this->manufacturer->select();
        $this->unicom_lists = $this->unicom->select();
        $this->supplier_lists = $this->supplier->select();
        $this->id = $this->request('get.id',intval);
        $this->info = $this->fornew->where(array('id' =>$this->id))->find();
        $this->display();
    }

//添加
    public function add(){
        if($this->isPost()){
            $form = new Form('post',array('ems_time','address_time'));
            $data = $form->getVal();
            $data['status'] = 0;
            $data['uptime'] = time();
            $data['add_time'] = time();
            $data['add_admin_name'] = $this->userInfo['username'];
            $data['add_admin_id'] = $this->userInfo['id'];
            $data['ems_time'] = strtotime($form->getVal('ems_time'));
            $data['address_time'] = strtotime($form->getVal('address_time'));
            $data['number'] = unique_str();
            $rel = $this->fornew->data($data)->insert();
            if($rel){
                $this->jsonMsg('换新录入成功',1,url('excel/index'));
            }else{
                $this->jsonMsg('换新录入失败');
            }
        }else{
            $this->producttype_lists = $this->producttype->select();
            $this->manufacturer_lists = $this->manufacturer->select();
            $this->unicom_lists = $this->unicom->select();
            $this->supplier_lists = $this->supplier->select();
            $this->display();
        }
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $form = new Form('post',array('id','ems_time','address_time'));
            $data = $form->getVal();
            $data['status'] = 0;
            $data['uptime'] = time();
            $data['ems_time'] = strtotime($form->getVal('ems_time'));
            $data['address_time'] = strtotime($form->getVal('address_time'));
            $id = intval($form->getVal('id'));
            $condition['id'] = $id;
            $rel = $this->fornew->where($condition)->data($data)->update();
            if($rel){
                $this->jsonMsg('设置成功',1,url('excel/index'));
            }else{
                $this->jsonMsg('设置失败');
            }
        }else{
            $this->producttype_lists = $this->producttype->select();
            $this->manufacturer_lists = $this->manufacturer->select();
            $this->unicom_lists = $this->unicom->select();
            $this->supplier_lists = $this->supplier->select();
            $this->id = $this->request('get.id',intval);
            $this->info = $this->fornew->where(array('id' =>$this->id))->find();
            $this->display();
        }
    }
    
    //上传表单
    public function lists(){
        $this->display();
    }

    //上传模板
    public function uploads(){
        if($this->isPost()){
            $config = config('fornew');
            echo $this->Files($_FILES,$config);
        }else{
            $this->frameid = $this->request('get.id');
            $this->display();
        }
    }

    //列表
    public function exceltotemp(){
        if($this->isPost()){
            $excel = $this->request('post.excel');;
            $msg = Check::rule(array(check::must($excel),'导入表格文件不存在或已删除,请重新上传。'));
            if(true !== $msg){
                $this->jsonMsg($msg);
            }
            //引入第三方类库
            vendor("PHPExcel");
            $PHPExcel=new \PHPExcel();  //创建PHPExcel对象，注意，不能少了\
            //vendor("PHPExcel/Reader/Excel5");
            //$PHPReader=new \PHPExcel_Reader_Excel5();
            vendor("PHPExcel/Reader/Excel2007");
            $PHPReader = new \PHPExcel_Reader_Excel2007();
            $Shared_Date = new \PHPExcel_Shared_Date();  //表给日期格式化

            $PHPExcel = $PHPReader->load(BASE_PATH.$excel);  //载入文件
            $currentSheet = $PHPExcel->getSheet(0);   //文件中第几个工作表
            $allColumn = $currentSheet->getHighestColumn(); //获取总列数
            $allRow = $currentSheet->getHighestRow();  //获取总行数
            //循环获取表中的数据，$currentRow表示当前行，从哪行开始读取数据，索引值从0开始
            for($currentRow=3;$currentRow<=$allRow;$currentRow++){
                for($currentColumn='A';$currentColumn<=$allColumn;$currentColumn++){//从哪列开始，A表示第一列
                    $address=$currentColumn.$currentRow;//数据坐标
                    $arr[$currentRow][$currentColumn]=$currentSheet->getCell($address)->getValue();//读取到的数据，保存到数组$arr中
                }
            }
            $i = 2;
            $manufacturer = $this->manufacturer_lists = $this->manufacturer->field('id,name')->select(); //厂商
            $manufacturer_names = array();
            if($manufacturer){
                foreach ($manufacturer  as $key => $value) {
                     $manufacturer_names[] = $value['name'];
                } 
            }
            $unicom = $this->unicom_lists = $this->unicom->field('id,name')->select(); //运营商:
            $unicom_names = array();
            if($unicom){
                foreach ($unicom  as $key => $value) {
                     $unicom_names[] = $value['name'];
                } 
            }
            $producttype = $this->producttype_lists = $this->producttype->field('id,name')->select(); //产品类型
            $producttype_names = array();
            if($producttype){
                foreach ($producttype  as $key => $value) {
                     $producttype_names[] = $value['name'];
                } 
            }
            $supplier = $this->supplier_lists = $this->supplier->field('id,name')->select(); //门店号
            $supplier_names = array();
            if($supplier){
                foreach ($supplier  as $key => $value) {
                     $supplier_names[] = $value['name'];
                } 
            }
            foreach ($arr as $key => $value) {
                $i++;
                //厂商
                if($value['A']){
                    if(!in_array($value['A'],$manufacturer_names)){
                        $this->fornew->DeleteFile($excel);
                        $this->jsonMsg('数据检测不通过,第'.$i.'行输入的产品名称系统不存在');
                    }
                }else{
                    $this->jsonMsg('数据检测不通过,第'.$i.'行厂商不能为空');
                }
                //运营商
                if($value['B']){
                    if(!in_array($value['B'],$unicom_names)){
                        $this->fornew->DeleteFile($excel);
                        $this->jsonMsg('数据检测不通过,第'.$i.'行输入的运营商系统不存在');
                    }
                }else{
                    $this->fornew->DeleteFile($excel);
                    $this->jsonMsg('数据检测不通过,第'.$i.'行运营商不能为空');
                }
                //产品类型
                if($value['C']){
                    if(!in_array($value['C'],$producttype_names)){
                        $this->fornew->DeleteFile($excel);
                        $this->jsonMsg('数据检测不通过,第'.$i.'行输入的产品类型不存在');
                    }                 
                }else{
                    $this->fornew->DeleteFile($excel);
                    $this->jsonMsg('数据检测不通过,第'.$i.'行产品类型不能为空');
                }
                //门店号
                if($value['D']){
                    if(!in_array($value['D'],$supplier_names)){
                            $this->fornew->DeleteFile($excel);
                            $this->jsonMsg('数据检测不通过,第'.$i.'行输入的门店号不存在');
                    }  
                }else{
                    $this->fornew->DeleteFile($excel);
                    $this->jsonMsg('数据检测不通过,第'.$i.'行输入的门店号不能为空');
                }

            }
            foreach ($arr as $key => $value) {
                $i++;
                //厂商
                foreach ($manufacturer as $k => $v) {
                    if(trim($value['A']) == $v['name']){
                        $data['manufacturer_id'] = $v['id'];
                    }                    
                }
                //运营商
                foreach ($unicom as $k => $v) {
                    if(trim($value['B']) == $v['name']){
                        $data['unicom_id'] = $v['id'];
                    }                    
                }
                //产品类型
                foreach ($producttype as $k => $v) {
                    if(trim($value['C']) == $v['name']){
                        $data['producttype_id'] = $v['id'];
                    }                    
                }
                //门店号
                foreach ($supplier as $k => $v) {
                    if(trim($value['D']) == $v['name']){
                        $data['supplier_id'] = $v['id'];
                    }                    
                }
                $data['supplier_code'] = $value['E'];
                $data['imei'] = $value['F'];
                $data['fault_why'] = $value['G'];
                if($value['H']){
                    $data['ems_time'] = strtotime(gmdate("Y-m-d H:i:s", $Shared_Date->ExcelToPHP($value['H'])));
                }
                if($value['I']){
                    $data['address_time'] = strtotime(gmdate("Y-m-d H:i:s", $Shared_Date->ExcelToPHP($value['I'])));
                }
                $data['ems_sn'] = $value['J'];
                $data['come_address'] = $value['K'];
                //默认初始值
                $data['status'] = 0;
                $data['uptime'] = time();
                $data['add_time'] = time();
                $data['add_admin_name'] = $this->userInfo['username'];
                $data['add_admin_id'] = $this->userInfo['id'];
                $data['number'] = unique_str();
                $this->fornew->data($data)->insert();
            }
            $this->fornew->DeleteFile($excel);
            $this->jsonMsg('已全部导入,导入是否正确请自行检查!',1,url('excel/index'));
        }
    } 
}