<?php
namespace app\fornew\controller;
use framework\ext\Form;
/* *
 * 维修管理
 * */
class serviceController extends \app\base\controller\AdminController{
    private $fornew = null;
    private $faulttype = null;
    private $iphone = null;
    private $bom = null;  

    private $producttype = null;  //产品类型
    private $manufacturer = null;  //厂商
    private $unicom = null;   //运营商
    private $supplier = null;   //门店

    public function __construct() {
        parent::__construct();
        $this->fornew = obj('fornew');
        $this->fornewbom = obj('fornewbom');
        $this->faulttype = obj('warehouse/faulttype');
        $this->bom = obj('warehouse/bom');

        $this->producttype = obj('warehouse/producttype');
        $this->manufacturer = obj('warehouse/manufacturer');
        $this->unicom = obj('warehouse/unicom');
        $this->supplier = obj('warehouse/supplier');
        $this->iphone = obj('warehouse/iphone');
    } 

    //列表
    public function index(){
        //搜索表单数据
        $this->supplier_lists = $this->supplier->select();
        //搜索条件
        $condition = array();
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->producttype_id = $this->request('post.producttype_id');
            $this->ems_start_time = $this->request('post.ems_start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->producttype_id = $this->request('get.producttype_id');
            $this->ems_start_time = $this->request('get.ems_start_time');
            $this->ems_end_time = $this->request('get.ems_end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['number'] = $this->number;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->start_time){
            $condition['1'] =  "ems_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->ems_end_time){
            $condition['2'] = "ems_time <=".strtotime($this->ems_end_time);
            $search['ems_end_time'] = $this->ems_end_time;
        }  
        //列表
        $condition['status'] = 0;
        $page = $this->request('get.page',intval);
        $this->list = $this->fornew->pager($page,20)->select_lists($condition);
        $this->pager = $this->getPage($this->fornew->pager,$search);
        $this->display();
    }

    //列表
    public function logs(){
        //搜索表单数据
        $this->supplier_lists = $this->supplier->select();
        //搜索条件
        $condition = array();
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->producttype_id = $this->request('post.producttype_id');
            $this->ems_start_time = $this->request('post.ems_start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->producttype_id = $this->request('get.producttype_id');
            $this->ems_start_time = $this->request('get.ems_start_time');
            $this->ems_end_time = $this->request('get.ems_end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['number'] = $this->number;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->start_time){
            $condition['1'] =  "ems_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->ems_end_time){
            $condition['2'] = "ems_time <=".strtotime($this->ems_end_time);
            $search['ems_end_time'] = $this->ems_end_time;
        }  
        //列表
        $condition['status'] = 1;
        $page = $this->request('get.page',intval);
        $this->list = $this->fornew->pager($page,20)->select_lists($condition);
        $this->pager = $this->getPage($this->fornew->pager,$search);
        $this->display();
    }   
    
    //编辑
    public function edit(){
        $this->id = $this->request('get.id',intval);
        $this->info = $this->fornew->where(array('id' =>$this->id,'status'=>0))->find();
        if(!$this->info){
            $this->pageMsg('换新手机不存在');
        }
        $this->list = $this->fornewbom->where(array('phone_id' =>$this->id))->select_lists(array('phone_id' =>$this->id));
        $this->display();
    }

    //编辑
    public function bom(){
        if($this->isPost()){
            $form = new Form('post',array('id','faulttype','out_time','service_admin_name'));
            $data = $form->getVal();
            $id        = intval($form->getVal('id'));
            $faulttype = $form->getVal('faulttype');
            $out_time  = $form->getVal('out_time');
            if(is_array($faulttype)){
                if($faulttype){
                    foreach ($faulttype as $key => $value) {
                        $faulttype_id[] = $key;
                        $str[] = implode(',',$value);
                    }
                    if(is_array($faulttype_id)){
                        $data['faulttype_id'] = implode(',',$faulttype_id);
                    }
                    if(is_array($str)){
                        $data['faulttype'] = implode(',',$str);
                    }
                }     
            }else{
                $this->jsonMsg('故障类型必选,请选择您的故障类型!');
            }
            $data['service_time'] = time();
            $data['service_admin_name'] = $this->userInfo['username'];
            $data['service_admin_id']   = $this->userInfo['id'];
            $data['out_time']           = strtotime($out_time);
            $data['status'] = 1;
            $condition['id'] = $id;
            $rel = $this->fornew->where($condition)->data($data)->update();
            if($rel){
                $this->jsonMsg('换新提交成功',1,url('service/index'));
            }else{
                $this->jsonMsg('换新提交失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $this->info = $this->fornew->where(array('id' =>$this->id))->find();
            $this->faulttype_id = array();
            if($this->info['faulttype']){
                $this->faulttype_id = explode(',',$this->info['faulttype']);
            } 
            $this->faulttype_lists = $this->faulttype->select_all();
            $this->display();
        }
    }

    //######################################
    //添加手机换新
    public function add_bom(){
        $this->id = $this->request('get.id',intval);
        $page = $this->request('get.page',intval);
        $condition = array();
        if($this->isPost()){
            $this->name = $this->request('post.name');
            $this->code = $this->request('post.code');
        }else{
            $this->name = $this->request('post.name');
            $this->code = $this->request('get.code');
        }
        if($this->name){
            $condition['name'] = $this->name;
            $search['0'] = "name LIKE '%".$this->name."%'";
        }
        if($this->code){
            $condition['code'] = $this->code;
            $search['code'] = $this->code;
        }
        $search['id'] = $this->id;
        $this->list = $this->iphone->where($condition)->pager($page,20)->select();
        $this->pager = $this->getPage($this->iphone->pager,$search);
        $this->display();
    }

    //列表
    public function add_bom_save(){
        $condition = array();
        if($this->isPost()){
            $bom_id = $this->request('post.bom_id');
            $id = $this->request('post.id');
            if (is_array($bom_id)) {
                $counts = count($bom_id['id']);
                $i = 0;
                foreach ($bom_id as $value) {
                    $result = $this->fornewbom->data(array('phone_id'=>$id,'bom_id'=>$value))->insert();
                    if($result){
                        $i++;
                    }
                }
                $this->jsonMsg('成功添加了'.$i.'条',1,url('service/edit',array('id'=>$id)));
            }else{
                $this->jsonMsg('你没有选择任何内容');
            }
        } 
    } 

    //撤销
    public function del(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $bom_id = $this->request('get.bom_id',intval);
            $result = $this->fornewbom->where(array('id' =>$id))->delete();
            if($result){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }
    }
}