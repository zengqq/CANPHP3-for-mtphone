<?php
namespace app\fornew\controller;
use framework\ext\Form;
/* *
 * 统计分析
 * */
class countsController extends \app\base\controller\AdminController{
    private $fornew = null;
    private $faulttype = null;
    private $iphone = null;
    private $bom = null;  

    private $producttype = null;  //产品类型
    private $manufacturer = null;  //厂商
    private $unicom = null;   //运营商
    private $supplier = null;   //门店

    public function __construct() {
        parent::__construct();
        $this->fornew = obj('fornew');
        $this->fornewbom = obj('fornewbom');
        $this->faulttype = obj('warehouse/faulttype');
        $this->bom = obj('warehouse/bom');

        $this->producttype = obj('warehouse/producttype');
        $this->manufacturer = obj('warehouse/manufacturer');
        $this->unicom = obj('warehouse/unicom');
        $this->supplier = obj('warehouse/supplier');
        $this->iphone = obj('warehouse/iphone');
    } 

    //列表
    //0:收单 1:换新 2:出库 3:故障确认
    public function index(){
       //搜索表单数据
        $this->supplier_lists = $this->supplier->select();
        //搜索条件
        $condition = array();
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->producttype_id = $this->request('post.producttype_id');
            $this->ems_start_time = $this->request('post.ems_start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->producttype_id = $this->request('get.producttype_id');
            $this->ems_start_time = $this->request('get.ems_start_time');
            $this->ems_end_time = $this->request('get.ems_end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['name'] = $this->number;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->start_time){
            $condition['1'] =  "ems_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->ems_end_time){
            $condition['2'] = "ems_time <=".strtotime($this->ems_end_time);
            $search['ems_end_time'] = $this->ems_end_time;
        }  
        $this->list = $this->fornew->where($condition)->select();
        if($this->list){
            foreach ($this->list as $key => $rs) {
                $counts[0] = $counts[0] + 1;
                if($rs['status'] == 0){  //收单
                    $counts[1] = $counts[1] + 1;
                }
                if($rs['status'] == 1){  //换新
                    $counts[2] = $counts[2] + 1;
                }
                if($rs['status'] == 2){  //出库
                    $counts[3] = $counts[3] + 1;
                }
                if($rs['status'] == 3){  //故障确认
                    $counts[4] = $counts[4] + 1;
                }
            }
        }
        $this->counts = $counts;
        $this->display();
    }

    //报表导出
    public function excel(){
        //搜索表单数据
        $this->supplier_lists = $this->supplier->select();
        //搜索条件
        $condition = array();
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->producttype_id = $this->request('post.producttype_id');
            $this->ems_start_time = $this->request('post.ems_start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->producttype_id = $this->request('get.producttype_id');
            $this->ems_start_time = $this->request('get.ems_start_time');
            $this->ems_end_time = $this->request('get.ems_end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['number'] = $this->number;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->start_time){
            $condition['1'] =  "ems_time >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->ems_end_time){
            $condition['2'] = "ems_time <=".strtotime($this->ems_end_time);
            $search['ems_end_time'] = $this->ems_end_time;
        }
        $this->search = $search;
        $condition['status'] = 3;  
        $page = $this->request('get.page',intval);
        $this->list = $this->fornew->pager($page,20)->select_lists($condition);
        $this->pager = $this->getPage($this->fornew->pager,$search);
        $this->display();
    }

    //汇总表
    public function reads_excel(){
        $this->outexcel();
        $this->layout ='';
        $this->id = $this->request('get.id',intval);
        $this->info = $this->fornew->where(array('id' =>$this->id,'status'=>3))->find();
        $this->list = $this->fornewbom->where(array('phone_id' =>$this->id))->select_lists(array('phone_id' =>$this->id));
        if($this->info['supplier_id']){
            $this->supplier_info = $this->supplier->where(array('id' =>$this->info['supplier_id']))->find();
        }
        $this->display();
    }

    //新录入
    public function toexcel_a(){
        $this->layout ='';
        $this->outexcel();
        $condition = array();
        $condition = $this->search(); 
        $condition['status'] = 0;   
        $this->list = $this->fornew->select_lists($condition);
        $this->unicom_lists = $this->unicom->select();
        $this->display();
    }

    //换新表
    public function toexcel_b(){
        $this->layout ='';
        $this->outexcel();
        $condition = array();
        $condition = $this->search(); 
        $condition['status'] = 1; 
        $info = $this->fornew->select_lists($condition);
        $this->unicom_lists = $this->unicom->select();
        $list = array();
        if($info){
            foreach ($info as $key => $value) {
                $list[$key] = $value;
                if($value['faulttype_id']){
                   $faulttype_id = explode(',',$value['faulttype_id']);
                   foreach ($faulttype_id as $k => $id) {
                       $faulttype_info = $this->faulttype->where(array('id'=>$id))->find();
                       if($faulttype_info){
                            $list[$key]['faulttype_big'][$id]['faulttype_code'] = $faulttype_info['name'];
                            $list[$key]['faulttype_big'][$id]['faulttype_name'] = $faulttype_info['about'];
                       }
                   }
                }
                if($value['faulttype']){
                   $faulttype = explode(',',$value['faulttype']);
                   foreach ($faulttype as $k => $faulttype_id) {
                       $faulttype_info = $this->faulttype->where(array('id'=>$faulttype_id))->find();
                       if($faulttype_info){
                            $list[$key]['faulttype_big'][$faulttype_info['pid']]['faulttype_smalle'][$k]['faulttype_code'] = $faulttype_info['name'];
                            $list[$key]['faulttype_big'][$faulttype_info['pid']]['faulttype_smalle'][$k]['faulttype_name'] = $faulttype_info['about'];            
                       }
                   }
                }
               $mobile_bom = $this->fornewbom->select_lists(array('phone_id' =>$value['id']));
               if($mobile_bom){
                    $list[$key]['mobile_bom'] = $mobile_bom;
               }
            }
        }
        $this->list = $list;
        $this->display();
    }

    //入库表
    public function toexcel_c(){
        $this->layout ='';
        $this->outexcel();
        $condition = array();
        $condition = $this->search(); 
        $condition['status'] = 2; 
        $this->list = $this->fornew->select_lists($condition);
        $this->unicom_lists = $this->unicom->select();
        $this->display();
    }

    //故障表
    public function toexcel_lists(){
        $this->layout ='';
        $this->outexcel();
        $condition = array();
        $condition = $this->search(); 
        $condition['status'] = 3;
        $info = $this->fornew->select_lists($condition);
        $this->unicom_lists = $this->unicom->select();
        $list = array();
        if($info){
            foreach ($info as $key => $value) {
                $list[$key] = $value;
                if($value['faulttype_id']){
                   $faulttype_id = explode(',',$value['faulttype_id']);
                   foreach ($faulttype_id as $k => $id) {
                       $faulttype_info = $this->faulttype->where(array('id'=>$id))->find();
                       if($faulttype_info){
                            $list[$key]['faulttype_big'][$id]['faulttype_code'] = $faulttype_info['name'];
                            $list[$key]['faulttype_big'][$id]['faulttype_name'] = $faulttype_info['about'];
                       }
                   }
                }
                if($value['faulttype']){
                   $faulttype = explode(',',$value['faulttype']);
                   foreach ($faulttype as $k => $faulttype_id) {
                       $faulttype_info = $this->faulttype->where(array('id'=>$faulttype_id))->find();
                       if($faulttype_info){
                            $list[$key]['faulttype_big'][$faulttype_info['pid']]['faulttype_smalle'][$k]['faulttype_code'] = $faulttype_info['name'];
                            $list[$key]['faulttype_big'][$faulttype_info['pid']]['faulttype_smalle'][$k]['faulttype_name'] = $faulttype_info['about'];            
                       }
                   }
                }
               $mobile_bom = $this->fornewbom->select_lists(array('phone_id' =>$value['id']));
               if($mobile_bom){
                    $list[$key]['mobile_bom'] = $mobile_bom;
               }
            }
        }
        $this->list = $list;
        $this->display();
    }   

    protected function search(){
        $condition = array();
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->producttype_id = $this->request('post.producttype_id');
            $this->ems_start_time = $this->request('post.ems_start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->producttype_id = $this->request('get.producttype_id');
            $this->ems_start_time = $this->request('get.ems_start_time');
            $this->ems_end_time = $this->request('get.ems_end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
        }
        if($this->start_time){
            $condition['1'] =  "ems_time >=".strtotime($this->start_time);
        }
        if($this->ems_end_time){
            $condition['2'] = "ems_time <=".strtotime($this->ems_end_time);
        }
        return $condition;
    }
    
    protected function outexcel(){
        header("Content-type:application/octet-stream");
        header("Accept-Ranges:bytes");
        header("Content-type:application/vnd.ms-excel");  
        header("Content-Disposition:attachment;filename=".date('Ymdhis').".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
    }
}