<?php
namespace app\fornew\model;
/* *
 * 换新录入
 * */
class fornewModel extends \app\base\model\AdminModel{
    protected $table = 'phone_fornew';

     //换新联合查询
    public function select_lists($condition = array()){
        return $this->table('phone_fornew as A')
              ->join('{pre}phone_manufacturer as B ON A.manufacturer_id = B.id','left')
              ->join('{pre}phone_producttype as C ON A.producttype_id = C.id','left')
              ->join('{pre}phone_supplier as D ON A.supplier_id = D.id','left')
              ->field('A.*,B.name as manufacturer_number,C.name as producttype_name,D.name as supplier_name')
              ->where($condition)
              ->order('A.id desc')
              ->select();
    }
     //删除上传文件
    public function DeleteFile($path){
        if($path){
            $ImagesPath = ROOT_PATH.$path;
            if(file_exists($ImagesPath)){
                if(!unlink($ImagesPath)){
                    return FALSE;
                }else{
                    $this->delete($condition);
                    return TRUE;
                }
            }else{
                $this->delete($condition);
                return TRUE;
            } 
        }
        return FALSE;
    } 
}