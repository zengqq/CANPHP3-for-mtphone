<?php
namespace app\package\model;
/* *
 * 打包入库
 * */
class packageModel extends \app\base\model\AdminModel{
    protected $table = 'phone_package';

     //打包入库
    public function select_package_lists($condition = array()){
        return $this->table('phone_package as A')
              ->join('{pre}phone_supplier as B ON A.supplier_id = B.id')
              ->field('A.*,B.name as company_number')
              ->where($condition)
              ->order('A.id desc')
              ->select();
    }
    
    //删除上传文件
    public function DeleteFile($path){
        if($path){
            $ImagesPath = ROOT_PATH.$path;
            if(file_exists($ImagesPath)){
                if(!unlink($ImagesPath)){
                    return FALSE;
                }else{
                    $this->delete($condition);
                    return TRUE;
                }
            }else{
                $this->delete($condition);
                return TRUE;
            } 
        }
        return FALSE;
    }
}