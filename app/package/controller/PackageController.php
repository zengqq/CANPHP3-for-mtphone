<?php
namespace app\package\controller;
use framework\ext\Form;
use framework\ext\Check;
/* *
 * 收件入库
 * */
class PackageController extends \app\base\controller\AdminController{
    private $package;
    private $supplier;
    private $manufacturer;
    private $unicom;
    private $region;

    public function __construct() {
        parent::__construct();
        $this->package = obj('package');
        $this->supplier = obj('warehouse/supplier');
        $this->unicom  = obj('warehouse/unicom');
        $this->manufacturer = obj('warehouse/manufacturer');
        $this->region = obj('warehouse/region');
    } 

    //列表
    public function index(){
        $condition = array();
        if($this->isPost()){
            $this->supplier_id     = $this->request('post.supplier_id');
            $this->manufacturer_id = $this->request('post.manufacturer_id');
            $this->unicom_id       = $this->request('post.unicom_id');
            $this->gprs_sn         = $this->request('post.gprs_sn');
            $this->area            = $this->request('post.area');
            $this->types_id        = $this->request('post.types_id');
            $this->ems_sn          = $this->request('post.ems_sn');
            $this->ems_start_time  = $this->request('post.ems_start_time');
            $this->ems_end_time    = $this->request('post.ems_end_time');
        }else{
            $this->supplier_id     = $this->request('get.supplier_id');
            $this->manufacturer_id = $this->request('get.manufacturer_id');
            $this->unicom_id       = $this->request('get.unicom_id');
            $this->gprs_sn         = $this->request('get.gprs_sn');
            $this->area            = $this->request('get.area');
            $this->types_id        = $this->request('get.types_id');
            $this->ems_sn          = $this->request('get.ems_sn');
            $this->ems_start_time  = $this->request('get.ems_start_time');
            $this->ems_end_time    = $this->request('get.ems_end_time');
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id']    = $this->supplier_id;
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
            $search['manufacturer_id']    = $this->manufacturer_id;
        }
        if($this->unicom_id){
            $condition['unicom_id'] = $this->unicom_id;
            $search['unicom_id']    = $this->unicom_id;
        }
        if($this->gprs_sn){
            $condition['gprs_sn'] = $this->gprs_sn;
            $search['gprs_sn']    = $this->gprs_sn;
        }
        if($this->area){
            $condition['area'] = $this->area;
            $search['area']    = $this->area;
        }
        if($this->types_id){
            $condition['types_id'] = $this->types_id == 1 ? 0 : 1;
            $search['types_id']    = $this->types_id == 1 ? 0 : 1;
        }
        if($this->ems_sn){
            $condition['ems_sn'] = $this->ems_sn;
            $search['ems_sn']    = $this->ems_sn;
        }
        if($this->ems_start_time){
            $condition['1']           = "ems_time >=".strtotime($this->ems_start_time);
            $search['ems_start_time'] = $this->ems_start_time;
        }
        if($this->ems_end_time){
            $condition['2']         = "ems_time <=".strtotime($this->ems_end_time);
            $search['ems_end_time'] = $this->ems_end_time;
        } 
        $manufacturer_lists = $this->manufacturer->order('id desc')->select();
        $manufacturer_name = array();
        if($manufacturer_lists){
            foreach ($manufacturer_lists as $key => $value) {
                $manufacturer_name[$value['id']] = $value['name'];
            }
        }
        $unicom_lists  = $this->unicom->order('id desc')->select();
        $unicom_name = array();
        if($unicom_lists){
            foreach ($unicom_lists as $key => $value) {
                $unicom_name[$value['id']] = $value['name'];
            }
        }
        $pageq  = $this->request('get.page',intval);
        $this->pager              = $this->getPage($this->package->pager,$search);
        $this->supplier_lists     = $this->supplier->select();
        $this->list               = $this->package->pager($page,20)->select_package_lists($condition);
        $this->unicom_lists       = $unicom_lists;
        $this->manufacturer_lists = $manufacturer_lists;
        $this->unicom_name        = $unicom_name;
        $this->manufacturer_name  = $manufacturer_name;
        $this->assign('search',$search);
        $this->display();
    }
    //导出
    public function excel(){
        $this->header_excel('logs_');
        $this->layout ='';
        $this->supplier_id     = $this->request('get.supplier_id');
        $this->manufacturer_id = $this->request('get.manufacturer_id');
        $this->unicom_id       = $this->request('get.unicom_id');
        $this->gprs_sn         = $this->request('get.gprs_sn');
        $this->area            = $this->request('get.area');
        $this->types_id        = $this->request('get.types_id');
        $this->ems_sn          = $this->request('get.ems_sn');
        $this->ems_start_time  = $this->request('get.ems_start_time');
        $this->ems_end_time    = $this->request('get.ems_end_time');
        $condition = [];
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
        }
        if($this->unicom_id){
            $condition['unicom_id'] = $this->unicom_id;
        }
        if($this->gprs_sn){
            $condition['gprs_sn'] = $this->gprs_sn;
        }
        if($this->area){
            $condition['area'] = $this->area;
        }
        if($this->types_id){
            $condition['types_id'] = $this->types_id == 1 ? 0 : 1;
        }
        if($this->ems_sn){
            $condition['ems_sn'] = $this->ems_sn;
        }
        if($this->ems_start_time){
            $condition['1']           = "ems_time >=".strtotime($this->ems_start_time);
        }
        if($this->ems_end_time){
            $condition['2']         = "ems_time <=".strtotime($this->ems_end_time);
        } 
        $manufacturer_lists = $this->manufacturer->order('id desc')->select();
        $manufacturer_name = array();
        if($manufacturer_lists){
            foreach ($manufacturer_lists as $key => $value) {
                $manufacturer_name[$value['id']] = $value['name'];
            }
        }
        $unicom_lists  = $this->unicom->order('id desc')->select();
        $unicom_name = array();
        if($unicom_lists){
            foreach ($unicom_lists as $key => $value) {
                $unicom_name[$value['id']] = $value['name'];
            }
        }
        $region_lists  = $this->region->order('id desc')->select();
        $region_name = array();
        if($region_lists){
            foreach ($region_lists as $key => $value) {
                $region_name[$value['id']] = $value['name'];
            }
        }
        $this->list = $this->package->select_package_lists($condition);
        $this->unicom_name       = $unicom_name;
        $this->manufacturer_name = $manufacturer_name;
        $this->region_name       = $region_name;
        $this->display();
    }

    //添加
    public function packageadd(){
        if($this->isPost()){
            $form = new Form('post',array('ems_time'));
            $data = $form->getVal();
            $data['uptime'] = time();
            $data['ems_time'] = strtotime($form->getVal('ems_time'));
            $rel = $this->package->data($data)->insert();
            if($rel){
                $this->jsonMsg('设置成功',1,url('package/index'));
            }else{
                $this->jsonMsg('设置失败');
            }
        }else{
            $this->supplier_lists     = $this->supplier->order('id desc')->select();
            $this->unicom_lists       = $this->unicom->order('id desc')->select();
            $this->manufacturer_lists = $this->manufacturer->order('id desc')->select();
            $this->region_lists = $this->region->order('id desc')->select();
            $this->display();
        }
    }

    //编辑
    public function packageedit(){
        if($this->isPost()){
            $form = new Form('post',array('id','ems_time'));
            $data = $form->getVal();
            $data['uptime'] = time();
            $data['ems_time'] = strtotime($form->getVal('ems_time'));
            $id = intval($form->getVal('id'));
            $condition['id'] = $id;
            $rel = $this->package->where($condition)->data($data)->update();
            if($rel){
                $this->jsonMsg('设置成功',1,url('package/index'));
            }else{
                $this->jsonMsg('设置失败');
            }
        }else{
            $this->supplier_lists     = $this->supplier->order('id desc')->select();
            $this->unicom_lists       = $this->unicom->order('id desc')->select();
            $this->manufacturer_lists = $this->manufacturer->order('id desc')->select();
            $this->region_lists = $this->region->order('id desc')->select();
            $this->id = $this->request('get.id',intval);
            $this->info = $this->package->where(array('id' =>$this->id))->find();
            $this->display();
        }
    }

    //删除
    public function packagedel(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $rel = $this->package->where(array('id' =>$id))->delete();
            if($rel){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }
    }
}