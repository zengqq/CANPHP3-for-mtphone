<?php
namespace app\warehouse\controller;
use framework\ext\Form;
/* *
 * Imei管理
 * */
class ImeiController extends \app\base\controller\AdminController{
    private $imei = null;

    public function __construct() {
        parent::__construct();
        $this->imei = obj('imei');
    } 

    //列表
    public function index(){
        $condition = array();
        if($this->isPost()){
            $this->old_imei = $this->request('post.old_imei');
            $this->new_imei = $this->request('post.new_imei');
        }else{
            $this->old_imei = $this->request('get.old_imei');
            $this->new_imei = $this->request('get.new_imei');
        }
        if($this->old_imei){
            $condition[0] = 'old_imei like "%' . $this->old_imei.'%"';
            $search['old_imei'] = $this->new_imei;
        }
        if($this->new_imei){
            $condition[0] = 'new_imei like "%' . $this->new_imei.'%"';
            $search['new_imei'] = $this->new_imei;
        }
        $types = $this->request('get.types');

        $page = $this->request('get.page',intval);
        $this->list = $this->imei->where($condition)->pager($page,20)->select();
        $this->pager = $this->getPage($this->imei->pager,$search);
        $search['types'] = 1;
        $this->assign('search',$search);
        if($types){
            $this->header_excel('imei_');
            $this->display('app/warehouse/view/imei/excel');
        }else{
            $this->display();
        }
    }
}