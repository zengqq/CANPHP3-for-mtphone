<?php
namespace app\warehouse\controller;
use framework\ext\Form;
/* *
 * 库存管理
 * */
class HouseController extends \app\base\controller\AdminController{
    private $bom;
    private $bomlogs;
    private $place;
    private $manufacturer;
    private $producttype;

    public function __construct() {
        parent::__construct();
        $this->bom          = obj('bom');
        $this->bomlogs      = obj('bomlogs');
        $this->place        = obj('place');
        $this->producttype  = obj('producttype');
        $this->manufacturer = obj('manufacturer');
        $this->place        = obj('place');
    } 

     //编辑
    public function index(){
        $condition = [];
        if($this->isPost()){
            $this->name            = $this->request('post.name');
            $this->code            = $this->request('post.code');
            $this->producttype_id  = $this->request('post.producttype_id');
            $this->manufacturer_id = $this->request('post.manufacturer_id');
            $this->place_id        = $this->request('post.place_id');
        }else{
            $this->name            = $this->request('get.name');
            $this->code            = $this->request('get.code');
            $this->producttype_id  = $this->request('get.producttype_id');
            $this->manufacturer_id = $this->request('get.manufacturer_id');
            $this->place_id        = $this->request('get.place_id');
        }
        if($this->name){
            $condition[] = "name LIKE '%".$this->name."%'";
            $search['name'] = $this->name;
        }
        if($this->code){
            $condition['code'] = $this->code;
            $search['code'] = $this->code;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['name'] = $this->producttype_id;
        }
        if($this->place_id){
            $condition['place_id'] = $this->place_id;
            $search['place_id'] = $this->place_id;
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
            $search['manufacturer_id'] = $this->manufacturer_id;
        }
        $this->types = $this->request('get.types',0,intval);
        $search['types'] = $this->types;
        $this->search = $search;
        $page = $this->request('get.page',intval);
        $condition['types'] = 1;  //不显示禁止操作的维修入库
        $this->list = $this->bom->pager($page,20)->where($condition)->select();
        $this->pager = $this->getPage($this->bom->pager,$search);
        $this->producttype_lists  = $this->producttype->order('id desc')->select();
        $this->manufacturer_lists = $this->manufacturer->order('id desc')->select();
        $this->place_list = $this->place->order('id desc')->select();
        $this->display();
    }  

    //导出
    public function excel(){
        $this->header_excel('house_');
        $condition = array();
        if($this->isPost()){
            $this->product = $this->request('post.product');
            $this->name = $this->request('post.name');
            $this->code = $this->request('post.code');
        }else{
            $this->product = $this->request('post.product');
            $this->name = $this->request('post.name');
            $this->code = $this->request('get.code');
        }
        if($this->name){
            $condition['name'] = $this->name;
        }
        if($this->product){
            $condition['product'] = $this->product;
        }
        if($this->code){
            $condition['code'] = $this->code;
        }
        $this->types = $this->request('get.types',0,intval);
        $this->list = $this->bom->where($condition)->select();
        $this->display(); 
    }

    //增加库存
    public function up(){
        if($this->isPost()){
            $id       = $this->request('post.id',intval);
            $imei     = $this->request('post.imei');
            $name     = $this->request('post.name');
            $place_id = $this->request('post.place_id');
            $amount   = 1;
            if($id){
                $info = $this->bom->where(array('id'=>$id))->field('amount')->find();
                $upamount = intval($info['amount'])+$amount;
                $rel = $this->bom->data(array('amount' =>$upamount))->where(array('id'=>$id))->update();
                if($rel){//增加日志
                    $data['types'] = 1;
                    $data['uptime'] = time();
                    $data['bom_id'] = $id;
                    $data['imei']  = $imei;
                    $data['amount'] = $amount;
                    $data['admin_name'] = $this->userInfo['username'];
                    $data['place_id'] = $place_id;
                    $rel = $this->bomlogs->data($data)->insert();
                }
            }
            if($rel){
                $this->jsonMsg('入库成功',1,url('house/index'));
            }else{
                $this->jsonMsg('入库失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $this->info = $this->bom->where(array('id' =>$this->id))->find();
            $this->place_lists = $this->place->order('id desc')->select();
            $this->display();
        }
    }

    //减少库存
    public function down(){
        if($this->isPost()){
            $id       = $this->request('post.id',intval);
            $imei     = trim($this->request('post.imei'));
            $name     = trim($this->request('post.name'));
            $place_id = $this->request('post.place_id');
            $amount = 1;
            if($id){
                //查找
                $logrel = $this->bomlogs->where(['imei' => $imei])->find();
                if(!$logrel){
                     $this->jsonMsg('IMEI：'.$imei.'物料库存中不存在');
                }
                $check_amount = $this->check_amount($id,$place_id);
                if($check_amount){
                    $down_place_amount = $check_amount-$amount;
                    if($down_place_amount < 0){
                        $this->jsonMsg('对应库位库存不足');
                    }
                }else{
                    $this->jsonMsg('对应库位已没有库存');
                }
                $info = $this->bom->where(array('id'=>$id))->field('amount')->find();
                $downamount = intval($info['amount'])-$amount;
                if($downamount < 0){
                    $this->jsonMsg('剩余总库存不足');
                }
                $rel = $this->bom->data(array('amount' =>$downamount))->where(array('id'=>$id))->update();
                if($rel){//增加日志
                    $data['types'] = 0;
                    $data['uptime'] = time();
                    $data['bom_id'] = $id;
                    $data['imei']  = $imei;
                    $data['amount'] = $amount;
                    $data['admin_name'] = $this->userInfo['username'];
                    $data['down_name'] = $name;
                    $data['place_id'] = $place_id;
                    $rel = $this->bomlogs->data($data)->insert();
                }
            }
            if($rel){
                $this->jsonMsg('出库成功',1,url('house/index'));
            }else{
                $this->jsonMsg('出库失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $this->info = $this->bom->where(array('id' =>$this->id))->find();
            $this->place_lists = $this->place->order('id desc')->select();
            $this->display();
        }
    }

    //检测对应仓库库存
    public function change(){
        if($this->isPost()){
           $uid = $this->request('post.uid');
           $place_id = $this->request('post.place_id');
            if($place_id){
                $rel = $this->bomlogs->where(array('bom_id'=>$uid,'place_id'=>$place_id))->field('amount,types')->order('id desc')->select();
                if($rel){
                    $i = 0;
                    $n = 0;
                    foreach ($rel as $key => $value) {
                        if($value['types'] == 1){
                            $i = $i + $value['amount'];
                        }else{
                            $n = $n + $value['amount'];
                        }
                    }
                    $counts = $i - $n;
                }else{
                    $counts = 0;
                }
            }else{
                $rel = $this->bom->where(array('id'=>$uid))->field('amount')->find();
                $counts = (int)$rel['amount'];
            }
            echo $counts;
        }
    }

    //检测对应仓库库存
    protected function check_amount($uid,$place_id){
        $counts = 0;
        $rel = $this->bomlogs->where(array('bom_id'=>$uid,'place_id'=>$place_id))->field('amount,types')->order('id desc')->select();
        if($rel){
            $i = 0;
            $n = 0;
            foreach ($rel as $key => $value) {
                if($value['types'] == 1){
                    $i = $i + $value['amount'];
                }else{
                    $n = $n + $value['amount'];
                }
            }
            $counts = $i - $n;
        }
        return $counts;
    }
}