<?php
namespace app\warehouse\controller;
use framework\ext\Form;
/* *
 * 故障管理
 * */
class FaulttypeController extends \app\base\controller\AdminController{
    private $faulttype = null;

    public function __construct() {
        parent::__construct();
        $this->faulttype = obj('faulttype');
    } 

    //列表
    public function index(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $result = $this->faulttype->where(array('id' =>$id))->delete();
            if($result){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }else{
            $condition = array();
            if($this->isPost()){
                $this->name = $this->request('post.name');
            }else{
                $this->name = $this->request('get.name');
            }
            if($this->name){
                $condition[0] = 'name like "%' . $this->name.'%"';
            }
            $page = $this->request('get.page',intval);
            $this->list = $this->faulttype->pager($page,20)->select_tree($condition);
            $this->pager = $this->getPage($this->faulttype->pager,$search);
            $this->display();
        }
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $form = new Form('post',array('id','pid'));
            $data = $form->getVal();
            $data['uptime'] = time();
            $id = intval($form->getVal('id'));
            $pid = intval($form->getVal('pid'));
            if($id){
                $condition['id'] = $id;
                $rel = $this->faulttype->where($condition)->data($data)->update();
            }else{
                $data['pid'] = $pid;
                $rel = $this->faulttype->data($data)->insert();
            }
            if($rel){
                $this->jsonMsg('设置成功',1,url('faulttype/index'));
            }else{
                $this->jsonMsg('设置失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $this->pid = $this->request('get.pid',intval);
            $this->info = $this->faulttype->where(array('id' =>$this->id))->find();
            $this->display();
        }
    }
}