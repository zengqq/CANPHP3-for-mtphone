<?php
namespace app\warehouse\controller;
use framework\ext\Form;
/* *
 * 库位管理
 * */
class PlaceController extends \app\base\controller\AdminController{
    private $place = null;

    public function __construct() {
        parent::__construct();
        $this->place = obj('place');
    } 

    //列表
    public function index(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $result = $this->place->where(array('id' =>$id))->delete();
            if($result){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }else{
            $condition = array();
            if($this->isPost()){
                $this->name = $this->request('post.name');
            }else{
                $this->name = $this->request('get.name');
            }
            if($this->name){
                $condition[0] = 'name like "%' . $this->name.'%"';
                $search['name'] = $this->name;
            }
            $page = $this->request('get.page',intval);
            $this->list = $this->place->where($condition)->pager($page,20)->select();
            $this->pager = $this->getPage($this->place->pager,$search);
            $this->display();
        }
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $form = new Form('post',array('id'));
            $data = $form->getVal();
            $data['uptime'] = time();
            $id = intval($form->getVal('id'));
            if($id){
                $condition['id'] = $id;
                $rel = $this->place->where($condition)->data($data)->update();
            }else{
                $rel = $this->place->data($data)->insert();
            }
            if($rel){
                $this->jsonMsg('设置成功',1,url('place/index'));
            }else{
                $this->jsonMsg('设置失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $this->info = $this->place->where(array('id' =>$this->id))->find();
            $this->display();
        }
    }
}