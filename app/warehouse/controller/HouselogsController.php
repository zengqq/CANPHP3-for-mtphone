<?php
namespace app\warehouse\controller;
use framework\ext\Form;
/* *
 * 库存记录
 * */
class HouselogsController extends \app\base\controller\AdminController{
    private $bom;
    private $bomlogs;
    private $place;
    private $producttype;
    private $manufacturer;

    public function __construct() {
        parent::__construct();
        $this->bom          = obj('bom');
        $this->bomlogs      = obj('bomlogs');
        $this->place        = obj('place');
        $this->producttype  = obj('producttype');
        $this->manufacturer = obj('manufacturer');
    } 

     //入库记录
    public function up(){
        $condition = array();
        $condition['A.types'] = 1;
        if($this->isPost()){
            $this->manufacturer_id = $this->request('post.manufacturer_id');
            $this->producttype_id  = $this->request('post.producttype_id');
            $this->place_id        = $this->request('post.place_id');
            $this->name            = $this->request('post.name');
            $this->code            = $this->request('post.code');
            $this->start_time      = $this->request('post.start_time');
            $this->end_time        = $this->request('post.end_time');
            $this->place_id        = $this->request('post.place_id');
        }else{
            $this->manufacturer_id = $this->request('get.manufacturer_id');
            $this->producttype_id  = $this->request('get.producttype_id');
            $this->place_id        = $this->request('get.place_id');
            $this->name            = $this->request('get.name');
            $this->code            = $this->request('get.code');
            $this->start_time      = $this->request('get.start_time');
            $this->end_time        = $this->request('get.end_time');
            $this->place_id        = $this->request('get.place_id');
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
            $search['manufacturer_id'] = $this->manufacturer_id;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->place_id){
            $condition['A.place_id'] = $this->place_id;
            $search['place_id']    = $this->place_id;
        }
        if($this->name){
            $search['name'] = $this->name;
            $condition['0'] = "B.name LIKE '%".$this->name."%'";
        }
        if($this->code){
            $condition['code'] = $this->code;
            $search['code'] = $this->code;
        }

        if($this->start_time){
            $condition['1'] =  "A.uptime >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "A.uptime <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }  
        $page = $this->request('get.page',intval);
        $this->list = $this->bomlogs->pager($page,20)->select_logs($condition);
        $this->pager = $this->getPage($this->bomlogs->pager,$search);
        $this->place_list = $this->place->order('id desc')->select();
        $this->producttype_lists = $this->producttype->order('id desc')->select();
        $this->manufacturer_lists = $this->manufacturer->order('id desc')->select();
        $this->search = $search;
        $this->display();
    } 

     //出库记录
    public function down(){
        $condition = array();
        $condition['A.types'] = 0;
        if($this->isPost()){
            $this->manufacturer_id = $this->request('post.manufacturer_id');
            $this->producttype_id  = $this->request('post.producttype_id');
            $this->place_id        = $this->request('post.place_id');
            $this->down_name  = $this->request('post.down_name');
            $this->name       = $this->request('post.name');
            $this->code       = $this->request('post.code');
            $this->start_time = $this->request('post.start_time');
            $this->end_time   = $this->request('post.end_time');
        }else{
            $this->manufacturer_id = $this->request('get.manufacturer_id');
            $this->producttype_id  = $this->request('get.producttype_id');
            $this->place_id        = $this->request('get.place_id');
            $this->down_name  = $this->request('post.down_name');
            $this->name       = $this->request('post.name');
            $this->code       = $this->request('get.code');
            $this->start_time = $this->request('get.start_time');
            $this->end_time   = $this->request('get.end_time');
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
            $search['manufacturer_id'] = $this->manufacturer_id;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
            $search['producttype_id'] = $this->producttype_id;
        }
        if($this->place_id){
            $condition['A.place_id'] = $this->place_id;
            $search['place_id']    = $this->place_id;
        }
        if($this->down_name){
            $condition['down_name'] = $this->down_name;
            $search['down_name'] = $this->down_name;
        }
        if($this->name){
            $condition['0'] = "B.name LIKE '%".$this->name."%'";
            $search['name'] = $this->name;
        }
        if($this->code){
            $condition['code'] = $this->code;
            $search['code'] = $this->code;
        }
        if($this->start_time){
            $condition['1'] =  "A.uptime >=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "A.uptime <=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }
        $page = $this->request('get.page',intval);
        $this->list = $this->bomlogs->pager($page,20)->select_logs($condition);
        $this->pager = $this->getPage($this->bomlogs->pager,$search);
        $this->place_list = $this->place->order('id desc')->select();
        $this->producttype_lists = $this->producttype->order('id desc')->select();
        $this->manufacturer_lists = $this->manufacturer->order('id desc')->select();
        $this->search = $search;
        $this->display();
    }


    //列表
    public function del(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $result = $this->bomlogs->where(array('id' =>$id))->delete();
            if($result){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }
    }

    //导出
    public function downexcel(){
        $this->header_excel('logs_');
        $condition = array();
        $condition['A.types'] = 0;
        $this->down_name = $this->request('get.down_name');
        $this->manufacturer_id = $this->request('get.manufacturer_id');
        $this->producttype_id  = $this->request('get.producttype_id');
        $this->place_id        = $this->request('get.place_id');
        $this->name = $this->request('get.name');
        $this->code = $this->request('get.code');
        $this->start_time = $this->request('get.start_time');
        $this->end_time = $this->request('get.end_time');
        if($this->down_name){
            $condition['down_name'] = $this->down_name;
        }
        if($this->name){
            $condition['0'] = "name LIKE '%".$this->name."%'";
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
        }
        if($this->place_id){
            $condition['A.place_id'] = $this->place_id;
        }
        if($this->code){
            $condition['code'] = $this->code;
        }
        if($this->start_time){
            $condition['1'] =  "A.uptime >=".strtotime($this->start_time);
        }
        if($this->end_time){
            $condition['2'] = "A.uptime <=".strtotime($this->end_time);
        }   
        $page = $this->request('get.page',intval);
        $this->list = $this->bomlogs->pager($page,20)->select_logs($condition);
        $this->display();
    }

    //导出
    public function upexcel(){
        $this->header_excel('logs_');
        $condition = array();
        $condition['A.types'] = 1;
        $this->manufacturer_id = $this->request('get.manufacturer_id');
        $this->producttype_id  = $this->request('get.producttype_id');
        $this->place_id        = $this->request('get.place_id');
        $this->name            = $this->request('get.name');
        $this->code            = $this->request('get.code');
        $this->start_time      = $this->request('get.start_time');
        $this->end_time        = $this->request('get.end_time');
        $this->down_name = $this->request('get.down_name');
        if($this->down_name){
            $condition['down_name'] = $this->down_name;
        }
        if($this->name){
            $condition['0'] = "name LIKE '%".$this->name."%'";
        }
        if($this->manufacturer_id){
            $condition['manufacturer_id'] = $this->manufacturer_id;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
        }
        if($this->place_id){
            $condition['A.place_id'] = $this->place_id;
        }
        if($this->code){
            $condition['code'] = $this->code;
        }
        if($this->start_time){
            $condition['1'] =  "A.uptime >=".strtotime($this->start_time);
        }
        if($this->end_time){
            $condition['2'] = "A.uptime <=".strtotime($this->end_time);
        }  
        $page = $this->request('get.page',intval);
        $this->list = $this->bomlogs->pager($page,20)->select_logs($condition);
        $this->display();
    }
}