<?php
namespace app\warehouse\controller;
use framework\ext\Form;
/* *
 * BOM管理
 * */
class BomController extends \app\base\controller\AdminController{
    private $bom;
    private $iphone;
    private $manufacturer;
    private $producttype;
    private $place;

    public function __construct() {
        parent::__construct();
        $this->bom          = obj('bom');
        $this->producttype  = obj('producttype');
        $this->manufacturer = obj('manufacturer');
        $this->iphone       = obj('iphone');
        $this->place        = obj('place');
    } 

    //物料详细列表
    public function index(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $result = $this->bom->where(array('id' =>$id))->delete();
            if($result){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }else{
            $condition['types'] = 0;
            if($this->isPost()){
                $this->manufacturer_id = $this->request('post.manufacturer_id');
                $this->producttype_id  = $this->request('post.producttype_id');
                $this->name            = $this->request('post.name');
                $this->code            = $this->request('post.code');
            }else{
                $this->manufacturer_id = $this->request('get.manufacturer_id');
                $this->producttype_id  = $this->request('get.producttype_id');
                $this->name            = $this->request('get.name');
                $this->code            = $this->request('get.code');
            }
            if($this->manufacturer_id){
                $condition['manufacturer_id'] = $this->manufacturer_id;
                $search['manufacturer_id']    = $this->manufacturer_id;
            }
            if($this->producttype_id){
                $condition['producttype_id'] = $this->producttype_id;
                $search['producttype_id']    = $this->producttype_id;
            }
            if($this->name){
                $condition['0'] = "name LIKE '%".$this->name."%'";
				$search['name'] = $this->name;
            }

            if($this->code){
                $condition['code'] = $this->code;
                $search['code'] = $this->code;
            }
            $page = $this->request('get.page',intval);
            $this->list = $this->bom->pager($page,20)->where($condition)->order('id desc')->select();
            $this->pager = $this->getPage($this->bom->pager,$search);
            $producttype_lists  = $this->producttype->select();
            $producttype_name = array();
            if($producttype_lists){
                foreach ($producttype_lists as $key => $value) {
                    $producttype_name[$value['id']] = $value['name'];
                }
            }
            $manufacturer_lists = $this->manufacturer->select();
            $manufacturer_name = array();
            if($manufacturer_lists){
                foreach ($manufacturer_lists as $key => $value) {
                    $manufacturer_name[$value['id']] = $value['name'];
                }
            }
            $this->assign('producttype_lists',$producttype_lists);
            $this->assign('manufacturer_lists',$manufacturer_lists);
            $this->assign('producttype_name',$producttype_name);
            $this->assign('manufacturer_name',$manufacturer_name);
            $this->place_list = $this->place->order('id desc')->select();
            $this->display();
        }
    }

    //添加编辑物料
    public function edit(){
        if($this->isPost()){
            $form = new Form('post',array('id'));
            $id = intval($form->getVal('id'));
            $data['uptime'] = time();
            $data['manufacturer_id'] = $form->getVal('manufacturer_id');
            $data['producttype_id']  = $form->getVal('producttype_id');
            $data['code']            = $form->getVal('code');
            $data['name']            = $form->getVal('name');
            $data['warning']         = $form->getVal('warning');
            $data['parameter']       = $form->getVal('parameter');
            if($data['producttype_id']){
                $producttype_info  = $this->producttype->where(['id' => $data['producttype_id']])->field('id,name')->find(); //产品名称
            }
            $data['product'] = $producttype_info['name'];
            if($data['manufacturer_id']){
                $manufacturer_info  = $this->manufacturer->where(['id' => $data['manufacturer_id']])->field('id,name')->find(); //产品
            }
            $data['manufacturer'] = $manufacturer_info['name'];
            if($id){
                $condition['id'] = $id;
                $rel = $this->bom->where($condition)->data($data)->update();
            }else{
                $rel = $this->bom->data($data)->insert();
            }
            if($rel){
                $this->jsonMsg('成功',1,url('bom/index'));
            }else{
                $this->jsonMsg('失败');
            }
        }else{
            $this->producttype_lists  = $this->producttype->order('id desc')->select();
            $this->manufacturer_lists = $this->manufacturer->order('id desc')->select();
            $this->place_lists = $this->place->order('id desc')->select();
            $this->id = $this->request('get.id',intval);
            $this->info = $this->bom->where(array('id' =>$this->id))->find();
            $this->display();
        }
    }
    //########################
    //手机管理列表
    public function iphone(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $result = $this->iphone->where(array('id' =>$id))->delete();
            if($result){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }else{
            $condition = array();
            if($this->isPost()){
                $this->name = $this->request('post.name');
                $this->code = $this->request('post.code');
            }else{
                $this->name = $this->request('post.name');
                $this->code = $this->request('get.code');
            }
            if($this->name){
                $condition['name'] = $this->name;
                $search['0'] = "name LIKE '%".$this->name."%'";
            }
            if($this->code){
                $condition['code'] = $this->code;
                $search['code'] = $this->code;
            }
            $page = $this->request('get.page',intval);
            $this->list = $this->iphone->pager($page,20)->where($condition)->select();
            $this->pager = $this->getPage($this->bom->iphone,$search);
            $this->display();
        }
    }

    //添加编辑手机中的物料
    public function iphoneedit(){
        if($this->isPost()){
            $form = new Form('post',array('id'));
            $data = $form->getVal();
            $data['uptime'] = time();
            $id = intval($form->getVal('id'));
            if($id){
                $condition['id'] = $id;
                $rel = $this->iphone->where($condition)->data($data)->update();
            }else{
                $rel = $this->iphone->data($data)->insert();
            }
            if($rel){
                $this->jsonMsg('设置成功',1,url('bom/iphone'));
            }else{
                $this->jsonMsg('设置失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $this->info = $this->iphone->where(array('id' =>$this->id))->find();
            $this->display();
        }
    }

    //添加手机包含的物料
    public function add_bom(){
        $this->id = $this->request('get.id',intval);
        $page = $this->request('get.page',intval);
        $condition = array();
        if($this->isPost()){
            $this->producttype_id = $this->request('post.producttype_id');
            $this->name = $this->request('post.name');
            $this->code = $this->request('post.code');
        }else{
            $this->producttype_id = $this->request('post.producttype_id');
            $this->name = $this->request('post.name');
            $this->code = $this->request('get.code');
        }
        if($this->name){
            $condition['name'] = $this->name;
        }
        if($this->producttype_id){
            $condition['producttype_id'] = $this->producttype_id;
        }
        if($this->code){
            $condition['code'] = $this->code;
        }
        $this->list = $this->bom->where($condition)->pager($page,10)->select();
        $bom_list = $this->iphone->phone_bom_phone_lists_select(array('bom_phone_id' =>$this->id));
        $bom_id = array();
        $number_id = array();
        if($bom_list){
            foreach ($bom_list as $key => $value) {
                $bom_id[] = $value['bom_id'];
                $number_id[$value['bom_id']] = $value['number'];
            }
        }
        $this->bom_list = $bom_id;
        $this->number_id = $number_id;
        $this->pager = $this->getPage($this->bom->pager,$condition);
        $this->producttype_lists  = $this->producttype->order('id desc')->select();
        $this->display();
    }

    //列表
    public function add_bom_save(){
        $condition = array();
        if($this->isPost()){
            $bom_id = $this->request('post.bom_id');
            $number = $this->request('post.number');
            $id = $this->request('post.id');
            if (is_array($bom_id)) {
                foreach ($bom_id as $value) {
                    $rel = $this->iphone->phone_bom_phone_lists_find(array('bom_phone_id'=>$id,'bom_id'=>$value));
                    if($rel){
                        $result = $this->iphone->phone_bom_phone_lists_update(array('bom_phone_id'=>$id,'bom_id'=>$value),array('number'=>$number[$value]));
                        if($result){
                            $i++;
                        }
                    }else{
                        $result = $this->iphone->phone_bom_phone_lists_insert(array('bom_phone_id'=>$id,'bom_id'=>$value,'number'=>$number[$value]));
                        if($result){
                            $i++;
                        }
                    }
                }
                $this->jsonMsg('成功添加了'.$i.'条',1,url('bom/reads',array('id'=>$id)));
            }else{
                $this->jsonMsg('你没有选择任何内容');
            }
        } 
    } 

    //列表
    public function reads(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $result = $this->iphone->bom_lists_delete(array('id' =>$id));
            if($result){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $page = $this->request('get.page',intval);
            $this->info = $this->iphone->where(array('id'=>$this->id))->find();
            $this->list = $this->iphone->pager($page,20)->bom_lists_select(array('bom_phone_id' =>$this->id));
            $this->pager = $this->getPage($this->iphone->pager,array('id'=>$this->id));
            $this->display();
        }
    }
}