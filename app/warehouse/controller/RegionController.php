<?php
namespace app\warehouse\controller;
use framework\ext\Form;
/* *
 * 区域
 * */
class RegionController extends \app\base\controller\AdminController{
    private $unicom = null;

    public function __construct() {
        parent::__construct();
        $this->region = obj('region');
    } 

    //列表
    public function index(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $result = $this->region->where(array('id' =>$id))->delete();
            if($result){
                $this->jsonMsg('删除成功',1);
            }else{
                $this->jsonMsg('删除失败');
            }
        }else{
            $condition = array();
            if($this->isPost()){
                $this->name = $this->request('post.name');
            }else{
                $this->name = $this->request('get.name');
            }
            if($this->name){
                $condition[0] = 'name like "%' . $this->name.'%"';
                $search['name'] = $this->name;
            }
            $page = $this->request('get.page',intval);
            $this->list = $this->region->where($condition)->pager($page,20)->select();
            $this->pager = $this->getPage($this->region->pager,$search);
            $this->display();
        }
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $form = new Form('post',array('id'));
            $data = $form->getVal();
            $data['uptime'] = time();
            $id = intval($form->getVal('id'));
            if($id){
                $condition['id'] = $id;
                $rel = $this->region->where($condition)->data($data)->update();
            }else{
                $rel = $this->region->data($data)->insert();
            }
            if($rel){
                $this->jsonMsg('设置成功',1,url('region/index'));
            }else{
                $this->jsonMsg('设置失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $this->info = $this->region->where(array('id' =>$this->id))->find();
            $this->display();
        }
    }
}