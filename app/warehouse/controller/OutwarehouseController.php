<?php
namespace app\warehouse\controller;
use framework\ext\Form;
/* *
 * 维修报表
 * */
class OutwarehouseController extends \app\base\controller\AdminController{
    private $mobile = null;
    private $supplier = null;   //门店

    public function __construct() {
        parent::__construct();
        $this->mobile = obj('repair/mobile');
        $this->supplier = obj('supplier');
    } 

    //列表
    public function index(){
        $condition = array();
        $condition['status'] = 3;
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->mobile_name = $this->request('post.mobile_name');
            $this->start_time = $this->request('post.start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->mobile_name = $this->request('get.mobile_name');
            $this->start_time = $this->request('get.start_time');
            $this->end_time = $this->request('get.end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['name'] = $this->number;
        }
        if($this->mobile_name){
            $condition['0'] = "mobile_name LIKE '%".$this->mobile_name."%'";
            $search['mobile_name'] = $this->mobile_name;
        }
        if($this->start_time){
            $condition['1'] =  "out_time <=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "out_time >=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }     
        $page = $this->request('get.page',intval);
        $this->list = $this->mobile->pager($page,20)->select_supplier_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        $this->supplier_lists = $this->supplier->select();
        $this->display();
    }

    //列表
    public function logs(){
        $condition = array();
        $condition['status'] = 4;
        if($this->isPost()){
            $this->imei = $this->request('post.imei');
            $this->supplier_id = $this->request('post.supplier_id');
            $this->number = $this->request('post.number');
            $this->mobile_name = $this->request('post.mobile_name');
            $this->start_time = $this->request('post.start_time');
            $this->end_time = $this->request('post.end_time');
        }else{
            $this->imei = $this->request('get.imei');
            $this->supplier_id = $this->request('get.supplier_id');
            $this->number = $this->request('get.number');
            $this->mobile_name = $this->request('get.mobile_name');
            $this->start_time = $this->request('get.start_time');
            $this->end_time = $this->request('get.end_time');
        }
        if($this->imei){
            $condition['imei'] = $this->imei;
            $search['imei'] = $this->imei;
        }
        if($this->supplier_id){
            $condition['supplier_id'] = $this->supplier_id;
            $search['supplier_id'] = $this->supplier_id;
        }
        if($this->number){
            $condition['number'] = $this->number;
            $search['name'] = $this->number;
        }
        if($this->mobile_name){
            $condition['0'] = "mobile_name LIKE '%".$this->mobile_name."%'";
            $search['mobile_name'] = $this->mobile_name;
        }
        if($this->start_time){
            $condition['1'] =  "testing_time <=".strtotime($this->start_time);
            $search['start_time'] = $this->start_time;
        }
        if($this->end_time){
            $condition['2'] = "testing_time >=".strtotime($this->end_time);
            $search['end_time'] = $this->end_time;
        }   
        $page = $this->request('get.page',intval);
        $this->list = $this->mobile->pager($page,20)->select_supplier_lists($condition);
        $this->pager = $this->getPage($this->mobile->pager,$search);
        $this->supplier_lists = $this->supplier->select();
        $this->display();
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $data = array();
            $form = new Form('post',array('id'));
            $data = $form->getVal();
            $data['status'] = 4;
            $data['out_time'] = time();
            $data['outwarehouse_admin_id'] = $this->userInfo['id'];
            $id = $this->request('post.id');
            $condition['id'] = $id;
            $rel = $this->mobile->where($condition)->data($data)->update();
            if($rel){
                $this->jsonMsg('提交发货单成功',1,url('outwarehouse/index'));
            }else{
                $this->jsonMsg('提交发货单失败');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $condition['id'] = $this->id;
            $this->info = $this->mobile->where($condition)->field('come_address')->find();
            $this->display();
        }
    } 
}