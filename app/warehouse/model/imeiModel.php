<?php
namespace app\warehouse\model;
/* *
 * imei管理
 * */
class imeiModel extends \app\base\model\AdminModel{
    protected $table = 'phone_imei';
 
    //判断imei号是否存在
    public function is_imei($imei){
        $condition['new_imei'] = $imei;
        return $this->where($condition)->find();
    }

    //新增IMEI号
    public function add_imei($data){
        $data['uptime'] = time();
        return $this->data($data)->insert();
    }
}