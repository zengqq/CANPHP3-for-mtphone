<?php
namespace app\warehouse\model;
use framework\ext\Category;
/* *
 * 运营商管理
 * */
class faulttypeModel extends \app\base\model\AdminModel{
    protected $table = 'phone_faulttype';

    //无限极查询
    public function select_tree($condition = array()){
        $rel = $this->where($condition)->order('id desc')->select();
        $Category = new Category(array('id','pid','name','cname'));
        return $Category->getTree($rel,0);
    }

    //故障多选
    public function select_all($condition = array()){
        $rel = $this->where(array('pid' => 0))->order('id desc')->select();
        if($rel){
            foreach ($rel as $key => $value) {
                $rel[$key] = $value;
                $list = $this->where(array('pid' => $value['id']))->order('id desc')->select();
                if($list){
                    $rel[$key]['list'] = $list;
                }
            }
        }else{
            return array();
        }
        return $rel;
    }
}