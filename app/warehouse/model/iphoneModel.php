<?php
namespace app\warehouse\model;
/* *
 * BOM管理
 * */
class iphoneModel extends \app\base\model\AdminModel{
    protected $table = 'phone_bom_phone';
 
    //包含的BOM
    public function bom_lists_select($condition = array()){
        return $this->table('phone_bom as A')
              ->join('{pre}phone_bom_phone_lists as B ON A.id = B.bom_id')
              ->field('A.*,B.bom_phone_id,B.id as bom_id,B.number as bom_number')
              ->where($condition)
              ->limit($limit)
              ->order('A.id desc')
              ->select();
    }
    
    //添加包含配件
    public function phone_bom_phone_lists_insert($data = array()){
        return $this->table('phone_bom_phone_lists')->data($data)->insert();
    }

    //修改包含配件
    public function phone_bom_phone_lists_update($condition = array(),$data = array()){
        return $this->table('phone_bom_phone_lists')->where($condition)->data($data)->update();
    }

    //已添加配件ID
    public function phone_bom_phone_lists_select($condition = array()){
        return $this->table('phone_bom_phone_lists')->where($condition)->select();
    } 

    //搜索是否已选择
    public function phone_bom_phone_lists_find($condition = array()){
        return $this->table('phone_bom_phone_lists')->where($condition)->find();
    }  

    //删除
    public function bom_lists_delete($condition = array()){
        return $this->table('phone_bom_phone_lists')->where($condition)->delete();
    }  
}