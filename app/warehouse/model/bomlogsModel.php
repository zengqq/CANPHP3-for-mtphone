<?php
namespace app\warehouse\model;
/* *
 * BOM记录
 * */
class bomlogsModel extends \app\base\model\AdminModel{
    protected $table = 'phone_bom_logs';

    //包含的BOM
    public function select_logs($condition = array()){
        return $this->table('phone_bom_logs as A')
              ->join('{pre}phone_bom as B ON A.bom_id = B.id')
              ->join('{pre}phone_place as C ON A.place_id = C.id')
              ->field('A.*,A.amount as logs_amount,B.product,B.manufacturer,B.code,B.name,B.parameter,C.name as place_name')
              ->where($condition)
              ->order('A.id desc')
              ->select();
    }
}