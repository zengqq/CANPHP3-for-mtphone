<?php
namespace app\admin\controller;
use framework\ext\Form;
/* *
 * 模块管理
 * */
class AppController extends \app\base\controller\AdminController{
    private $app = null;

    public function __construct() {
        parent::__construct();
        $this->app = obj('App');
    } 

    //首页
    public function index(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            $subclass = $this->app->where(array('pid'=>$id))->find(); 
            if($subclass){
                $this->jsonMsg('Error:Please delete content');
            }
            $result = $this->app->where(array('id'=>$id))->delete();
            if($result){
                $this->jsonMsg('Success:Delete success',1);
            }else{
                $this->jsonMsg('Error:Delete failed');
            }
        }else{
            $this->pid = $this->request('get.pid',intval);
            $this->list  = $this->app->select_tree($this->pid);
            $this->path = $this->app->path($this->pid,'app/index');
            $this->display();
        }
    }
    //编辑添加
    public function edit(){
        if($this->isPost()){
            $form = new Form('post',array('id'));
            $data = $form->getVal();
            $data['uptime'] = time();
           //获取根ID开始
            if($data['pid'] == 0){
                $data['rootid'] = 0;
            }else{
                $root = $this->app->where(array('id'=>$data['pid']))->field('id,pid,rootid')->find();
                if($root['pid'] == 0){
                    $data['rootid'] = $root['id'];
                }else{
                    $data['rootid'] = $root['rootid'];
                }
            }
            $id  = intval($form->getVal('id'));
            if($id){
                $result = $this->app->data($data)->where(array('id'=>$id))->update();
            }else{
                $result = $this->app->data($data)->insert();
            }
            if($result){
                $this->jsonMsg('Success:editor success',1,url('app/index',array('pid'=>$data['rootid']))); 
            }else{
                $this->jsonMsg('Error:editor failed');
            }
        }else{
            $this->types = $this->request('get.types',intval);
            $this->id = $this->request('get.id',intval);
            $this->pid = $this->request('get.pid',intval);
            $this->info = $this->app->where(array('id' =>$this->id))->find();
            if(!$this->info){
                $this->rel = $this->app->where(array('id' =>$this->pid))->find();
            }
            $this->display();
        }
    }
}