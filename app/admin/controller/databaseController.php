<?php
namespace app\admin\controller;
use framework\ext\Http;
/* *
 * 数据库备份
 * */
class databaseController extends \app\base\controller\AdminController{
    private $database = null;

    public function __construct() {
        parent::__construct();
        $this->database = obj('database');
    }  

    //模板列表
    public function index(){
        $this->list = $this->database->dbList();
        $this->display();  
    } 

    //获取所有表
    public function tables(){
        $this->list = $this->database->DBTableList();
        $this->display();  
    }    

    //数据库备份
    public function backupdb(){
        $rel = $this->database->backup();
        if($rel == 1){
            $this->pageMsg('数据库备份操作执行完毕！',1);
        }else{
            $this->pageMsg($rel);
        }
    } 
    //数据库优化
    public function optimizeddb(){
        $rel = $this->database->optimized();
        if($rel == 1){
            $this->pageMsg('数据库数据库优化操作执行完毕！',1);
        }else{
            $this->pageMsg($rel);
        }
        
    } 
    //数据库修复
    public function repairdb(){
        $rel = $this->database->repair();
        if($rel == 1){
            $this->pageMsg('数据库修复操作执行完毕！',1);
        }else{
            $this->pageMsg($rel);
        } 
    } 

    //数据库恢复
    public function importdb(){
        $DBBakFiles = $this->request('get.name');
        if(empty($DBBakFiles)){
            $this->pageMsg('参数不能为空！');
        }
        //获取备份数量
        if($this->database->import($DBBakFiles)){
            $this->pageMsg('备份恢复成功！',1);
        }else{
            $rel = $this->database->DBError;
            if(empty($rel)){
                $this->pageMsg('备份恢复失败！');
            }else{
                $this->pageMsg($rel);
            }
        }
    }

    //删除备份
    public function delectdb(){
        $act = $this->request('get.act');
        $name =$this->request('get.name');
        if(empty($act) || empty($name)){
            $this->jsonMsg('参数不能为空！');
        }
        if($this->database->DelectDB($name)){
            $this->jsonMsg('备份文件删除完毕！',1);
        }else{
            $this->jsonMsg('删除失败');
        }
        
    }

    //下载备份
    public function downdb(){
        $name = $this->request('get.name');
        if(empty($name)){
            $this->jsonMsg('参数不能为空！');
        }
        $rel = $this->database->downdb($name);
        if($rel){
            Http::download($rel['path'],$rel['name']);
        }else{
            $this->jsonMsg('备份文件不存在');
        }
    }
}