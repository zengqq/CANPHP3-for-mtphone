<?php
namespace app\admin\controller;
use framework\ext\Form;
/* *
 * 权限组
 * */
class GroupController extends \app\base\controller\AdminController{
    private $group = null;
    private $app = null;
    private $user = null;

    public function __construct() {
        parent::__construct();
        $this->group = obj('group');
        $this->app = obj('app');
        $this->user = obj('user');
    } 

	//用户列表
	public function index(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            if($id == 1){
                $this->jsonMsg('No locking administrator');
            } 
            $rel = $this->user->where(array('group_id'=>$id))->find();
            if(!$rel){
                $result = $this->group->where(array('id'=>$id))->delete();
            }
            if($result){
                $this->jsonMsg('Success:Delete success',1);
            }else{
                $this->jsonMsg('Error:Delete failed');
            } 
        }else{
            $page = $this->request('get.page',intval);
            $this->rel = $this->group->pager($page,20)->where(array(0=>'id > 1'))->select();
            $this->pager = $this->getPage($this->group->pager);
            $this->display();
        }
	}

    //添加或编辑用户
    public function edit(){
        if($this->isPost()){
            $form = new Form('post',array('id','auth','menu'));
            $data = $form->getVal();
            $data['uptime'] = time();
            $id = intval($form->getVal('id'));
            $auth = $form->getVal('auth');
            if(is_array($auth)){
                $data['auth_value'] = json_encode($auth);
            }
            if($id){
                $rel = $this->group->_update($data,array('id'=>$id));
            }else{    
                $rel = $this->group->_update($data);
            }
            if($rel){
                $this->jsonMsg('Success:edit success',1,url('group/index'));
            }else{
                $this->jsonMsg('Error:edit failed');
            }
        }else{
            $this->pid = $this->request('get.pid',intval);
            $this->id = $this->request('get.id',intval);
            $this->list  = $this->app->group_select_tree($this->pid);
            $this->info = $this->group->where(array('id'=>$this->id))->find();
            $this->auth = array();
            if($this->info['auth_value']){
                $this->auth = json_decode($this->info['auth_value'],1);
            }
            $this->display();
        }
    }
}