<?php
namespace app\admin\controller;
use framework\ext\Http;
/* *
 * 认证查询
 * */
class ClientController extends \app\base\controller\AdminController{
    private $auth = null;

    //检查版本
    public function version(){
        $cof = config('admin');
        $data['app'] = $cof['APP_NAME'];
        $rel = Http::doPost($this->auth().'/apps/server/version',$data,6);
        $str = $rel?$rel:'<span class="red fn-f14">检测最新版本超时,请稍后重试。</span>';
        echo $str;
    } 

     //查询授权
    public function vip(){
        $cof = config('admin');
        $data['app'] = $cof['APP_NAME'];
        $data['url'] = COME_URL;
        $rel = Http::doPost($this->auth().'/apps/server/vip',$data,6);
        $str = $rel?$rel:'<span class="red fn-f14">检测商业用户超时,请稍后重试。</span>';
        echo $str;
    } 

    //通知推送
    public function notice(){
        $rel = Http::doGet($this->auth().'/apps/server/notice',6);
        $str = $rel?$rel:'<span class="red fn-f14">获取官方推送信息超时,请稍后重试。</span>';
        echo $str;
    }  
}