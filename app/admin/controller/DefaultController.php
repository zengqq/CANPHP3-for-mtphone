<?php
namespace app\admin\controller;
use framework\ext\Form;
/* *
 * 后台UI首页
 * */
class DefaultController extends \app\base\controller\AdminController{
    private $app = null;
    private $group = null;
    private $default = null;
    private $user = null;

    public function __construct() {
        parent::__construct();
        $this->app = obj('app');
        $this->default = obj('default');
        $this->user = obj('user');
        $this->group = obj('group');
    } 
    //首页
	public function index(){
        $this->nav = $this->app->top_select($this->group_id);
        $this->groupInfo = $this->group->field('name')->where(array('id'=>$this->group_id))->find();
		$this->display();
	}
	
    //欢迎
	public function welcome(){
        $this->groupInfo = $this->group->field('name')->where(array('id'=>$this->group_id))->find();
		$this->display();
	}

    //信息通知
    public function notice(){
        $this->display();
    }

	//菜单
    public function menu(){
        $id = $this->request('get.id',intval);
        $this->leftMenu = $this->app->menu($id);
        $this->display();
    }

    //缓存管理
    public function cache(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $types = $this->request('get.types');
            $result = $this->default->cache_delete($types);
            if($result){
                $this->jsonMsg('Success:Delete success',1);
            }else{
                $this->jsonMsg('Error:Delete failed');
            } 
        }else{
            $this->tpl_szie = $this->default->cache_List('tpl');
            $this->log_szie = $this->default->cache_List('log');
            $this->display();
        }
    } 

	//登录
	public function login(){
		if($this->isPost()){
            $form = new form('post');
            $username = $form->getData('username');
            $password = $form->getData('password');
            if(!$form->isEmpty('username')){
                $this->jsonMsg('Please input name');
            }
            if(!$form->isEmpty('password')){
                $this->jsonMsg('Please input password');
            }
            $rel = $this->user->getUserInfo($username);
            if($rel['lock'] == 1){
                $this->jsonMsg('Lock up your account');
            }
            $password = md5($password);
            if(!empty($rel) && $rel['password'] == $password){
                 $this->user->setUserInfo($rel['id']);
                $this->setLogin($rel);
                $this->jsonMsg('Success:Login success',1,url('default/index'));
            }else{
                $this->jsonMsg('Error:Your username or password error');
            }
		}else{
            $this->display();
        } 
	}
	
	//退出登录
	public function logout(){
		$this->clearLogin( url('default/login') );
	} 
}