<?php
namespace app\admin\controller;
use framework\ext\Form;
/* *
 * 管理员
 * */
class UserController extends \app\base\controller\AdminController{
    private $admin = null;
    private $group = null;
    
    public function __construct() {
        parent::__construct();
        $this->admin = obj('user');
        $this->group = obj('group');
    } 

	//用户列表
	public function index(){
        $act = $this->request('get.act');
        if($act == 'del'){
            $id = $this->request('get.id',intval);
            if($id == 1){
                $this->jsonMsg('Can\'t delete the administrator');
            } 
            $condition['id'] = $id;
            $result =  $this->admin->where($condition)->delete();
            if($result){
                $this->jsonMsg('Success:Delete success',1);
            }else{
                $this->jsonMsg('Error:edit failed');
            } 
        }else{
            $page = $this->request('get.page',intval);
            $this->rel = $this->admin->pager($page,20)->_select();
            $this->pager = $this->getPage($this->admin->pager);
    		$this->display();
        }
	}

    //添加或编辑用户
    public function edit(){
        if($this->isPost()){
            $form = new form('post',array('id','password','chackpassword'));
            $data = $form->getVal();
            $id = $form->getVal('id');
            $password = $form->getVal('password');
            $chackpassword = $form->getVal('chackpassword');
            if(!$form->isEmpty('username')){
                $this->jsonMsg('Please input name');
            }
            if(!$form->isEmpty('password')){
                $this->jsonMsg('Please input password');
            }
            if(!$form->isEmpty('chackpassword')){
                $this->jsonMsg('Please input chackpassword');
            }
            if(!$form->isSame('password','chackpassword')){
                $this->jsonMsg('Two password input is not the same');
            }
            $data['password'] = md5($password);
            if($id){
                $condition['id']= $id;
                $rel = $this->admin->_update($data,$condition);
            }else{
                $info = $this->admin->where(array('username' =>$data['username']))->find();
                if($info){
                    $this->jsonMsg('Duplicate administrator name');
                }
                $data['login_time'] = time();
                $rel = $this->admin->_update($data);
            }
            if($rel){
                $this->jsonMsg('Success:editor success',1,url('user/index'));
            }else{
                $this->jsonMsg('Error:editor failed');
            }
        }else{
            $this->id = $this->request('get.id',intval);
            $this->info = $this->admin->where(array('id'=>$this->id))->find();
            $this->auth = $this->group->select();
            $this->display();
        }
    }

    //更改密码
    public function password(){
        if($this->isPost()){
            $password = $this->request('post.password');
            $newpassword = $this->request('post.newpassword');
            $chackpassword = $this->request('post.chackpassword');
            $condition['id'] = $_SESSION['admin_userInfo']['id'];
            $rel = $this->admin->where($condition)->field('password')->find();
            $password = md5($password);
            if(!empty($rel) && $rel['password'] == $password){
                $data['password'] = md5($newpassword);
                $this->admin->where($condition)->data($data)->update();
                $this->jsonMsg('Success:editor success',1,url('admin/default/welcome'));
            }else{
                $this->jsonMsg('Error:editor failure');
            }
        }else{
            $this->login_info = $_SESSION[$this->appID . '_userInfo'];
            $this->display();
        }   
    }
    
}