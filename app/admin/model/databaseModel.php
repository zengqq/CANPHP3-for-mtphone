<?php
namespace app\admin\model;
use framework\ext\Dbbak;
use framework\ext\Util;
/* *
 * 数据库管理
 * */
class databaseModel extends \app\base\model\BaseModel{

    public function __construct(){
        parent::__construct();
        $this->DirDB = DATA_PATH . 'backup'.DIRECTORY_SEPARATOR;
        $this->DakDB = $this->DirDB.date('YmdHis').DIRECTORY_SEPARATOR;
        $db = config('DB.default');
        $this->db = new Dbbak($db['DB_HOST'].':'.$db['DB_PORT'],$db['DB_USER'],$db['DB_PWD'],$db['DB_NAME'],'utf8',$this->DakDB);
    }

    //备份文件列表
    public function dbList(){
        if(!is_dir($this->DirDB)){
            return false;
        }
        $DBFile = glob($this->DirDB.'*');
        if(is_array($DBFile)){
            foreach ($DBFile as $key => $value) {
                $value = basename($value);
                $ary[$key]['name'] = $value;
                $ary[$key]['times'] = strtotime($value);
            }
        }
        return $ary;
    }

    //备份数据
    public function backup(){
        $DirDB = $this->DakDB;
        //检查目录
        if(!is_dir($DirDB)){
            if(!mkdir($DirDB,true)){
                return '无法创建备份目录，请手动根目录创建Backup文件夹！';
            }
        }
        //检查备份目录是否可写
        if(!is_writeable($DirDB)){
            return '备份目录不存在或不可写，请检查后重试！';
        }
        $tables = $this->db->getTables();
        if(!$this->db->exportSql($tables)){
            return '备份文件创建失败，请稍后再试！';
        }
        return 1;
    }

    //备份优化
    public function optimized(){
        $tables = $this->db->getTables();
        $tables = implode('`,`', $tables);
        if($this->query("OPTIMIZE TABLE `{$tables}`")){
            return 1;
        } else {
            return '数据库优化失败，请稍后重试！';
        }
    }

    //数据修复
    public function repair(){
        $tables = $this->db->getTables();
        $tables = implode('`,`', $tables);
        if($this->query("REPAIR TABLE `{$tables}`")){
            return 1;
        } else {
            return '数据库优化失败，请稍后重试！';
        }
    } 

    //还原数据库
    public function import($name){
        $importFiles = $this->DirDB.$name.DIRECTORY_SEPARATOR;
        $DBFile = glob($importFiles.'*.sql.php');
        if(empty($DBFile)){
            $this->DBError = '没有发现数据库文件！';
            return false;
        }
        //还原数据库
        if(!$this->db->importSql($importFiles)){
            $this->DBError = '数据库恢复失败，请稍后再试！';
            return false;
        }
        return true;
    }

    //删除备份文件
    public function DelectDB($name){
        $importFiles = $this->DirDB.$name.DIRECTORY_SEPARATOR;
        $rel = Util::delDir($importFiles);
        if(!$rel){
            return true;
        }else{
            return false;
        }
    }

    //数据库列表
    public function DBTableList(){
        return $this->query('SHOW TABLE STATUS');
    }

    //下载备份文件
    public function downdb($name){
        $importFiles = $this->DirDB.$name.DIRECTORY_SEPARATOR;
        $DBFile = glob($importFiles.'*.sql.php');
        if(empty($DBFile)){
            $this->DBError = '没有发现数据库文件！';
            return false;
        }
        if(is_array($DBFile)){
            foreach ($DBFile as $key => $value) {
                $ary['path'] = $value;
                $ary['name'] = basename($value);
            }
        }
        return $ary;
    }
}