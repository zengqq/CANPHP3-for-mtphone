<?php
namespace app\admin\model;
/* *
 * 管理员管理
 * */
class userModel extends \app\base\model\BaseModel{
	protected $table = 'admin';
	
    //登陆
    public function getUserInfo($username){
        $rel = $this->where(array('username'=>$username))->find();
	    if($username == $rel['username']){
		   return $rel;
        }else{
	       return false;
	   }
    }
    
    //更新登陆信息
    public function setUserInfo($id){
		$data = array();
		$data['login_ip'] = get_client_ip();
		$login_time = time();
		return $this->where(array('id'=>$id))->data($data)->update();
    }
    
    //用户列表
    public function _select(){
        return $this->table('admin as A')
            ->join('{pre}admin_group as B ON A.group_id = B.id','left')
            ->field('A.*,B.name as group_name')
            ->order('A.id desc')
            ->select();
    }
}