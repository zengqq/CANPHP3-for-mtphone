<?php
namespace app\admin\model;
use framework\ext\Category;
/* *
 * 应用管理
 * */
class appModel extends \app\base\model\BaseModel{
    protected $table ='admin_app';  //模型表
    private $group = null;

    public function __construct() {
        parent::__construct();
        $this->group = obj('group');
    } 

  //权限组目录书
   public function group_select_tree($pid = 0){
       $condition['pid'] = $pid;
       $rel = $this->where($condition)->order('neworder asc,id desc')->select();
       if($rel){
            foreach($rel as $key=>$rs){
                $ary['lists'][$key]= $rs;
                $str = $this->group_select_tree($rs['id']);
                if($str){
                    foreach($str as $k => $vo){
                       $ary['lists'][$key][$k] = $vo;
                    }
                }
            }
        }
        return $ary;
   }

    //配置管理菜单
    public function select_tree($pid){
        $rel = $this->field('id,rootid,pid,name,uptime,neworder,is_show,controller,method,types')->order('neworder asc,id desc')->select();
        $Category = new Category(array('id','pid','name','cname'));
        return $Category->getTree($rel,$pid);
    }

    //顶栏管理
    public function top_select($group_id){
        $auth_value = $this->auth_value($group_id);  //返回可管理的数组
        $rel = $this->where(array('pid' => 0))->field('id,name,icon,app')->order('neworder asc,id asc')->select();
        $nav = array();
        if(is_array($rel)){
            foreach ($rel as $key => $value) {
                foreach ($auth_value as $k => $v) {
                    if($v['app'] == -1){
                        $nav[$key]['name'] = $value['name'];
                        $nav[$key]['icon'] = $value['icon'];
                        $nav[$key]['id'] = $value['id'];
                    }else{
                        if($value['app'] == $v['app']){
                            $nav[$key]['name'] = $value['name'];
                            $nav[$key]['icon'] = $value['icon'];
                            $nav[$key]['id'] = $value['id'];
                        }
                    }
                }
            }
        }
        return $nav;
    }

    //返回可管理的数组
    public function auth_value($group_id){
        $group = $this->group->where(array('id'=>$group_id))->find();  //权限组
        if($group['auth_value']){
            $auth_array = json_decode($group['auth_value'],TRUE);
            foreach ($auth_array as $key => $value) {
                $str = explode('_', $value,3);
                if($str[0]){
                    $sry[$key]['app'] =$str[0];
                }
                if($str[1]){
                    $sry[$key]['controller'] = $str[0].'_'.$str[1];
                }
                if($str[2]){
                    $sry[$key]['method'] = $str[0].'_'.$str[1].'_'.$str[2];
                }
            }
        }
        return $sry;
    }

    //菜单
    public function menu($id){
        //获取权限组
        $group_id = intval($_SESSION['admin_userInfo']['group_id']);
        $userid = intval($_SESSION['admin_userInfo']['id']);
        $auth_group = obj('group')->where(array('id'=>$group_id))->find();
        if(!$auth_group['auth_value'])return false;
        $auth = json_decode($auth_group['auth_value'],TRUE); //显示可管理类方法的返回数组
        //获取全部菜单
        $rel = $this->where(array('rootid'=>$id,'is_show'=>1))->order('neworder asc,id desc')->select();
        $Category = new Category(array('id','pid','name','cname'));
        $result = $Category->getTree($rel,$id);
        if($result){
            foreach ($result as $key => $value) {
                if($value['pid'] == $id){
                    $nav[$key]['title'] = htmlspecialchars_decode($value['icon']).htmlspecialchars_decode($value['name']);
                    $top_id = $value['id'];
                    $top_key = $key;
                }
                if($value['pid'] == $top_id){
                    $method_id = $value['id'];
                }
                if($value['pid'] == $method_id){
                    if($auth[0] == -1){
                        $nav[$top_key]['list'][htmlspecialchars_decode($value['name'])] = url($value['app'].'/'.$value['controller'].'/'.$value['method']);
                    }else{
                        if(in_array($value['app'].'_'.$value['controller'].'_'.$value['method'],$auth )){
                            $nav[$top_key]['list'][htmlspecialchars_decode($value['name'])] = url($value['app'].'/'.$value['controller'].'/'.$value['method']);
                        }
                    }
                }
            }
        }
        return $nav;
    }
    
   //当前路径
    public function path($pid = 0,$url = NULL) {
        $condition['id'] = intval($pid);
         $result = $this->where($condition)->field('id,pid,name')->find();
        if ($result) {
            $Str = $this->path($result['pid'],$url);
            if($url){
                $Str .= ' / <a href="' . url(''.$url.'',array(pid=>$result['id'])).'">' . $result['name'] . '</a>';
            }else{
                $Str .= ' / <a href="###">' . $result['name'] . '</a>';
            }
        }
        return $Str;
    }
}