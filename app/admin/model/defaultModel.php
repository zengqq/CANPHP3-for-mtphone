<?php
namespace app\admin\model;
use framework\ext\Util;
/* *
 * 管理首页
 * */
class defaultModel extends \app\base\model\BaseModel{

    //获取缓存列表
    public function cache_List($folder){
        return dir_size(DATA_PATH.'cache/'.$folder)%1024;
    }

    //清空指定缓存
    public function cache_delete($types) {
        if($types ==1){
            $folder = 'tpl';
        }else{
            $folder = 'log';
        }
        $file = DATA_PATH.'cache/'.$folder;
        if(is_dir($file)){
            Util::delDir($file);
        }elseif(is_file($file)){
            unlink($file);
        }else{
            return FLASE;
        }
        return true;
    }
}