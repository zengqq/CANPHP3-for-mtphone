<?php
/**
 * 保存模块配置
 * @param string $app 对应APP  $new_config 修正配置内容
 */
function save_config($app = NULL, $new_config = array()){
    if($app){
        $file = APP_PATH.'/'.$app.'/config.php';
    }else{
        $file = CONFIG_PATH.'config.php';
    }
    if(!file_exists($file)){
        throw new \Exception("APP Config '{$name}' not found", 500);
    }
    if( is_file($file) ) {
        $config = require($file);
        $config = array_merge($config, $new_config);
    }else{
        $config = $new_config;
    }
    $content = var_export($config, true);
    $content = str_replace("_PATH' => '" . addslashes(ROOT_PATH), "_PATH' => ROOT_PATH . '", $content);

    if( file_put_contents($file, "<?php \r\nreturn " . $content . ';' ) ) {
        return true;
    }
    return false;
}

/**
 * 夸APP调用配置
 * @param string $app
 */
function appconfig($appname){
    $appConfig = BASE_PATH.'app/'.$appname.'/config.php';   //引入APP配置函数
    if(file_exists($appConfig)){
        config($appname,require($appConfig));
        return config($appname);
    }
    return false;
}

/**
 * 前台标签调用函数
 * $labelID：标签ID，$ClassID：栏目ID, $function：回调函数
 * @return array
 */
function label($labelID,$ClassID,$page = NULL){
    $labelID   = intval($labelID);
    $ClassID   = intval($ClassID);
    $rel = obj('cms/label')->_find(array('id'=> $labelID));
    if($rel){
        switch ($rel['type']) {
            case 1:
                return obj('cms/channel')->label($rel,$ClassID);
                break;
            case 2:
                return obj('cms/index')->label($rel,$ClassID,$page);
                break;
            default:
                echo 'SQL标签参数出错!';
                break;
        }
    }else{
        return array();
    }
}

/**
 * 数据签名认证
 * @param  array  $data 被认证的数据
 * @return string       签名
 */
function data_auth_sign($data) {
    //数据类型检测
    if(!is_array($data)){
        $data = (array)$data;
    }
    ksort($data); //排序
    $code = http_build_query($data); //url编码并生成query字符串
    $sign = sha1($code); //生成签名
    return $sign;
}

//获取客户端IP
function get_client_ip(){
    return \framework\ext\Util::getIp();
}

//字符串截取
function len($str, $len=0){
    if(!empty($len)){
        return \framework\ext\Util::msubstr($str, 0, $len);
    }else{
        return $str;
    }
}

//浏览器友好的变量输出
function dump($var, $exit=false){
    $output = print_r($var, true);
    $output = "<pre>" . htmlspecialchars($output, ENT_QUOTES) . "</pre>";
    echo $output;
    if($exit) exit();
}

//获取微秒时间，常用于计算程序的运行时间
function utime(){
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


/* *
 * 方法库-获取随机值
 * @return string  
* */
function random_str($str,$len){
    return substr(md5(uniqid(rand()*strval($str))),0, (int) $len);
}

/**
 * 生成唯一字符串(数字)
 * @return string  字符串
 */
function unique_str(){
    return date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0,8);
}

/**
 * 方法库-获取随机Hash值 
 * @return string
 */
function get_hash($length = 13) { 
    $chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    $max = strlen($chars) - 1;
    mt_srand((double)microtime() * 1000000);
    for ($i=0; $i<$length; $i++) {
        $hash .= $chars[mt_rand(0, $max)];
    }
    return $hash;
}

/**
 * 快速导入第三方框架类库 所有第三方框架的类库文件统一放到 系统的app/base/lib目录下面
 * @param string $class 类库
 * @param string $baseUrl 基础目录
 * @param string $ext 类库后缀
 * @return boolean
 */
function vendor($class, $baseUrl = '',$ext='.php') {
    if (empty($baseUrl))
        $baseUrl = BASE_PATH.'app/base/lib/';
    return import($class, $baseUrl, $ext);
}

/**
 * 导入所需的类库 同java的Import 本函数有缓存功能
 * @param string $class 类库命名空间字符串
 * @param string $baseUrl 起始路径
 * @param string $ext 导入的文件扩展名
 * @return boolean
 */

function import($class, $baseUrl = '',$ext=EXT) {
    static $_file = array();
    $class = str_replace(array('.', '#'), array('/', '.'), $class);
    if (isset($_file[$class . $baseUrl])){
        return true;
    }else{
        $_file[$class . $baseUrl] = true;
    }
    //路径修正
    if (substr($baseUrl, -1) != '/'){
        $baseUrl .= '/';
    }
    $classfile  = $baseUrl . $class . $ext;
    if (!class_exists(basename($class),false)) {
        // 如果类不存在 则导入类库文件
        return require_cache($classfile);
    }
}

/**
 * 优化的require_once
 * @param string $filename 文件地址
 * @return boolean
 */
function require_cache($filename) {
    static $_importFiles = array();
    if (!isset($_importFiles[$filename])) {
        if (file_exists_case($filename)) {
            require $filename;
            $_importFiles[$filename] = true;
        } else {
            $_importFiles[$filename] = false;
        }
    }
    return $_importFiles[$filename];
}

/**
 * 区分大小写的文件存在判断
 * @param string $filename 文件地址
 * @return boolean
 */
function file_exists_case($filename) {
    if (is_file($filename)) {
        if (IS_WIN && APP_DEBUG) {
            if (basename(realpath($filename)) != basename($filename))
                return false;
        }
        return true;
    }
    return false;
}

/**
 * 获取文件或文件大小
 * @param string $directoty 路径
 * @return int
 */
function dir_size($directoty){
    $dir_size = 0;
    if ($dir_handle = @opendir($directoty)) {
        while ($filename = readdir($dir_handle)) {
            $subFile = $directoty . DIRECTORY_SEPARATOR . $filename;
            if ($filename == '.' || $filename == '..') {
                continue;
            } elseif (is_dir($subFile)) {
                $dir_size += dir_size($subFile);
            } elseif (is_file($subFile)) {
                $dir_size += filesize($subFile);
            }
        }
        closedir($dir_handle);
    }
    return ($dir_size);
}