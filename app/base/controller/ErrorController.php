<?php
namespace app\base\controller;
/* *
 * 返回信息类
 * */
class ErrorController extends BaseController{

    public $layout = NULL;

    /**
     * ajax返回信息
     * @param  object $msg 提示信息
     * @param  object $status 状态 0:错误。1：成功
     * @param  object $url 跳转的url
     * @return void
     */
    protected function jsonMsg($msg,$status = 0,$url =NULL){
        header('Content-type:text/json');
        echo json_encode(array("info" => $msg,"status" =>$status,"url" => $url));
        exit();
    } 

    /**
     * 页面返回信息
     * @param  object $msg 提示信息
     * @param  object $status 状态 0:错误。1：成功
     * @param  object $url 跳转的url
     * @return void
     */
    protected function pageMsg($msg = NULL,$status = 0,$url = NULL){
        $this->msg = $msg;
        $this->status = $status;
        $this->url = $url;
        $this->display('app/base/view/error_message');
        exit();
    }

    /**
     * 404错误
     * @param  object $e 错误对象
     * @return void
     */
    public function error404($e=null) {
        header('HTTP/1.1 404 Not Found'); 
        header("status: 404 Not Found");
        $this->error($e);
    }
    
    /**
     * 错误输出
     * @param  object $e 错误对象
     * @return void
     */
    public function error($e=null){
        if(false!==stripos(get_class($e), 'Exception')) {
            $this->errorMessage = $e->getMessage();
            $this->errorCode = $e->getCode();
            $this->errorFile = $e->getFile();
            $this->errorLine = $e->getLine();
            $this->trace = $e->getTrace();      
        }     
        //关闭调试或者是线上版本，不显示详细错误
        if(false==config('DEBUG') || 'production'==config('ENV')) {
            $tpl = 'error_production';
            //记录错误日志
        }else{
            $tpl = 'error_development';
        }
        $this->display('app/base/view/'.$tpl);
    }   
}