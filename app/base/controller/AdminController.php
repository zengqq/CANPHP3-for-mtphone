<?php
namespace app\base\controller;
/* *
 * 后台基础类
 * */
class AdminController extends ErrorController{

	protected $appID = 'admin';
	public $layout = 'app/admin/view/layout';  //layout
	public $userInfo = 0;
	public $admin_id = 0;
	public $group_id = 0;


	public function __construct(){
		if(!isset( $_SESSION )) session_start();
		parent::__construct();
		$this->checkLogin();  //判断是否登录
		$this->appconfig = config(APP_NAME);
	}
	//权限验证
	protected function checkLogin(){
		//不需要登录验证的页面
		$noLogin = array('default'=>array('login','verify','logout'),);	

		//如果当前访问是无需登录验证，则直接返回		
		if( isset($noLogin[CONTROLLER_NAME]) && in_array(ACTION_NAME, $noLogin[CONTROLLER_NAME]) ){	
			return true;
		}
		//没有登录,则跳转到登录页面
		if(!$this->isLogin()){
			$this->redirect( url('admin/default/login') );
		}else{
			if(!$this->isAuth()){
				$this->pageMsg('您没有权限访问本次操纵,请联系管理员增加权限.');
			}
		}
		return true;
	}

	//权限判断
	protected function isAuth(){
		$group = obj('admin/group')->where(array('id'=>$this->group_id))->find();  //权限组
		if($group['auth_value']){
			$auth = json_decode($group['auth_value'],TRUE);
			if($auth[0] == -1)return true;  //超级权限
			foreach ($auth as $key => $value) {
				$route = explode('_', $value, 3);
				$routeNum = count($route);
				switch ($routeNum) {
					case 1:
						$url = APP_NAME;
						break;
					case 2:
						$url = APP_NAME.'_'.CONTROLLER_NAME;
						break;
					case 3:
						$url = APP_NAME.'_'.CONTROLLER_NAME.'_'.ACTION_NAME;
						break;
				}
				if(in_array($url,$auth)){
					return true;
				}
			}
			return false;
		}else{
			return false;
		}
		return true;
	}

	//判断是否登录
	protected function isLogin(){
		if( empty( $_SESSION[ $this->appID . '_userInfo' ] ) ){
			return false;
		}else{
			$this->userInfo = $_SESSION[ $this->appID . '_userInfo' ];
			$this->group_id = intval($this->userInfo['group_id']);
			$this->admin_id = $this->userInfo['id'];
			return true;
		}
	}
	
	//设置登录
	protected function setLogin($userInfo ){
		$_SESSION[ $this->appID . '_userInfo' ] = $userInfo;
	}
	
	//退出登录
	protected function clearLogin($url='' ){
		$_SESSION[ $this->appID . '_userInfo' ] = NULL;
		if( !empty($url) ){
			$this->redirect( $url );
		}
		return true;
	}

	//加密接口
	protected function auth(){
		return 'http://cms.ilinzhou.cn';
	    //return \framework\ext\Util::cpDecode('2fadVMwbhHc5Bkdjp3/09B7So2AQhtbk85jNkyLKb7TarbXNvgmXchHiU81Ruazz7nHcDNpuXuOSQ6P6Zg','siteplus');
	}
}