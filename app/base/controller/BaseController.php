<?php
namespace app\base\controller;
use framework\base\Config;
use framework\ext\UploadFile;
/* *
 * 根类
 * */
class BaseController extends \framework\base\Controller{
    
    /**
     * 定义常量和配置信息
     * @access public
     * @return void
     */
    public function __construct(){
        define('COME_URL',$_SERVER['HTTP_HOST']);
        define('APP_PATH', ROOT_PATH . 'app' . DIRECTORY_SEPARATOR);  //APP目录
        define('DATA_PATH', ROOT_PATH . 'data' . DIRECTORY_SEPARATOR); //缓存目录
        define('__PUBLIC__', substr(PUBLIC_URL, 0, -1));  //附件目录
        define('__ROOT__', substr(ROOT_URL, 0, -1)); //系统网址
        defined('__TPL__') or define('__TPL__', ROOT_URL .'themes');  //前台模板目录
        include_once(APP_PATH . 'base/lib/function.php');   //引入函数
        $appConfig = BASE_PATH.'app/'.APP_NAME.'/config.php';   //引入APP配置函数
        if(file_exists($appConfig)){
            config(APP_NAME,require($appConfig));
        }
    }

    /**
     * 前台模板显示调用内置的模板引擎
     * @access protected
     * @param bool $type 模板输出
     * @return void
     */
    protected function show($tpl='',$type = true) {
        if( empty($tpl) ){
            $tpl = __TPL__.'/'.APP_NAME . '/' . strtolower(CONTROLLER_NAME) . config('TPL.TPL_DEPR') . strtolower(ACTION_NAME);
        }else{
            $tpl = __TPL__.'/'.$tpl;
        }
        if($type){
            $this->display($tpl);
        }else{
            return $this->display($tpl,true);
        }
    }

    /**
     * 分页结果显示
     * @return string  字符串
     * @return action  数组,URL后面的参数
     */
     protected function getPage($pageArray,$action = array()){
        if(is_array($pageArray)){
            $html = '<ul class="pager">';
            $html .= '<li><a href="'.$this->GreatePageUrl($pageArray['firstPage'],$action).'" >首页</a></li>';
            $html .= '<li><a href="'.$this->GreatePageUrl($pageArray['prevPage'],$action).'">上一页</a></li>';
                foreach ($pageArray['allPages'] as $value) {
                    if($value){
                        if($value == $pageArray['page']){
                            $html .= '<li class="active">';
                        }else{
                            $html .= '<li>';
                        }
                        $html .= '<a href="'.$this->GreatePageUrl($value,$action).'">'.$value.'</a></li>';
                    }        
               }
             $html .= '<li><a href="'.$this->GreatePageUrl($pageArray['nextPage'],$action).'">下一页</a></li>';
             $html .= '<li><a href="'.$this->GreatePageUrl($pageArray['lastPage'],$action).'">末页</a></li>';
             $html .= '</ul>';
        }
        return $html;
    }
    
    /**
     * 分页URL处理
     * @return page    当前页面
     * @return action  数组,URL后面的参数
     */
    protected function GreatePageUrl($page,$action = array()){
        $ary_page = array('page' => $page);
        if($action){
            $ary_page = array_merge($ary_page,$action);
        }
        return url( APP_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME,$ary_page);
    }

    /**
     * 表单处理接口
     * @param  array  $str 要数据数的数据 $function 回调函数
     * @return array/sting 数组或字符串
     */
    protected function request($str = null,$function = '') {
        if(empty($str)){
            return false;
        }
        $str = trim($str);
        list($method,$name) = explode('.',$str,2);
        $method = strtolower($method);
        switch ($method) {
            case 'get':
                $type = $_GET;
                break;
            case 'post':
                $type = $_POST;
                break;
            default:
                $input =  file_get_contents("php://input",'r');
                parse_str($input, $type);
        }
        if(empty($name)){
            $data = $this->code($type);
        }else{
            switch ($method) {
                case 'get':
                    $data = urldecode($type[$name]);
                    break;
                default:
                    $data = $type[$name];
                    break;
            }
        }
        $data = $this->code($data);
        if(!empty($name) && !empty($function)){
            $data = call_user_func($function,$data);
        }
        return $data;
    }

    /**
     * 过滤数据
     * @param  array  $data 需要过滤的数组
     * @return array  过滤后的数据
     */
    protected function code($data,$decode = 0){
        if(empty($data)){
            return false;
        }
        if (is_array($data)){
            foreach ($data as $key => $value){
                $data[$key] = $this->code($value,$decode);
            }
            return $data;
        }else{
            if($decode){
                return htmlspecialchars_decode($data,ENT_QUOTES);
            }else{
                return htmlspecialchars(trim($data),ENT_QUOTES, 'UTF-8');
            }
        }
    } 

    /**
     * 上传处理
     * @access protected
     * @param array $files 文件数据
     * @param array $config 配置参数
     * @return json
     */
    protected function Files($files,$config){
        if(empty($files)){
            return false;
        }
        $upload = new UploadFile();  //上传类
        $upload->maxSize = 1024 * 1024 * intval($config['UP_SIZE']); //大小
        $upload->allowExts = explode(',',$config['UP_EXTS']);  //允许上传的文件的类型
        $upload->savePath = $config['UP_FOLDER'].date('Ymd').'/';  //设置文件上传目录的路径
        $upload->saveRule = unique_str; //上传文件名
        if ($upload->upload()){ //取得成功上传的文件信息
            $UploadFileInfo = $upload->getUploadFileInfo();
            $result = $UploadFileInfo[0];
            $str['error'] = 0;
            $str['url'] = ROOT_URL.$result['savepath'].$result['savename'];
        }else{
            $result = $upload->getErrorMsg();
            $str['error'] = 1;
            $str['message'] = $result;
        }
        return json_encode($str);
    } 
    /**
     * 过滤数据
     * @param  array  $data 需要过滤的数组
     * @return array  过滤后的数据
     */
    protected function header_excel($name){
        header("Content-Type: text/html; charset=UTF-8");
        header("Content-type:application/octet-stream");
        header("Accept-Ranges:bytes");
        header("Content-type:application/vnd.ms-excel");  
        header("Content-Disposition:attachment;filename=".$name.date('Ymdhis').".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        $this->layout ='';
    }
}