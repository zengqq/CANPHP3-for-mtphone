<?php
namespace app\base\model;

/* *
 * 基础Model
 * */
class BaseModel extends \framework\base\Model{

    //上传处理
    public function uploadFile($files){
        if(empty($files)){
            return false;
        }
        $config = config('base');
        $upload = new UploadFile();  //上传类
        $upload->maxSize = 1024 * 1024 * intval($config['UP_SIZE']); //大小
        $upload->allowExts = explode(',',$config['UP_EXTS']);  //允许上传的文件的类型
        $upload->savePath = $config['UP_FOLDER'].date('Ymd').'/';  //设置文件上传目录的路径
        $upload->saveRule = unique_str; //上传文件名
        if ($upload->upload()){ //取得成功上传的文件信息
            $result = $upload->getUploadFileInfo()[0];
            $arr['error'] = 0;
            $arr['url'] = ROOT_URL.$result['savepath'].$result['savename'];
        }else{
            $result = $upload->getErrorMsg();
            $arr['error'] = 1;
            $arr['message'] = $result;
        }
        return $arr;
    }

    //编辑/添加
    public function _update($data = array(),$condition = array()){
        if($condition){
            return $this->where($condition)->data($data)->update();
        }else{
            return $this->data($data)->insert();
        }
    } 
}