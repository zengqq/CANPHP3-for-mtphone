<?php

/**
 * 模板钩子
 */

namespace app\base\hook;

class TempalteHook {

    /**
     * 模板解析
     * @param  string $template 模板内容
     * @return string
     */
    public function templateParse($template){
       $label = array( 
                /**variable label
                    {$name} => <?php echo $name;?>
                    {$user['name']} => <?php echo $user['name'];?>
                    {$user.name}    => <?php echo $user['name'];?>
                */  
                '/{(\\$[a-zA-Z_]\w*(?:\[[\w\.\"\'\[\]\$]+\])*)}/i' => "<?php echo $1; ?>",
                '/\$(\w+)\.(\w+)\.(\w+)\.(\w+)/is' => "\$\\1['\\2']['\\3']['\\4']",
                '/\$(\w+)\.(\w+)\.(\w+)/is' => "\$\\1['\\2']['\\3']",
                '/\$(\w+)\.(\w+)/is' => "\$\\1['\\2']",
                
                /**constance label
                {CONSTANCE} => <?php echo CONSTANCE;?>
                */
                '/\{([A-Z_\x7f-\xff][A-Z0-9_\x7f-\xff]*)\}/s' => "<?php echo \\1;?>",
                
                /**include label
                    {include file="test"}
                */                              
                '/{include\s*file=\"(.*)\"}/i' => "<?php \$__Template->display(\"$1\"); ?>",
                  
                /**if label
                    {if $name==1}       =>  <?php if ($name==1){ ?>
                    {elseif $name==2}   =>  <?php } elseif ($name==2){ ?>
                    {else}              =>  <?php } else { ?>
                    {/if}               =>  <?php } ?>
                */              
                '/\{if\s+(.+?)\}/' => "<?php if(\\1) { ?>",
                '/\{else\}/' => "<?php } else { ?>",
                '/\{elseif\s+(.+?)\}/' => "<?php } elseif (\\1) { ?>",
                '/\{\/if\}/' => "<?php } ?>",
                
                /**for label
                    {for $i=0;$i<10;$i++}   =>  <?php for($i=0;$i<10;$i++) { ?>
                    {/for}                  =>  <?php } ?>
                */              
                '/\{for\s+(.+?)\}/' => "<?php for(\\1) { ?>",
                '/\{\/for\}/' => "<?php } ?>",
                
                /**foreach label
                    {foreach $arr as $vo}           =>  <?php $n=1; if (is_array($arr) foreach($arr as $vo){ ?>
                    {foreach $arr as $key => $vo}   =>  <?php $n=1; if (is_array($array) foreach($arr as $key => $vo){ ?>
                    {/foreach}                  =>  <?php $n++;}unset($n) ?> 
                */
                '/\{foreach\s+(\S+)\s+as\s+(\S+)\}/' => "<?php \$n=1;if(is_array(\\1)) foreach(\\1 as \\2) { ?>", 
                '/\{foreach\s+(\S+)\s+as\s+(\S+)\s*=>\s*(\S+)\}/' => "<?php \$n=1; if(is_array(\\1)) foreach(\\1 as \\2 => \\3) { ?>",
                '/\{\/foreach\}/' => "<?php \$n++;}unset(\$n); ?>",
                
                /*
                    {loop $arr $vo}         =>  <?php $n=1; if (is_array($arr) foreach($arr as $vo){ ?>
                    {loop $arr $key $vo}    =>  <?php $n=1; if (is_array($array) foreach($arr as $key => $vo){ ?>
                    {/loop}                 =>  <?php $n++;}unset($n) ?>
                 */
                '/\{loop\s+(\S+)\s+(\S+)\}/' => "<?php \$n=1;if(is_array(\\1)) foreach(\\1 AS \\2) { ?>",
                '/\{loop\s+(\S+)\s+(\S+)\s+(\S+)\}/' => "<?php \$n=1; if(is_array(\\1)) foreach(\\1 AS \\2 => \\3) { ?>",
                '/\{\/loop\}/' => "<?php \$n++;}unset(\$n); ?>",

                /**function label
                    {date('Y-m-d H:i:s')}   =>  <?php echo date('Y-m-d H:i:s');?> 
                    {$date('Y-m-d H:i:s')}  =>  <?php echo $date('Y-m-d H:i:s');?> 
                */
                '/\{([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff:]*\(([^{}]*)\))\}/' => "<?php echo \\1;?>",
                '/\{(\\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff:]*\(([^{}]*)\))\}/' => "<?php echo \\1;?>", 
  
                /**
                * 调用模板{include="test"}
                */                   
                '/{include=\"(.*)\"}/i' => "<?php \$__Template->display(__TPL__.'/'.\"$1\"); ?>",
                /**
                * 自定义链接[page:id]
                */
                "/{page:(\S+)}/iU"=> "<?php echo url('cms/default/page',array('uid'=>$1));?>",
                /**
                * 替换数据碎片 
                */
                "/{inc:(\S+)}/iU"=> "<?php echo model('fragment','cms')->Label($1)?>",  
                /**
                * 广告管理 
                */
                "/{ad:(\S+)}/iU"=> "<?php echo model('adlable','ad')->index($1)?>",  
                /**
                * SQL读取标签,带翻页<!--lable:page(标签ID,栏目ID)-->
                */
                "/<!--(\S+):(\S+)\((.+?)\,(.+?)\)-->/i"=> "<?php \$rel=label($3,$4,$2);$$1 = \$rel[0]; $$2 = \$rel[1];\$$1_i=0;if(!empty($$1)) foreach($$1 as $$1_key=>$$1){\$$1_i++; ?>",
                /**
                * SQL读取标签<!--lable(标签ID,栏目ID)-->
                */
                "/<!--(\S+)\((.+?)\,(.+?)\)-->/i"=> "<?php \$rel=label($2,$3);$$1 = \$rel[0];\$$1_i=0;if(!empty($$1)) foreach($$1 as $$1_key=>$$1){\$$1_i++; ?> ",

                /**
                * SQL读取标签<!--lable(广告ID)-->
                */
                "/<!--(\S+)\((.+?)\)-->/i"=> "<?php $$1=model('adlable','ad')->index($2);\$$1_i=0;if(!empty($$1)) foreach($$1 as $$1_key=>$$1){\$$1_i++; ?> ",

                /**
                * 结束SQL标签<!--/lable-->
                */
                "/<!--\/([a-zA-Z_]+)-->/i"=> "<?php } unset($$1)?>",
        );

        $template = str_replace('__TPL__', __TPL__, $template);
        $template = str_replace('__PUBLIC__', __PUBLIC__, $template);
        $template = str_replace('__ROOT__', __ROOT__, $template);

        foreach ($label as $key => $value) {
            $template = preg_replace($key, $value, $template);
        }

        return $template;
    }
}