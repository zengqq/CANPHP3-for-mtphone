//jQuery常用函数封装
$.ajaxSetup({cache : false});
(function ($){
//#################
/*
$.fn.YouName = function (options){
    var defaults = {}
    var options = $.extend(defaults,options);
    this.each(function (){
        ....Code....
    });
};
*/
//删除操作
$.fn.del = function (){
    var url = $(this).attr("url");
    var css = this;
    var win = $.dialog.top;
    return this.each(function() {
        art.dialog.confirm("确认要删除本资源?", function() {
            $.get(url,function(data) {
                if (data.status != 1) {
                    win.art.dialog({content:data.info,lock:true,fixed:true,time:1});
                    return false;
                }
                $(css).parents("tr").remove();
                $(css).parent().remove();
            },"json");
        }, function(){art.dialog.tips('取消继续操作!');});
    });
};
//弹窗窗口
$.fn.win= function (){
    var win = $.dialog.top;
    var url = $(this).attr("url");
    var title = $(this).attr("title");
    return this.each(function() {
        $.dialog.open(url,{opacity:0.20,title:title,lock:true,fixed:true})
    });
};
//上传窗口
$.fn.upload= function (url){
    var win = $.dialog.top;
    return this.each(function() {
        $.dialog.open(url,{opacity:0.20,title:'文件上传管理器',lock:true,fixed:true})
    });
};
//颜色
$.fn.color = function (options){
    return this.each(function (){
        $(this).soColorPacker();
    });
};
//时间插件
$.fn.times = function (options) {
    var defaults = {lang: 'ch'}
    var options = $.extend(defaults,options);
    this.each(function () {
        $(this).datetimepicker(options);
    });
};
//表单处理
$.fn.formvalidator = function (options){
    var defaults ={types:'ajax'}
    var options = $.extend(defaults, options);
    return this.each(function (){
        var form = this;
        if (options.types == 'ajax'){
            $(form).Validform({
                btnSubmit: ".submit",showAllError: true,tiptype: 4,ajaxPost: true,postonce:true,
                callback: function(data) {
                    if (data.status == "1") {
                        art.dialog.confirm(data.info,function() {
                            var winone = artDialog.open.origin;
                            var wintwo = art.dialog.open.origin;
                            if (data.url) {
                                wintwo.location.replace(data.url)
                            } else {
                                wintwo.location.reload()
                            }
                        },function() {
                            $.dialog.through({content:"你取消了操作!",lock:true,fixed:true,time:1});
                        })
                    } else {
                        $.dialog.through({id: "vform",time: 2,lock: true,content:data.info})
                    }
                }
            })
        }else{
            $(form).Validform({btnSubmit: ".submit",showAllError: true,tiptype: 4})
        }
    });
};
//引入提交表单
$.fn.forms = function (options){
    var defaults = {
        types:'ajax',
        uploads:undefined
    }
    var options = $.extend(defaults,options);
    return this.each(function (){
        var form = this;
        form = $(form);
        form.formvalidator({types:options.types});//表单处理
        if ($(".ui-editor").length > 0){form.find('.ui-editor').editor(options.uploads);}//编辑器
        if ($(".ui-date").length > 0){form.find('.ui-date').times({format:'Y/m/d'});}//时间选择器
        if ($(".ui-time").length > 0){form.find('.ui-time').times({format:'Y/m/d H:i'});}//时间选择器
        if ($(".ui-color").length > 0){form.find('.ui-color').color();}//颜色选择器   
        if ($(".ui-upload").length > 0){$(".ui-upload").click(function(){$(this).upload(options.uploads);});}//上传附件
    });
};
//联动插件封装
$.fn.liandong = function(a,url){
    $.ajax({
        url:url,
        type:'post',
        data:{current:$(this).val()},
        success:function(msg){
            var json = eval(msg);
            var str = '';
            for(var i=0;i<json.length;i++){
                str+='<option value='+json[i].id+'>'+json[i].name+'</option>';
            }
            $('#'+a).html(str);
        }
    });
}
//在光标位置插入内容, 并选中
$.fn.insertContent = function(myValue, t) {
    var $t = $(this)[0];
    if (document.selection) { //ie
        this.focus();
        var sel = document.selection.createRange();
        sel.text = myValue;
        this.focus();
        sel.moveStart('character', -l);
        var wee = sel.text.length;
        if (arguments.length == 2) {
            var l = $t.value.length;
            sel.moveEnd("character", wee + t);
            t <= 0 ? sel.moveStart("character", wee - 2 * t - myValue.length) : sel.moveStart("character", wee - t - myValue.length);

            sel.select();
        }
    } else if ($t.selectionStart || $t.selectionStart == '0') {
        var startPos = $t.selectionStart;
        var endPos = $t.selectionEnd;
        var scrollTop = $t.scrollTop;
        $t.value = $t.value.substring(0, startPos) + myValue + $t.value.substring(endPos, $t.value.length);
        this.focus();
        $t.selectionStart = startPos + myValue.length;
        $t.selectionEnd = startPos + myValue.length;
        $t.scrollTop = scrollTop;
        if (arguments.length == 2) {
            $t.setSelectionRange(startPos - t, $t.selectionEnd + t);
            this.focus();
        }
    }
    else {
        this.value += myValue;
        this.focus();
    }
}
//Jquery全选插件 全选，反选和取消按钮功能
$.fn.checked = function(a) {
    var a = a || "on";
    return this.each(function() {
        switch (a) {
          case "on":
            this.checked = true;
            break;
          case "off":
            this.checked = false;
            break;
          case "toggle":
            this.checked = !this.checked;
            break;
        }
    });
};
//编辑器调用
$.fn.editor = function (uploads){
    var defaults = {uploadUrl : uploads}
    var options = $.extend(defaults,options);
    return this.each(function (){
        var id = this;
        var editorConfig ={
            allowFileManager : false,
            uploadJson:options.uploadUrl,
            items : ['source','plainpaste','|','fontsize','forecolor','fontname','hilitecolor','bold', 'italic','underline','removeformat','|', 'justifyleft','justifycenter','justifyright','insertorderedlist','insertunorderedlist','|', 'image','flash', 'media','|', 'table','link','unlink','clearhtml','quickformat','|','fullscreen'],
            afterBlur : function (){this.sync();},
            width : '100%',
            height: '450px'
        };
        editorConfig = $.extend(editorConfig,options.config);
        var editor = KindEditor.create(id,editorConfig);
    });
};
})(jQuery);