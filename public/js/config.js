Do.setConfig('coreLib', [baseDir + 'jquery.js']);
Do.add('base',{path : baseDir + 'Base/base.js'});
//form
Do.add('formcss',{path : baseDir + 'Form/style.css',type : 'css'});
Do.add('formjs',{path : baseDir + 'Form/Validform.js',type : 'js'});
Do.add('form',{path : baseDir + 'Form/Validform_Datatype.js',requires : ['formjs', 'formcss']});
//Dialog
Do.add('dialogcss',{path : baseDir + 'Dialog/skins/default.css',type : 'css'});
Do.add('dialogJs',{path : baseDir + 'Dialog/artDialog.js',type : 'js'});
Do.add('dialog',{path : baseDir + 'Dialog/iframeTools.js',requires:['dialogcss', 'dialogJs']});
//kindeditor
Do.add('editorjs',{path : baseDir + 'Kindeditor/kindeditor-min.js'});
Do.add('editor',{path : baseDir + 'Kindeditor/lang/zh_CN.js',requires : ['editorjs']});
//Treeview
Do.add('treeviewcss',{path : baseDir + 'Treeview/jquery.treeview.css',type : 'css'});
Do.add('tree',{path : baseDir + 'Treeview/jquery.treeview.js',requires : ['treeviewcss']});
//uploadify
Do.add('webuploadercss',{path : baseDir + 'webuploader/webuploader.css',type : 'css'});
Do.add('webuploader',{path : baseDir + 'webuploader/webuploader.min.js',requires : ['webuploadercss']});
//SuperSlide
Do.add('slide',{path : baseDir + 'SuperSlide/SuperSlide.js'});
//tip
Do.add('tipcss',{path : baseDir + 'Tip/style.css',type : 'css'});
Do.add('tip',{path : baseDir + 'Tip/powerFloat.js', requires : ['tipcss']});
//DatePicker
Do.add('datecss',{path : baseDir + 'Date/jquery.datetimepicker.css',type : 'css'});
Do.add('date',{path : baseDir + 'Date/jquery.datetimepicker.js',requires:['datecss']});
//color
Do.add('colorcss',{path : baseDir + 'Color/style.css',type : 'css'});
Do.add('color',{path : baseDir + 'Color/ColorPacker.js',requires : ['colorcss']});