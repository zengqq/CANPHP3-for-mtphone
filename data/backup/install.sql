/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : mtphone

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-12-15 18:28:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `easy_admin`
-- ----------------------------
DROP TABLE IF EXISTS `easy_admin`;
CREATE TABLE `easy_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `group_id` tinyint(4) NOT NULL DEFAULT '1',
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `login_time` int(10) unsigned DEFAULT NULL,
  `login_ip` varchar(15) DEFAULT NULL,
  `lock` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  KEY `groupid` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of easy_admin
-- ----------------------------
INSERT INTO `easy_admin` VALUES ('1', '1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '1421479887', '127.0.0.1', '0');

-- ----------------------------
-- Table structure for `easy_admin_app`
-- ----------------------------
DROP TABLE IF EXISTS `easy_admin_app`;
CREATE TABLE `easy_admin_app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rootid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `is_show` tinyint(4) DEFAULT '0',
  `types` tinyint(1) DEFAULT '0' COMMENT '0是app,1是模块,2是控制器,3是方法',
  `icon` varchar(255) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `en_name` varchar(100) DEFAULT NULL,
  `app` varchar(100) DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `method` varchar(100) DEFAULT NULL,
  `uptime` varchar(20) DEFAULT NULL,
  `neworder` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of easy_admin_app
-- ----------------------------
INSERT INTO `easy_admin_app` VALUES ('1', '0', '0', '1', '0', '&lt;i class=&quot;iconfont icon-shezhi&quot;&gt;&lt;/i&gt; ', '系统', null, 'admin', '', '', '1434906538', '100');
INSERT INTO `easy_admin_app` VALUES ('2', '1', '1', '1', '1', '&lt;i class=&quot;iconfont icon-shezhi&quot;&gt;&lt;/i&gt; ', '系统管理', null, 'admin', '', '', '1434889434', '0');
INSERT INTO `easy_admin_app` VALUES ('3', '1', '2', '1', '2', '', '模块管理', null, 'admin', 'app', '', '1434890677', '0');
INSERT INTO `easy_admin_app` VALUES ('4', '1', '3', '1', '3', '', '模块管理', null, 'admin', 'app', 'index', '1434892020', '1');
INSERT INTO `easy_admin_app` VALUES ('5', '1', '2', '1', '2', '', '管理员', null, 'admin', 'user', '', '1434891673', '0');
INSERT INTO `easy_admin_app` VALUES ('6', '1', '5', '1', '3', '', '管理员', null, 'admin', 'user', 'index', '1434891828', '1');
INSERT INTO `easy_admin_app` VALUES ('7', '1', '5', '0', '3', '', '添加/编辑', null, 'admin', 'user', 'edit', '1434892129', '2');
INSERT INTO `easy_admin_app` VALUES ('8', '1', '5', '0', '3', '', '修改密码', null, 'admin', 'user', 'password', '1434892054', '3');
INSERT INTO `easy_admin_app` VALUES ('9', '1', '3', '0', '3', '', '模块添加/编辑', null, 'admin', 'app', 'edit', '1434892013', '2');
INSERT INTO `easy_admin_app` VALUES ('10', '1', '2', '0', '2', '', '数据库管理', null, 'admin', 'database', '', '1436973133', '3');
INSERT INTO `easy_admin_app` VALUES ('11', '1', '10', '1', '3', '', '数据库管理', null, 'admin', 'database', 'index', '1434892196', '1');
INSERT INTO `easy_admin_app` VALUES ('12', '1', '10', '0', '3', '', ' 数据库列表', null, 'admin', 'database', 'tables', '1434892296', '2');
INSERT INTO `easy_admin_app` VALUES ('13', '1', '10', '0', '3', '', '数据库备份', null, 'admin', 'database', 'backupdb', '1434892315', '3');
INSERT INTO `easy_admin_app` VALUES ('14', '1', '10', '0', '3', '', '数据库优化', null, 'admin', 'database', 'optimizeddb', '1434892332', '4');
INSERT INTO `easy_admin_app` VALUES ('15', '1', '10', '0', '3', '', '数据库修复', null, 'admin', 'database', 'repairdb', '1434892345', '5');
INSERT INTO `easy_admin_app` VALUES ('16', '1', '10', '0', '3', '', '数据库恢复', null, 'admin', 'database', 'importdb', '1434892360', '6');
INSERT INTO `easy_admin_app` VALUES ('17', '1', '10', '0', '3', '', '删除备份', null, 'admin', 'database', 'delectdb', '1434892382', '6');
INSERT INTO `easy_admin_app` VALUES ('18', '1', '10', '0', '3', '', '下载备份', null, 'admin', 'database', 'downdb', '1434892395', '8');
INSERT INTO `easy_admin_app` VALUES ('20', '1', '2', '1', '2', '', '管理中心', null, 'admin', 'default', '', '1434902646', '1');
INSERT INTO `easy_admin_app` VALUES ('21', '1', '20', '0', '3', '', '管理首页', null, 'admin', 'default', 'index', '1434892532', '1');
INSERT INTO `easy_admin_app` VALUES ('22', '1', '20', '0', '3', '', '欢迎页', null, 'admin', 'default', 'welcome', '1434892550', '2');
INSERT INTO `easy_admin_app` VALUES ('23', '1', '20', '0', '3', '', '官方通知', null, 'admin', 'default', 'notice', '1434892574', '1');
INSERT INTO `easy_admin_app` VALUES ('24', '1', '20', '0', '3', '', '管理菜单', null, 'admin', 'default', 'menu', '1434892592', '0');
INSERT INTO `easy_admin_app` VALUES ('25', '1', '20', '1', '3', '', '缓存清除', null, 'admin', 'default', 'cache', '1434902653', '0');
INSERT INTO `easy_admin_app` VALUES ('26', '1', '2', '1', '2', '', '权限组', null, 'admin', 'group', '', '1434901497', '1');
INSERT INTO `easy_admin_app` VALUES ('27', '1', '26', '1', '3', '', '权限组', null, 'admin', 'group', 'index', '1434901549', '1');
INSERT INTO `easy_admin_app` VALUES ('52', '0', '0', '1', '0', '&lt;i class=&quot;iconfont  icon-shezhi1&quot;&gt;&lt;/i&gt;', '维修', null, 'repair', null, null, '1449151275', '2');
INSERT INTO `easy_admin_app` VALUES ('53', '0', '0', '1', '0', '&lt;i class=&quot;iconfont  icon-ziliaoku&quot;&gt;&lt;/i&gt;', '数据', null, 'warehouse', null, null, '1501592868', '4');
INSERT INTO `easy_admin_app` VALUES ('54', '53', '53', '1', '1', '&lt;i class=&quot;iconfont  icon-wuliu&quot;&gt;&lt;/i&gt;', '物料管理', null, 'warehouse', null, null, '1438157676', '2');
INSERT INTO `easy_admin_app` VALUES ('55', '53', '53', '1', '1', '&lt;i class=&quot;iconfont  icon-guanli&quot;&gt;&lt;/i&gt;', '基础数据', null, 'warehouse', null, null, '1437213648', '1');
INSERT INTO `easy_admin_app` VALUES ('56', '53', '55', '1', '2', '', '运营商', null, 'warehouse', 'unicom', null, '1436974086', '1');
INSERT INTO `easy_admin_app` VALUES ('57', '53', '55', '1', '2', '', '产品名称', null, 'warehouse', 'producttype', null, '1505293579', '2');
INSERT INTO `easy_admin_app` VALUES ('58', '53', '55', '1', '2', '', '厂商', null, 'warehouse', 'manufacturer', null, '1436975078', '3');
INSERT INTO `easy_admin_app` VALUES ('59', '53', '55', '1', '2', '', '故障类别', null, 'warehouse', 'faulttype', null, '1438157643', '7');
INSERT INTO `easy_admin_app` VALUES ('60', '53', '56', '1', '3', '', '运营商管理', null, 'warehouse', 'unicom', 'index', '1436975246', '1');
INSERT INTO `easy_admin_app` VALUES ('61', '52', '52', '1', '1', '&lt;i class=&quot;iconfont  icon-jianqie&quot;&gt;&lt;/i&gt;', '维修管理', null, 'repair', null, null, '1501592285', '3');
INSERT INTO `easy_admin_app` VALUES ('63', '52', '61', '1', '2', '', '维修管理', null, 'repair', 'service', null, '1449403071', '5');
INSERT INTO `easy_admin_app` VALUES ('64', '52', '177', '1', '2', '', '质量管理', null, 'repair', 'testing', null, '1449403087', '6');
INSERT INTO `easy_admin_app` VALUES ('65', '52', '178', '1', '2', '', '出库管理', null, 'repair', 'outwarehouse', null, '1449403092', '7');
INSERT INTO `easy_admin_app` VALUES ('67', '53', '57', '1', '3', '', '产品名称', null, 'warehouse', 'producttype', 'index', '1505293567', '1');
INSERT INTO `easy_admin_app` VALUES ('68', '53', '58', '1', '3', '', '厂商管理', null, 'warehouse', 'manufacturer', 'index', '1437009597', '1');
INSERT INTO `easy_admin_app` VALUES ('69', '53', '59', '1', '3', '', '故障管理', null, 'warehouse', 'faulttype', 'index', '1438152961', '1');
INSERT INTO `easy_admin_app` VALUES ('72', '53', '56', '0', '3', '', '添加/编辑', null, 'warehouse', 'unicom', 'edit', '1437213579', '2');
INSERT INTO `easy_admin_app` VALUES ('73', '53', '57', '0', '3', '', '编辑/添加', null, 'warehouse', 'producttype', 'edit', '1437213596', '2');
INSERT INTO `easy_admin_app` VALUES ('74', '53', '58', '0', '3', '', '编辑/添加', null, 'warehouse', 'manufacturer', 'edit', '1437213607', '2');
INSERT INTO `easy_admin_app` VALUES ('75', '53', '59', '0', '3', '', '编辑/添加', null, 'warehouse', 'faulttype', 'edit', '1437213616', '2');
INSERT INTO `easy_admin_app` VALUES ('76', '53', '54', '1', '2', '', '物料管理', null, 'warehouse', 'bom', null, '1438157657', '2');
INSERT INTO `easy_admin_app` VALUES ('77', '53', '76', '1', '3', '', 'BOM管理', null, 'warehouse', 'bom', 'index', '1513333054', '1');
INSERT INTO `easy_admin_app` VALUES ('78', '53', '76', '0', '3', '', 'BOM添加/编辑', null, 'warehouse', 'bom', 'edit', '1513333062', '2');
INSERT INTO `easy_admin_app` VALUES ('79', '53', '76', '1', '3', '', '整机物料', null, 'warehouse', 'bom', 'iphone', '1513332681', '5');
INSERT INTO `easy_admin_app` VALUES ('80', '53', '76', '0', '3', '', '整机物料添加/编辑', null, 'warehouse', 'bom', 'iphoneedit', '1513332691', '6');
INSERT INTO `easy_admin_app` VALUES ('81', '52', '175', '1', '2', '', ' 预入库', null, 'repair', 'mobile', null, '1501581357', '2');
INSERT INTO `easy_admin_app` VALUES ('82', '53', '55', '1', '2', '', '门店管理', null, 'warehouse', 'supplier', null, '1437482772', '5');
INSERT INTO `easy_admin_app` VALUES ('83', '53', '55', '1', '2', '', '库位', null, 'warehouse', 'place', null, '1437274076', '6');
INSERT INTO `easy_admin_app` VALUES ('84', '53', '82', '1', '3', '', '门店号', null, 'warehouse', 'supplier', 'index', '1437482790', '1');
INSERT INTO `easy_admin_app` VALUES ('85', '53', '82', '0', '3', '', '编辑/添加', null, 'warehouse', 'supplier', 'edit', '1437274256', '2');
INSERT INTO `easy_admin_app` VALUES ('86', '53', '83', '1', '3', '', '库位管理', null, 'warehouse', 'place', 'index', '1437275626', '1');
INSERT INTO `easy_admin_app` VALUES ('87', '53', '83', '0', '3', '', ' 编辑/删除', null, 'warehouse', 'place', 'edit', '1437274299', '2');
INSERT INTO `easy_admin_app` VALUES ('88', '52', '81', '1', '3', '', '预入库', null, 'repair', 'mobile', 'index', '1501664213', '1');
INSERT INTO `easy_admin_app` VALUES ('89', '52', '81', '0', '3', '', '添加', null, 'repair', 'mobile', 'add', '1437285169', '2');
INSERT INTO `easy_admin_app` VALUES ('90', '52', '81', '0', '3', '', '编辑', null, 'repair', 'mobile', 'edit', '1437285158', '3');
INSERT INTO `easy_admin_app` VALUES ('91', '52', '81', '0', '3', '', '删除', null, 'repair', 'mobile', 'del', '1437285183', '3');
INSERT INTO `easy_admin_app` VALUES ('93', '52', '81', '0', '3', '', '查看入库详情', null, 'repair', 'mobile', 'reads', '1437492195', '5');
INSERT INTO `easy_admin_app` VALUES ('95', '52', '63', '1', '3', '', '待维修', null, 'repair', 'service', 'index', '1507784195', '1');
INSERT INTO `easy_admin_app` VALUES ('97', '52', '63', '0', '3', '', '维修物料选择弹窗', null, 'repair', 'service', 'add_bom', '1438182765', '4');
INSERT INTO `easy_admin_app` VALUES ('98', '52', '63', '0', '3', '', '选择维修配件(数据保存)', null, 'repair', 'service', 'add_bom_save', '1437325096', '5');
INSERT INTO `easy_admin_app` VALUES ('99', '52', '63', '0', '3', '', '撤销维修配件', null, 'repair', 'service', 'del', '1437301541', '5');
INSERT INTO `easy_admin_app` VALUES ('100', '52', '63', '0', '3', '', '预览维修单BOM', null, 'repair', 'service', 'reads', '1438182805', '6');
INSERT INTO `easy_admin_app` VALUES ('101', '52', '63', '1', '3', '', '维修记录', null, 'repair', 'service', 'logs', '1507784205', '2');
INSERT INTO `easy_admin_app` VALUES ('102', '52', '64', '1', '3', '', '待质检', null, 'repair', 'testing', 'index', '1501814188', '1');
INSERT INTO `easy_admin_app` VALUES ('103', '52', '64', '0', '3', '', '质检审核', null, 'repair', 'testing', 'edit', '1437302378', '2');
INSERT INTO `easy_admin_app` VALUES ('105', '52', '64', '0', '3', '', '导出报表', null, 'repair', 'testing', 'excel', '1437659121', '3');
INSERT INTO `easy_admin_app` VALUES ('106', '52', '65', '1', '3', '', '待出库', null, 'repair', 'outwarehouse', 'index', '1501814220', '0');
INSERT INTO `easy_admin_app` VALUES ('107', '52', '65', '0', '3', '', '出库操作', null, 'repair', 'outwarehouse', 'edit', '1437308126', '2');
INSERT INTO `easy_admin_app` VALUES ('108', '52', '65', '1', '3', '', '已出库', null, 'repair', 'outwarehouse', 'logs', '1501814231', '2');
INSERT INTO `easy_admin_app` VALUES ('109', '52', '63', '0', '3', '', '维修物料清单', null, 'repair', 'service', 'edit', '1438182740', '3');
INSERT INTO `easy_admin_app` VALUES ('110', '53', '53', '1', '1', '&lt;i class=&quot;iconfont  icon-tisheng&quot;&gt;&lt;/i&gt;', '库存管理', null, 'warehouse', null, null, '1437572646', '3');
INSERT INTO `easy_admin_app` VALUES ('115', '52', '52', '1', '1', '&lt;i class=&quot;iconfont  icon-jihuabaobiao&quot;&gt;&lt;/i&gt;', '统计分析', null, 'repair', null, null, '1501591970', '6');
INSERT INTO `easy_admin_app` VALUES ('116', '52', '115', '1', '2', '', '统计分析', null, 'repair', 'counts', null, '1437574627', '2');
INSERT INTO `easy_admin_app` VALUES ('117', '52', '116', '1', '3', '', '统计分析', null, 'repair', 'counts', 'index', '1437574651', '1');
INSERT INTO `easy_admin_app` VALUES ('118', '52', '116', '1', '3', '', '报表导出', null, 'repair', 'counts', 'excel', '1437651527', '2');
INSERT INTO `easy_admin_app` VALUES ('119', '52', '64', '1', '3', '', '已质检', null, 'repair', 'testing', 'logs', '1501814208', '2');
INSERT INTO `easy_admin_app` VALUES ('120', '52', '116', '0', '3', '', ' 导出入库信息表', null, 'repair', 'counts', 'toexcel_a', '1437659152', '3');
INSERT INTO `easy_admin_app` VALUES ('121', '52', '116', '0', '3', '', '导出入库故障表', null, 'repair', 'counts', 'toexcel_b', '1437659175', '3');
INSERT INTO `easy_admin_app` VALUES ('122', '52', '116', '0', '3', '', '导出维修BOM表', null, 'repair', 'counts', 'toexcel_c', '1437659190', '4');
INSERT INTO `easy_admin_app` VALUES ('123', '52', '116', '0', '3', '', '单汇总表', null, 'repair', 'counts', 'all_excel', '1437659243', '5');
INSERT INTO `easy_admin_app` VALUES ('125', '53', '110', '1', '2', '', '库存管理', null, 'warehouse', 'house', null, '1438320581', '1');
INSERT INTO `easy_admin_app` VALUES ('133', '53', '76', '0', '3', '', '整机', null, 'warehouse', 'bom', 'mobile', '1513332862', '3');
INSERT INTO `easy_admin_app` VALUES ('134', '53', '76', '0', '3', '', '整机添加/编辑', null, 'warehouse', 'bom', 'mobileedit', '1513332872', '4');
INSERT INTO `easy_admin_app` VALUES ('135', '52', '63', '0', '3', '', '提交维修单', null, 'repair', 'service', 'bom', '1438182691', '4');
INSERT INTO `easy_admin_app` VALUES ('136', '53', '125', '1', '3', '', '库存管理', null, 'warehouse', 'house', 'index', '1438320644', '1');
INSERT INTO `easy_admin_app` VALUES ('137', '53', '125', '0', '3', '', '增加库存', null, 'warehouse', 'house', 'up', '1438320691', '2');
INSERT INTO `easy_admin_app` VALUES ('138', '53', '125', '0', '3', '', '删除库存', null, 'warehouse', 'house', 'down', '1438320685', '3');
INSERT INTO `easy_admin_app` VALUES ('139', '53', '110', '1', '2', '', '库存历史', null, 'warehouse', 'houselogs', null, '1438320729', '2');
INSERT INTO `easy_admin_app` VALUES ('140', '53', '139', '1', '3', '', '入库记录', null, 'warehouse', 'houselogs', 'up', '1438329172', '1');
INSERT INTO `easy_admin_app` VALUES ('141', '53', '139', '1', '3', '', '出库记录', null, 'warehouse', 'houselogs', 'down', '1438320891', '2');
INSERT INTO `easy_admin_app` VALUES ('142', '53', '139', '0', '3', '', '删除库存记录', null, 'warehouse', 'houselogs', 'del', '1438329921', '3');
INSERT INTO `easy_admin_app` VALUES ('147', '0', '0', '1', '0', '&lt;i class=&quot;iconfont  icon-shouji&quot;&gt;&lt;/i&gt;', '换新', null, 'fornew', null, null, '1449151034', '3');
INSERT INTO `easy_admin_app` VALUES ('148', '0', '0', '1', '0', '&lt;i class=&quot;iconfont  icon-gouwu&quot;&gt;&lt;/i&gt;', '包裹', null, 'package', null, null, '1449151316', '1');
INSERT INTO `easy_admin_app` VALUES ('149', '147', '147', '1', '1', '&lt;i class=&quot;iconfont  icon-xin&quot;&gt;&lt;/i&gt;', '换新管理', null, 'fornew', null, null, '1449152785', '1');
INSERT INTO `easy_admin_app` VALUES ('151', '147', '149', '1', '2', '', '换新录入', null, 'fornew', 'excel', null, '1449308371', '1');
INSERT INTO `easy_admin_app` VALUES ('152', '147', '151', '1', '3', '', '换新录入', null, 'fornew', 'excel', 'index', '1449308375', '1');
INSERT INTO `easy_admin_app` VALUES ('153', '147', '149', '1', '2', '', '换新管理', null, 'fornew', 'service', null, '1449156979', '2');
INSERT INTO `easy_admin_app` VALUES ('154', '147', '153', '1', '3', '', '换新管理', null, 'fornew', 'service', 'index', '1449156994', '1');
INSERT INTO `easy_admin_app` VALUES ('155', '147', '153', '1', '3', '', '换新历史', null, 'fornew', 'service', 'logs', '1449157020', '2');
INSERT INTO `easy_admin_app` VALUES ('156', '147', '149', '1', '2', '', '入库管理', null, 'fornew', 'addwarehouse', null, '1449157073', '3');
INSERT INTO `easy_admin_app` VALUES ('157', '147', '156', '1', '3', '', '入库管理', null, 'fornew', 'addwarehouse', 'index', '1449157087', '1');
INSERT INTO `easy_admin_app` VALUES ('158', '147', '156', '1', '3', '', '入库历史', null, 'fornew', 'addwarehouse', 'logs', '1449157104', '2');
INSERT INTO `easy_admin_app` VALUES ('159', '147', '149', '1', '2', '', '故障管理', null, 'fornew', 'fault', null, '1449157208', '4');
INSERT INTO `easy_admin_app` VALUES ('160', '147', '159', '1', '3', '', '故障管理', null, 'fornew', 'fault', 'index', '1449157229', '1');
INSERT INTO `easy_admin_app` VALUES ('162', '147', '147', '1', '1', '&lt;i class=&quot;iconfont  icon-jihuabaobiao&quot;&gt;&lt;/i&gt;', '统计分析', null, 'fornew', null, null, '1449157360', '2');
INSERT INTO `easy_admin_app` VALUES ('163', '147', '162', '1', '2', '', '统计分析', null, 'fornew', 'counts', null, '1449157386', '1');
INSERT INTO `easy_admin_app` VALUES ('164', '147', '163', '1', '3', '', '统计分析', null, 'fornew', 'counts', 'index', '1449157409', '1');
INSERT INTO `easy_admin_app` VALUES ('165', '147', '163', '1', '3', '', '数据导出', null, 'fornew', 'counts', 'excel', '1449157430', '2');
INSERT INTO `easy_admin_app` VALUES ('166', '148', '148', '1', '1', '&lt;i class=&quot;iconfont  icon-gouwu&quot;&gt;&lt;/i&gt;', '包裹管理', null, 'package', null, null, '1449159306', '1');
INSERT INTO `easy_admin_app` VALUES ('167', '148', '166', '1', '2', '', '包裹管理', null, 'package', 'package', null, '1449159340', '1');
INSERT INTO `easy_admin_app` VALUES ('168', '148', '167', '1', '3', '', '包裹管理', null, 'package', 'package', 'index', '1449159364', '1');
INSERT INTO `easy_admin_app` VALUES ('171', '52', '175', '1', '2', '', '入库管理', null, 'repair', 'addwarehouse', null, '1501580844', '3');
INSERT INTO `easy_admin_app` VALUES ('172', '52', '171', '1', '3', '', '入库管理', null, 'repair', 'addwarehouse', 'index', '1505641612', '2');
INSERT INTO `easy_admin_app` VALUES ('173', '52', '171', '1', '3', '', '入库历史', null, 'repair', 'addwarehouse', 'logs', '1505641622', '3');
INSERT INTO `easy_admin_app` VALUES ('175', '52', '52', '1', '1', '&lt;i class=&quot;iconfont  icon-shouji&quot;&gt;&lt;/i&gt;', '库存管理', null, 'repair', null, null, '1501574949', '1');
INSERT INTO `easy_admin_app` VALUES ('176', '52', '52', '1', '1', '&lt;i class=&quot;iconfont  icon-sousuowenjian&quot;&gt;&lt;/i&gt;', '故障诊断', null, 'repair', null, null, '1501592315', '2');
INSERT INTO `easy_admin_app` VALUES ('177', '52', '52', '1', '1', '&lt;i class=&quot;iconfont  icon-dengdai&quot;&gt;&lt;/i&gt;', '维修质检', null, 'repair', null, null, '1501592269', '4');
INSERT INTO `easy_admin_app` VALUES ('178', '52', '52', '1', '1', '&lt;i class=&quot;iconfont  icon-tuichu&quot;&gt;&lt;/i&gt;', '出库管理', null, 'repair', null, null, '1501592163', '5');
INSERT INTO `easy_admin_app` VALUES ('179', '53', '55', '1', '2', '', 'IMEI库管理', null, 'warehouse', 'imei', null, '1501593029', '6');
INSERT INTO `easy_admin_app` VALUES ('180', '53', '179', '1', '3', '', 'IMEI库管理', null, 'warehouse', 'imei', 'index', '1501594562', '1');
INSERT INTO `easy_admin_app` VALUES ('181', '53', '179', '0', '3', '', '编辑/删除', null, 'warehouse', 'imei', 'edit', '1501593120', '2');
INSERT INTO `easy_admin_app` VALUES ('182', '52', '176', '1', '2', '', '故障诊断', null, 'repair', 'diacrisis', null, '1501658137', '1');
INSERT INTO `easy_admin_app` VALUES ('183', '52', '182', '1', '3', '', '待诊断', null, 'repair', 'diacrisis', 'index', '1501658193', '1');
INSERT INTO `easy_admin_app` VALUES ('184', '52', '182', '1', '3', '', '已诊断', null, 'repair', 'diacrisis', 'logs', '1501658245', '2');
INSERT INTO `easy_admin_app` VALUES ('185', '53', '55', '1', '2', '', '区域管理', null, 'warehouse', 'region', null, '1507774439', '2');
INSERT INTO `easy_admin_app` VALUES ('186', '53', '185', '1', '3', '', '区域管理', null, 'warehouse', 'region', 'index', '1507774463', '1');
INSERT INTO `easy_admin_app` VALUES ('187', '53', '185', '0', '3', '', '添加/管理', null, 'warehouse', 'region', 'edit', '1507774482', '1');

-- ----------------------------
-- Table structure for `easy_admin_group`
-- ----------------------------
DROP TABLE IF EXISTS `easy_admin_group`;
CREATE TABLE `easy_admin_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `auth_value` text,
  `uptime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of easy_admin_group
-- ----------------------------
INSERT INTO `easy_admin_group` VALUES ('1', 'SYSADMIN', '[\"-1\"]', '1318860709');
INSERT INTO `easy_admin_group` VALUES ('7', '系统管理员', '[\"repair_mobile_index\",\"repair_mobile_add\",\"repair_mobile_del\",\"repair_mobile_edit\",\"repair_mobile_reads\",\"repair_service_index\",\"repair_service_logs\",\"repair_service_add_bom\",\"repair_service_del\",\"repair_service_add_bom_save\",\"repair_service_reads\",\"repair_testing_index\",\"repair_testing_edit\",\"repair_testing_excel\",\"repair_outwarehouse_index\",\"repair_outwarehouse_logs\",\"repair_outwarehouse_edit\",\"warehouse_unicom_index\",\"warehouse_unicom_edit\",\"warehouse_producttype_index\",\"warehouse_producttype_edit\",\"warehouse_manufacturer_index\",\"warehouse_manufacturer_edit\",\"warehouse_supplier_index\",\"warehouse_supplier_edit\",\"warehouse_place_index\",\"warehouse_place_edit\",\"warehouse_faulttype_index\",\"warehouse_faulttype_edit\",\"warehouse_bom_index\",\"warehouse_bom_edit\",\"warehouse_bom_iphone\",\"warehouse_bom_iphoneedit\",\"admin_user_index\",\"admin_user_edit\",\"admin_user_password\",\"admin_app_index\",\"admin_app_edit\",\"admin_group_index\",\"admin_default_cache\",\"admin_default_menu\",\"admin_default_index\",\"admin_default_welcome\",\"admin_database_index\",\"admin_database_tables\",\"admin_database_backupdb\",\"admin_database_optimizeddb\",\"admin_database_repairdb\",\"admin_database_delectdb\",\"admin_database_importdb\",\"admin_database_downdb\"]', '1501657988');

-- ----------------------------
-- Table structure for `easy_phone_bom`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_bom`;
CREATE TABLE `easy_phone_bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `types` tinyint(4) DEFAULT '0',
  `producttype_id` int(11) DEFAULT NULL,
  `product` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `place_id` int(11) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL COMMENT '代码',
  `warning` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `amount` int(11) DEFAULT '0' COMMENT '数量',
  `parameter` varchar(255) DEFAULT NULL COMMENT '规格',
  `uptime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='物料BOM表';

-- ----------------------------
-- Records of easy_phone_bom
-- ----------------------------
INSERT INTO `easy_phone_bom` VALUES ('8', '0', '2', '魅族Pro6', '1', '珠海魅族通讯科技有限公司', '3', '北京仓库', '10001', '10', '手机主板', '1', 'HTC60478', '1507749831');
INSERT INTO `easy_phone_bom` VALUES ('9', '0', '3', '小米MIX2', '2', '北京小米科技有限公司', '2', '广州仓库', '10002', '10', '触摸屏组', '9', 'DC6570A', '1513332487');
INSERT INTO `easy_phone_bom` VALUES ('17', '1', '1', '魅族MX5', '1', '珠海魅族通讯科技有限公司', '2', '广州仓库', '2017101253505251', '1', '魅族MX5', '2', '魅族MX5', '1507779759');

-- ----------------------------
-- Table structure for `easy_phone_bom_logs`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_bom_logs`;
CREATE TABLE `easy_phone_bom_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bom_id` int(11) DEFAULT NULL COMMENT 'bomid',
  `place_id` int(11) DEFAULT NULL,
  `types` tinyint(2) DEFAULT '1' COMMENT '1入库 0出库',
  `amount` int(11) DEFAULT NULL COMMENT '数量',
  `admin_name` varchar(255) DEFAULT NULL COMMENT '管理员',
  `uptime` int(11) DEFAULT NULL,
  `down_name` varchar(255) DEFAULT NULL COMMENT '领取人',
  `imei` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='物料BOM出入库纪录';

-- ----------------------------
-- Records of easy_phone_bom_logs
-- ----------------------------
INSERT INTO `easy_phone_bom_logs` VALUES ('18', '9', '3', '1', '10', 'admin', '1507751716', null, null);
INSERT INTO `easy_phone_bom_logs` VALUES ('19', '9', '3', '0', '1', 'admin', '1507751785', '1', null);
INSERT INTO `easy_phone_bom_logs` VALUES ('20', '8', '3', '1', '1', 'admin', '1507752019', null, '110100101');
INSERT INTO `easy_phone_bom_logs` VALUES ('21', '8', '3', '1', '1', 'admin', '1507752024', null, '101010101101');
INSERT INTO `easy_phone_bom_logs` VALUES ('22', '8', '3', '0', '1', 'admin', '1507752036', 'admin', '101010101101');
INSERT INTO `easy_phone_bom_logs` VALUES ('24', '17', '2', '1', '1', 'admin', '1507779759', null, '10010');
INSERT INTO `easy_phone_bom_logs` VALUES ('26', '17', '2', '0', '1', 'admin', '1509096651', null, '10010');
INSERT INTO `easy_phone_bom_logs` VALUES ('27', '17', '2', '1', '1', 'admin', '1509341526', null, '10101010');
INSERT INTO `easy_phone_bom_logs` VALUES ('28', '17', '2', '1', '1', 'admin', '1509341534', null, '10101010101');

-- ----------------------------
-- Table structure for `easy_phone_bom_phone`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_bom_phone`;
CREATE TABLE `easy_phone_bom_phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `uptime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='手机管理';

-- ----------------------------
-- Records of easy_phone_bom_phone
-- ----------------------------
INSERT INTO `easy_phone_bom_phone` VALUES ('5', 'MX5', '魅族MX5', '1507749261');

-- ----------------------------
-- Table structure for `easy_phone_bom_phone_lists`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_bom_phone_lists`;
CREATE TABLE `easy_phone_bom_phone_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bom_phone_id` int(11) DEFAULT NULL COMMENT '代码',
  `bom_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='手机的BOM物料';

-- ----------------------------
-- Records of easy_phone_bom_phone_lists
-- ----------------------------
INSERT INTO `easy_phone_bom_phone_lists` VALUES ('18', '5', '9', '4');
INSERT INTO `easy_phone_bom_phone_lists` VALUES ('20', '5', '8', '1');

-- ----------------------------
-- Table structure for `easy_phone_faulttype`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_faulttype`;
CREATE TABLE `easy_phone_faulttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='故障类别基础库';

-- ----------------------------
-- Records of easy_phone_faulttype
-- ----------------------------
INSERT INTO `easy_phone_faulttype` VALUES ('1', '0', '001', '开机/关机/死机故障', '1437273800');
INSERT INTO `easy_phone_faulttype` VALUES ('2', '1', '00101', '不开机', '1437273842');
INSERT INTO `easy_phone_faulttype` VALUES ('5', '1', '00104', '自动重启', '1437273842');
INSERT INTO `easy_phone_faulttype` VALUES ('6', '1', '00106\r\n', '自动关机\r\n', '1437273842');
INSERT INTO `easy_phone_faulttype` VALUES ('7', '1', '00108\r\n', '开机不保持\r\n', '1437273842');
INSERT INTO `easy_phone_faulttype` VALUES ('8', '1', '00119', '待机时死机', '1437273842');
INSERT INTO `easy_phone_faulttype` VALUES ('9', '1', '00124', '插卡死机\r\n', '1437273842');
INSERT INTO `easy_phone_faulttype` VALUES ('10', '0', '002', '通话/短信类故障', '1438333120');
INSERT INTO `easy_phone_faulttype` VALUES ('11', '10', '00201', '无法呼入', '1438333136');
INSERT INTO `easy_phone_faulttype` VALUES ('12', '10', '00201', '00201', '1438333149');
INSERT INTO `easy_phone_faulttype` VALUES ('13', '10', '00204', '通话回音', '1438333160');

-- ----------------------------
-- Table structure for `easy_phone_fornew`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_fornew`;
CREATE TABLE `easy_phone_fornew` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) DEFAULT '0' COMMENT '0:录入1换新2入库3故障',
  `number` varchar(20) DEFAULT '0' COMMENT '跟踪号',
  `imei` varchar(100) DEFAULT NULL COMMENT 'IMEI编号',
  `ems_sn` varchar(100) DEFAULT NULL COMMENT '快递单号',
  `ems_time` int(11) DEFAULT NULL,
  `ems_take_time` int(11) DEFAULT NULL COMMENT '邮寄收获时间',
  `come_address` varchar(255) DEFAULT NULL,
  `shop_time` int(11) DEFAULT NULL COMMENT '购买时间',
  `address_time` int(11) DEFAULT NULL COMMENT '门店最晚收获时间',
  `producttype_id` int(11) DEFAULT NULL COMMENT '产品类型',
  `manufacturer_id` int(11) DEFAULT NULL COMMENT 'manufacturer_id',
  `unicom_id` int(11) DEFAULT NULL COMMENT 'unicom_id',
  `supplier_id` int(11) DEFAULT NULL COMMENT '门店号',
  `supplier_code` varchar(100) DEFAULT NULL COMMENT '门店号',
  `fault_why` varchar(255) DEFAULT NULL COMMENT '故障描述',
  `add_time` int(11) DEFAULT '0' COMMENT ' 入库时间',
  `add_admin_name` varchar(100) DEFAULT NULL COMMENT '入库人员',
  `add_admin_id` int(11) DEFAULT '0' COMMENT '入库用户ID',
  `fault_level` varchar(10) DEFAULT NULL COMMENT '故障级别',
  `faulttype_id` varchar(200) DEFAULT NULL COMMENT '故障大类ID',
  `faulttype` varchar(200) DEFAULT NULL COMMENT '故障小类',
  `new_imei` varchar(100) DEFAULT NULL COMMENT '新机IMEI码',
  `service_admin_id` int(11) DEFAULT '0' COMMENT '换新用户',
  `service_admin_name` varchar(100) DEFAULT NULL,
  `service_time` int(11) DEFAULT '0' COMMENT '换新时间',
  `out_time` int(11) DEFAULT NULL COMMENT '换新出库时间',
  `outwarehouse_ems_sn` varchar(100) DEFAULT NULL,
  `outwarehouse_address` varchar(255) DEFAULT NULL,
  `addwarehouse_admin_id` int(11) DEFAULT NULL,
  `addwarehouse_admin_name` varchar(100) DEFAULT NULL,
  `addwarehouse_time` int(11) DEFAULT NULL,
  `overtime` varchar(20) DEFAULT NULL COMMENT '是否保内',
  `place_id` int(11) DEFAULT NULL COMMENT '库位',
  `failure_cause` varchar(255) DEFAULT NULL,
  `testing_admin_id` int(11) DEFAULT NULL,
  `testing_admin_name` varchar(100) DEFAULT NULL,
  `testing_time` int(11) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='换新入库';

-- ----------------------------
-- Records of easy_phone_fornew
-- ----------------------------

-- ----------------------------
-- Table structure for `easy_phone_fornew_bom`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_fornew_bom`;
CREATE TABLE `easy_phone_fornew_bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_id` int(11) DEFAULT NULL COMMENT '手机ID',
  `bom_id` int(11) DEFAULT NULL COMMENT 'BomID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='换新手机';

-- ----------------------------
-- Records of easy_phone_fornew_bom
-- ----------------------------

-- ----------------------------
-- Table structure for `easy_phone_imei`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_imei`;
CREATE TABLE `easy_phone_imei` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `new_imei` varchar(255) DEFAULT NULL,
  `old_imei` varchar(255) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL,
  `producttype_name` varchar(255) DEFAULT NULL,
  `manufacturer_name` varchar(255) DEFAULT NULL,
  `unicom_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='IMEI库';

-- ----------------------------
-- Records of easy_phone_imei
-- ----------------------------

-- ----------------------------
-- Table structure for `easy_phone_manufacturer`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_manufacturer`;
CREATE TABLE `easy_phone_manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='厂商基础库';

-- ----------------------------
-- Records of easy_phone_manufacturer
-- ----------------------------
INSERT INTO `easy_phone_manufacturer` VALUES ('1', '珠海魅族通讯科技有限公司', '珠海魅族通讯科技有限公司', '1507749046');
INSERT INTO `easy_phone_manufacturer` VALUES ('2', '北京小米科技有限公司', '北京小米科技有限公司', '1507749061');

-- ----------------------------
-- Table structure for `easy_phone_mobile`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_mobile`;
CREATE TABLE `easy_phone_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) DEFAULT '0' COMMENT '0 待入库 1已入库',
  `number` varchar(20) DEFAULT '0' COMMENT '跟踪号',
  `product_count` int(11) DEFAULT NULL COMMENT '手机名称',
  `imei` varchar(100) DEFAULT NULL COMMENT 'IMEI编号',
  `ems_sn` varchar(100) DEFAULT NULL COMMENT '快递单号',
  `ems_time` int(11) DEFAULT NULL COMMENT '邮寄时间',
  `ems_take_time` int(11) DEFAULT NULL COMMENT '邮寄收货时间',
  `accept_time` int(11) DEFAULT NULL COMMENT '门店受理时间',
  `shop_time` int(11) DEFAULT '0' COMMENT '购机时间',
  `producttype_id` int(11) DEFAULT NULL COMMENT '产品类型',
  `place_id` int(11) DEFAULT NULL COMMENT '库位',
  `manufacturer_id` int(11) DEFAULT NULL COMMENT '厂商ID',
  `unicom_id` int(11) DEFAULT NULL COMMENT 'unicom_id',
  `supplier_id` int(11) DEFAULT NULL,
  `supplier_code` varchar(100) DEFAULT NULL COMMENT '门店号',
  `fault_why` varchar(255) DEFAULT NULL COMMENT '客户故障描述',
  `overtime` varchar(10) DEFAULT NULL,
  `is_urgent` tinyint(1) DEFAULT '0' COMMENT '0不加急，1加急',
  `is_rmb` tinyint(1) DEFAULT '0' COMMENT '0不付费，1付费',
  `fault_level` varchar(10) DEFAULT NULL COMMENT '故障级别',
  `failure_cause` varchar(255) DEFAULT NULL COMMENT '实际故障原因',
  `faulttype_id` varchar(200) DEFAULT NULL COMMENT '故障大类ID',
  `faulttype` varchar(200) DEFAULT NULL COMMENT '故障小类',
  `diacrisis_time` int(11) DEFAULT NULL COMMENT '诊断时间',
  `diacrisis_admin_name` varchar(100) DEFAULT NULL COMMENT '诊断用户',
  `diacrisis_admin_id` int(11) DEFAULT NULL COMMENT '诊断用户ID',
  `add_time` int(11) DEFAULT '0' COMMENT ' 入库时间',
  `add_admin_name` varchar(100) DEFAULT NULL COMMENT '入库人员',
  `add_admin_id` int(11) DEFAULT '0' COMMENT '入库用户ID',
  `addwarehouse_admin_id` int(11) DEFAULT NULL,
  `addwarehouse_admin_name` varchar(50) DEFAULT NULL,
  `addwarehouse_time` int(11) DEFAULT NULL,
  `plan_time` int(11) DEFAULT '0' COMMENT '派单时间',
  `plan_admin_name` varchar(100) DEFAULT NULL COMMENT '派单人员',
  `service_mode` varchar(255) DEFAULT NULL COMMENT '建议维修方式',
  `service_admin_name` varchar(100) DEFAULT NULL,
  `service_admin_id` int(11) DEFAULT '0' COMMENT '维修用户',
  `service_time` int(11) DEFAULT '0' COMMENT '质检时间',
  `is_news_phone` tinyint(1) DEFAULT '0' COMMENT '1换新 0不换',
  `new_imei` varchar(100) DEFAULT NULL COMMENT '如果换新 新机IMEI码',
  `testing_admin_name` varchar(100) DEFAULT NULL COMMENT '质检员',
  `testing_admin_id` int(11) DEFAULT '0' COMMENT '审核用户',
  `testing_text` varchar(255) DEFAULT NULL COMMENT '审核意见',
  `testing_time` int(11) DEFAULT NULL COMMENT '质检时间',
  `is_testing` tinyint(1) DEFAULT '0',
  `outwarehouse_admin_name` varchar(100) DEFAULT NULL,
  `outwarehouse_admin_id` int(11) DEFAULT '0' COMMENT '发货用户ID',
  `outwarehouse_ems_sn` varchar(100) DEFAULT NULL,
  `outwarehouse_address` varchar(255) DEFAULT NULL,
  `out_time` int(11) DEFAULT NULL COMMENT '出库时间',
  `uptime` int(11) DEFAULT NULL COMMENT '最后更新时间',
  `package_id` int(11) DEFAULT '0' COMMENT '包裹ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='收件入库';

-- ----------------------------
-- Records of easy_phone_mobile
-- ----------------------------
INSERT INTO `easy_phone_mobile` VALUES ('1', '2', '2017101253505251', '1', '10010', '56054650', '1506787200', '1507478400', '1507737600', '1506960000', '1', '2', '1', '3', '1', '25015', '开不了机器了', '是', '0', '0', null, '10', null, null, '1507782423', 'admin', '1', '1507776901', 'admin', '1', '1', 'admin', '1507779759', '0', null, '软件刷新', null, '0', '0', '0', null, 'admin', '1', '1212', '1513331017', '1', 'admin', '1', '10', '10', '1509096651', '1507778466', '0');
INSERT INTO `easy_phone_mobile` VALUES ('2', '0', '2017102753505153', '0', '12', '11', '1507478400', '1507478400', '1507564800', '0', '1', null, '2', '3', '2', '12', '12', null, '0', '0', null, null, null, null, null, null, null, '1509067093', 'admin', '1', null, null, null, '0', null, null, null, '0', '0', '0', null, null, '0', null, null, null, null, '0', null, null, null, '1509067093', '0');
INSERT INTO `easy_phone_mobile` VALUES ('3', '0', '2017102797981001', null, '1010', '4000', '-2121926400', '1488643200', '1488643200', '0', '1', null, '1', '3', '1', '2222', null, null, '0', '0', null, null, null, null, null, null, null, '1509093018', 'admin', '1', null, null, null, '0', null, null, null, '0', '0', '0', null, null, '0', null, null, null, null, '0', null, null, null, '1509093018', '0');

-- ----------------------------
-- Table structure for `easy_phone_mobile_bom`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_mobile_bom`;
CREATE TABLE `easy_phone_mobile_bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_id` int(11) DEFAULT NULL COMMENT '手机ID',
  `bom_id` int(11) DEFAULT NULL COMMENT 'BomID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='手机BOM配件';

-- ----------------------------
-- Records of easy_phone_mobile_bom
-- ----------------------------
INSERT INTO `easy_phone_mobile_bom` VALUES ('1', '1', '8');
INSERT INTO `easy_phone_mobile_bom` VALUES ('2', '1', '9');

-- ----------------------------
-- Table structure for `easy_phone_package`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_package`;
CREATE TABLE `easy_phone_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(10) DEFAULT NULL COMMENT '门店ID',
  `unicom_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `area` varchar(100) DEFAULT NULL COMMENT '区域',
  `status` int(11) DEFAULT '0',
  `gprs_sn` varchar(50) DEFAULT NULL COMMENT '跟踪号',
  `ems_name` varchar(100) DEFAULT NULL COMMENT '快递公司名称',
  `ems_sn` varchar(50) DEFAULT NULL COMMENT '快递单号',
  `ems_time` int(11) DEFAULT NULL,
  `types_id` tinyint(1) DEFAULT '0' COMMENT '0维修或1换新',
  `counts` int(11) NOT NULL,
  `uptime` int(11) DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='包裹';

-- ----------------------------
-- Records of easy_phone_package
-- ----------------------------
INSERT INTO `easy_phone_package` VALUES ('20', '1', '4', '2', '4', '0', '0', '00110', '00', '00', '1507737600', '0', '0', '1509084926');

-- ----------------------------
-- Table structure for `easy_phone_place`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_place`;
CREATE TABLE `easy_phone_place` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `admin_name` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='库位管理';

-- ----------------------------
-- Records of easy_phone_place
-- ----------------------------
INSERT INTO `easy_phone_place` VALUES ('2', '广州仓库', '小李子', '广州仓库', '1507749140');
INSERT INTO `easy_phone_place` VALUES ('3', '北京仓库', '老王', '北京仓库', '1507749154');

-- ----------------------------
-- Table structure for `easy_phone_producttype`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_producttype`;
CREATE TABLE `easy_phone_producttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='产品类型基础库';

-- ----------------------------
-- Records of easy_phone_producttype
-- ----------------------------
INSERT INTO `easy_phone_producttype` VALUES ('1', '魅族MX5', '产品名称', '1507748857');
INSERT INTO `easy_phone_producttype` VALUES ('2', '魅族Pro6', '魅族Pro6', '1507749030');
INSERT INTO `easy_phone_producttype` VALUES ('3', '小米MIX2', '小米MIX2', '1507749795');

-- ----------------------------
-- Table structure for `easy_phone_region`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_region`;
CREATE TABLE `easy_phone_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='区域';

-- ----------------------------
-- Records of easy_phone_region
-- ----------------------------
INSERT INTO `easy_phone_region` VALUES ('3', '华中地区', '', '1507775879');
INSERT INTO `easy_phone_region` VALUES ('4', '华北地区', '', '1507775896');

-- ----------------------------
-- Table structure for `easy_phone_service`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_service`;
CREATE TABLE `easy_phone_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_id` int(11) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0:新单1待维修2维修完成3检修4出库',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='维修管理';

-- ----------------------------
-- Records of easy_phone_service
-- ----------------------------

-- ----------------------------
-- Table structure for `easy_phone_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_supplier`;
CREATE TABLE `easy_phone_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='门店号';

-- ----------------------------
-- Records of easy_phone_supplier
-- ----------------------------
INSERT INTO `easy_phone_supplier` VALUES ('1', '200158', '北京西直门认证店', '1507749082');
INSERT INTO `easy_phone_supplier` VALUES ('2', '200510', '上海东方明珠旗舰店', '1507749110');

-- ----------------------------
-- Table structure for `easy_phone_unicom`
-- ----------------------------
DROP TABLE IF EXISTS `easy_phone_unicom`;
CREATE TABLE `easy_phone_unicom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `uptime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='运营商基础库';

-- ----------------------------
-- Records of easy_phone_unicom
-- ----------------------------
INSERT INTO `easy_phone_unicom` VALUES ('3', '中国移动', '', '1507749004');
INSERT INTO `easy_phone_unicom` VALUES ('4', '中国联通', '', '1507749017');

-- ----------------------------
-- Table structure for `easy_testing_logs`
-- ----------------------------
DROP TABLE IF EXISTS `easy_testing_logs`;
CREATE TABLE `easy_testing_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moblie_id` int(11) DEFAULT NULL,
  `testing_admin_name` varchar(100) DEFAULT NULL COMMENT '质检员',
  `testing_admin_id` int(11) DEFAULT '0' COMMENT '审核用户',
  `testing_text` varchar(255) DEFAULT NULL COMMENT '审核意见',
  `testing_time` int(11) DEFAULT NULL COMMENT '质检时间',
  `is_pass` tinyint(1) DEFAULT '0' COMMENT '0不通过1通过',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='质检结论';

-- ----------------------------
-- Records of easy_testing_logs
-- ----------------------------
INSERT INTO `easy_testing_logs` VALUES ('1', '1', 'admin', '1', '1212', '1513331017', '1');
