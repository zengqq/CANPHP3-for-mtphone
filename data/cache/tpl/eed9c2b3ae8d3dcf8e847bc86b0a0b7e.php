<?php exit;?>001540630567764b364f887f4e4c10589d36e647865es:4206:"a:2:{s:8:"template";s:4142:"<div class="path">
    <i class="iconfont icon-shouye1"></i>
    <a href="<?php echo url('admin/default/welcome');?>">开始</a> / 
    <a href="<?php echo url('service/index');?>">维修任务</a>
</div>
<div class="table-head fn-clear">
    <div class="fn-l">
        <a href="<?php echo url('service/index');?>" class="button">
            <i class="iconfont icon-xiangzuo1"></i> 返回
        </a>
    </div>
</div>
<div class="m10">
<form id="form" action="<?php echo url('service/edit');?>">
<table class="table table-border">
    <tr class="bg-f5">
      <th>跟踪号:</th>
      <td><?php echo $info['number']; ?></td>
      <th>IMEI号:</th>
      <td><?php echo $info['imei']; ?></td>
    </tr>
    <tr class="bg-f5">
      <th>厂商:</th>
      <td><?php echo $info['manufacturer_name']; ?></td>
      <th>机型:</th>
      <td><?php echo $info['producttype_name']; ?></td>
    </tr>
    <tr class="bg-f5">
      <th>购买日期:</th>
      <td><?php if($info['shop_time']) { ?><?php echo date('Y-m-d',$info['shop_time']);?><?php } ?></td>
      <th>维修工程师:</th>
      <td><?php echo $userInfo['username']; ?></td>
    </tr>
    <tr class="bg-f5">
      <th>是否保内:</th>
      <td><?php if($info['overtime'] == '是') { ?><span class="badge">保内</span><?php } else { ?><span class="badge bg-red">过保</span><?php } ?></td>
      <th>是否紧急:</th>
      <td><?php if($info['is_urgent']) { ?><span class="badge bg-red">加急</span><?php } else { ?><span class="badge">普通</span><?php } ?></td>
    </tr>
    <tr class="bg-f5">
      <th>客户描述故障:</th>
      <td colspan="3"><?php echo $info['fault_why']; ?></td>
    </tr>  
    <tr>
      <th>检查实际故障:</th>
      <td colspan="3"><?php echo $info['failure_cause']; ?></td>
    </tr>
    <tr>
    <th>选物料:</th>
    <td colspan="3">
      <a title="选择维修使用的物料配件" href="javascript:void(0);" url="<?php echo url('service/add_bom',array('id'=>$info['id']));?>" class="button button-xifen win">
      <i class="iconfont icon-xiangxia"></i> 选择维修使用的物料配件
      </a>
    </td>
    </tr>
</table>
<div class="table-head fn-clear"> 
    <div class="fn-l">
    使用维修物料
    </div>
    <div class="fn-r">
        <a href="<?php echo url('service/edit',['id'=>$id,'is_excel'=>1]);?>" class="button button-blue">
            <i class="iconfont icon-tianjia"></i> 导出
        </a>
        <a href="<?php echo url('service/edit',['id'=>$id,'print'=>1]);?>" class="button" target="_blank">
            <i class="iconfont icon-dayinji"></i> 打印
        </a>
    </div>    
</div>
<table class="table table-border">
<thead>
    <tr>
        <td class="fn-tac w60">#</td>
        <td>产品名称</td>
        <td class="w100 fn-tac">物料代码</td>
        <td>物料名称</td>
        <td>规格型号</td>
        <td class="fn-tac w60">操作</td>
    </tr>
</thead>
<tbody>
<?php $n=1;if(is_array($list)) foreach($list AS $vo) { ?>
<tr>
    <td class="fn-tac"><?php echo $vo['id']; ?></td>
    <td><?php echo $vo['product']; ?></td>
    <td class="fn-tac"><?php echo $vo['code']; ?></td>
    <td><?php echo $vo['name']; ?></td>
    <td><?php echo $vo['parameter']; ?></td>
    <td class="fn-tac tip">
    <a href="javascript:void(0);" class="del" title="撤销" url="<?php echo url('service/del',array(bom_id=>$vo['bom_id'],id=>$vo['id'],act=>del));?>">撤销</a></td>
</tr>
<?php $n++;}unset($n); ?>
</tbody>
</table>
</div>
<div class="btn fn-tac">
    <a href="<?php echo url('service/bom',array('id'=>$id));?>" class="button button-blue">
          <i class="iconfont icon-xiangxia"></i> 下一步填写维修信息
    </a>
    <a href="<?php echo url('service/index');?>" class="button button-yellow">
          <i class="iconfont icon-xiangzuo1"></i> 返回
    </a>
</div>
<script type="text/javascript">
Do('base','form','date','dialog',function(){ 
    $('.win').click(function(){$(this).win();});
    $('.del').click(function(){$(this).del();});
})
</script>";s:12:"compile_time";i:1509094567;}";