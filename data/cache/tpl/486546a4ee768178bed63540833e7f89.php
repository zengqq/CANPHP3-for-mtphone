<?php exit;?>0015406293149ff6524b1dd2e4449d49d05186d22067s:5782:"a:2:{s:8:"template";s:5718:"<div class="path">
    <i class="iconfont icon-shouye1"></i>
    <a href="<?php echo url('admin/default/welcome');?>">开始</a> / 
    <a href="javascript:void(0);">收件入库</a>
</div>
<form id="form" action="<?php echo url('mobile/index');?>" method="post">
<div class="table-search fn-clear">
<ul>
<li>
    <span class="fn-tar">IMEI号:</span>
    <span class="sinput"><input class="input w80" type="text" name="imei" value="<?php echo $imei; ?>" /></span>
</li>
<li>
    <span class="fn-tar">门店号:</span>
    <span class="sinput">
<select class="select" name="supplier_id">
    <option value="" selected="selected"></option>
<?php $n=1;if(is_array($supplier_lists)) foreach($supplier_lists AS $vo) { ?> 
    <option value="<?php echo $vo['id']; ?>" <?php if($supplier_id ==$vo['id']) { ?> selected="selected"<?php } ?>><?php echo $vo['name']; ?></option>
<?php $n++;}unset($n); ?>
</select>
    </span>
</li>
<li>
    <span class="fn-tar">跟踪号:</span>
    <span class="sinput"><input class="input w80" type="text" name="number" value="<?php echo $number; ?>" /></span>
</li>
<li>
    <span class="fn-tar">DHL快递号:</span>
    <span class="sinput"><input class="input w60" type="text" name="ems_sn" value="<?php echo $ems_sn; ?>" /></span>
</li>
<li>  
    <span class="fn-tar">产品名称:</span>
    <span class="sinput"><input class="input w100" type="text" name="mobile_name" value="<?php echo $mobile_name; ?>" /></span>
</li>
<li>
    <span class="fn-tar">产品类型:</span>
    <span class="sinput">
<select class="select" name="producttype_id">
    <option value="" <?php if(!$producttype_id) { ?> selected="selected"<?php } ?>></option>
<?php $n=1;if(is_array($producttype_lists)) foreach($producttype_lists AS $vo) { ?> 
    <option value="<?php echo $vo['id']; ?>" <?php if($producttype_id ==$vo['id']) { ?> selected="selected"<?php } ?>><?php echo $vo['name']; ?></option>
<?php $n++;}unset($n); ?>
</select>
    </span>
</li>
<li>
    <span class="fn-tar">厂商:</span>
    <span class="sinput">
<select class="select" name="manufacturer_id">
    <option value="" <?php if(!$manufacturer_id) { ?> selected="selected"<?php } ?>></option>
<?php $n=1;if(is_array($manufacturer_lists)) foreach($manufacturer_lists AS $vo) { ?> 
    <option value="<?php echo $vo['id']; ?>" <?php if($manufacturer_id ==$vo['id']) { ?> selected="selected"<?php } ?>><?php echo $vo['name']; ?></option>
<?php $n++;}unset($n); ?>
</select>
    </span>
</li>
<li>
    <span class="fn-tar">运营商:</span>
    <span class="sinput">
<select class="select" name="unicom_id">
    <option value="" <?php if(!$unicom_id) { ?> selected="selected"<?php } ?>></option>
<?php $n=1;if(is_array($unicom_lists)) foreach($unicom_lists AS $vo) { ?> 
    <option value="<?php echo $vo['id']; ?>" <?php if($unicom_id ==$vo['id']) { ?> selected="selected"<?php } ?>><?php echo $vo['name']; ?></option>
<?php $n++;}unset($n); ?>
</select>
    </span>
</li>
<li><input type="submit" value="查找" class="button button-green submit" /></li>
</ul>
</div>
</form>
<div class="table-head fn-clear"> 
    <div class="fn-l">
        <a href="<?php echo url('mobile/add');?>" class="button button-blue">
            <i class="iconfont icon-tianjia"></i> 单件预入库
        </a>
        <a href="<?php echo url('mobile/excel');?>" class="button button-green">
            <i class="iconfont icon-tianjia"></i> 批量预入库
        </a>
    </div>
</div>
<div class="m10">
<table class="table table-border">
<thead>
    <tr>
        <th class="fn-tac w140">跟踪号</th>
        <th class="fn-tac w100">门店号</th>
        <th class="fn-tac w100">IMEI号</th> 
        <th>产品名称</th>
        <th class="fn-tac w100">DHL快递号</th>
        <th>发件地址</th>
        <th class="fn-tac w110">邮寄时间</th>
        <th class="fn-tac w100">门店收货日期</th>
        <th class="fn-tac w100">门店受理日期</th>
        <th class="fn-tac w100">操作</th>
    </tr>
</thead>
<tbody>
<?php $n=1;if(is_array($list)) foreach($list AS $vo) { ?>
<tr>
    <td class="fn-tac"><a title="手机入库单详情" href="javascript:void(0);" url="<?php echo url('mobile/reads',array(id=>$vo['id']));?>" class="win"><?php echo $vo['number']; ?></a></td>
    <td class="fn-tac"><?php echo $vo['supplier_name']; ?></td>
    <td class="fn-tac"><?php echo $vo['imei']; ?></td>
    <td><?php echo $producttype_name[$vo['producttype_id']]; ?></td>
    <td class="fn-tac"><?php echo $vo['ems_sn']; ?></td>
    <td><?php echo $vo['come_address']; ?></td>
    <td class="fn-tac"><?php echo date('Y-m-d',$vo['ems_time']);?></td>
    <td class="fn-tac"><?php echo date('Y-m-d',$vo['ems_take_time']);?></td>
    <td class="fn-tac"><?php echo date('Y-m-d',$vo['accept_time']);?></td>
    <td class="fn-tac tip">
    <?php if($vo['package_id']) { ?>
    <a title="编辑" href="<?php echo url('package/edit',array('package_id'=>$vo['package_id'],id=>$vo['id']));?>">编辑</a> / 
    <?php } else { ?>
    <a title="编辑" href="<?php echo url('mobile/edit',array(id=>$vo['id']));?>">编辑</a> / 
    <?php } ?>
    <a href="javascript:void(0);" class="del" title="删除" url="<?php echo url('mobile/del',array(id=>$vo['id'],act=>del));?>">删除</a></td>
</tr>
<?php $n++;}unset($n); ?>
</tbody>
</table>
<div id="page" class="fn-clear"><?php echo $pager; ?></div>
</div>
<script type="text/javascript">
Do('base','form','date','dialog',function(){ 
  $('.del').click(function(){$(this).del();});
  $('.win').click(function(){$(this).win();});
  $('#form').forms({types:'post'});
})
</script>
";s:12:"compile_time";i:1509093314;}";