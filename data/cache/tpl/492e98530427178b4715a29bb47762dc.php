<?php exit;?>0015406056408c7021dd976eaebdf7133d3aa4a7ecbbs:5481:"a:2:{s:8:"template";s:5417:"<div class="path">
    <i class="iconfont icon-shouye1"></i>
    <a href="<?php echo url('admin/default/welcome');?>">开始</a> / 
    <a href="javascript:void(0);">包裹管理</a>
</div>
<form id="form" action="<?php echo url('package/index');?>" method="post">
<div class="table-search fn-clear">
<ul>

<li><select class="select" name="manufacturer_id">
    <option value=" ">请选择厂商</option>
<?php $n=1;if(is_array($manufacturer_lists)) foreach($manufacturer_lists AS $vo) { ?> 
    <option value="<?php echo $vo['id']; ?>" <?php if($manufacturer_id == $vo['id']) { ?> selected="selected"<?php } ?>><?php echo $vo['name']; ?></option>
<?php $n++;}unset($n); ?>
</select>
</li>
</select>
<li>
<select class="select" name="unicom_id">
    <option value=" ">请选择厂商运营商</option>
<?php $n=1;if(is_array($unicom_lists)) foreach($unicom_lists AS $vo) { ?> 
    <option value="<?php echo $vo['id']; ?>" <?php if($unicom_id == $vo['id']) { ?> selected="selected"<?php } ?>><?php echo $vo['name']; ?></option>
<?php $n++;}unset($n); ?>
</select>
</li>
<li>
    <span class="fn-tar">门店号:</span>
    <span class="sinput">
<select class="select" name="supplier_id">
    <option value="" selected="selected"></option>
<?php $n=1;if(is_array($supplier_lists)) foreach($supplier_lists AS $vo) { ?> 
    <option value="<?php echo $vo['id']; ?>" <?php if($supplier_id ==$vo['id']) { ?> selected="selected"<?php } ?>><?php echo $vo['name']; ?></option>
<?php $n++;}unset($n); ?>
</select>
    </span>
</li>
<li>
    <span class="fn-tar">跟踪号:</span>
    <span class="sinput"><input class="input w80" type="text" name="gprs_sn" value="<?php echo $gprs_sn; ?>" /></span>
</li>
<li>
    <span class="fn-tar">快递单号:</span>
    <span class="sinput"><input class="input w80" type="text" name="ems_sn" value="<?php echo $ems_sn; ?>" /></span>
</li>
<li>
    <span class="fn-tar">区域:</span>
    <span class="sinput"><input class="input w80" type="text" name="area" value="<?php echo $area; ?>" /></span>
</li>
<li>
    <span class="fn-tar">类型:</span>
    <span class="sinput">
        <select class="select" name="types_id">
            <option value="" selected="selected"></option>
            <option value="1" <?php if($types_id == 1) { ?> selected="selected"<?php } ?>>维修</option>
            <option value="2" <?php if($types_id == 2) { ?> selected="selected"<?php } ?>>换新</option>
        </select>
    </span>
</li>
<li>
    <span class="fn-tar">收货时间起:</span>
    <span class="sinput"><input class="input ui-date w80" type="text" name="ems_start_time" value="<?php echo $ems_start_time; ?>" /></span>
</li>
<li>
    <span class="fn-tar">收货时间止:</span>
    <span class="sinput"><input class="input ui-date w80" type="text" name="ems_end_time" value="<?php echo $ems_end_time; ?>" /></span>
</li>
<li><input type="submit" value="查找包裹" class="button button-green submit" /></li>
</ul>
</div>
</form>
<div class="table-head fn-clear"> 
    <div class="fn-l">
        <a href="<?php echo url('package/packageadd');?>" class="button button-blue">
            <i class="iconfont icon-tianjia"></i> 添加包裹
        </a>
    </div>
    <div class="fn-r">
        <a href="<?php echo url('package/excel',$search);?>" class="button">
            <i class="iconfont icon-xiazai"></i> 导出到表格
        </a>
    </div>    
</div>
<div class="m10">
<table class="table table-border">
<thead>
    <tr>
        <th class="fn-tac w100">门店号</th>
        <th class="fn-tac w100">门店追踪号</th>
        <th>厂商</th>
        <th>运营商</th>
        <th>快递名称</th>
        <th class="fn-tac w100">快递单号</th>
        <th class="fn-tac w50">类型</th>
        <th class="fn-tac w50">数量</th>
        <th class="fn-tac w120">收货时间</th>
        <th class="fn-tac w100">操作</th>
    </tr>
</thead>
<tbody>
<?php $n=1;if(is_array($list)) foreach($list AS $vo) { ?>
<tr>
    <td class="fn-tac"><?php echo $vo['company_number']; ?></td>
    <td class="fn-tac"><?php echo $vo['gprs_sn']; ?></td>
    <td><?php echo $manufacturer_name[$vo['manufacturer_id']]; ?></td>
    <td><?php echo $unicom_name[$vo['unicom_id']]; ?></td>
    <td><?php echo $vo['ems_name']; ?></td>
    <td class="fn-tac"><?php echo $vo['ems_sn']; ?></td>
    <td class="fn-tac"><?php if($vo['types_id']) { ?><span class="badge bg-green">换新</span><?php } else { ?><span class="badge bg-red">维修</span><?php } ?></td>
    <td class="fn-tac"><span class="badge bg-red"><?php echo $vo['counts']; ?></span></td>
    <td class="fn-tac"><?php echo date('Y-m-d',$vo['ems_time']);?></td>
    <td class="fn-tac tip">
        <a title="编辑" href="<?php echo url('package/packageedit',array(id=>$vo['id']));?>">编辑</a> / 
        <a href="javascript:void(0);" class="del" title="删除" url="<?php echo url('package/packagedel',array(id=>$vo['id'],act=>del));?>">删除</a>
    </td>
</tr>
<?php $n++;}unset($n); ?>
</tbody>
</table>
<div id="page" class="fn-clear"><?php echo $pager; ?></div>
</div>
<script type="text/javascript">
Do('base','form','date','dialog',function(){ 
  $('.del').click(function(){$(this).del();});
  $('.win').click(function(){$(this).win();});
  $('#form').forms({types:'post'});
})
</script>
";s:12:"compile_time";i:1509069640;}";