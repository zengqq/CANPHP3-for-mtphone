<?php exit;?>001533360348291c995a6fd572f903d5ca4a1e3a1e02s:5420:"a:2:{s:8:"template";s:5356:"<div class="path">
    <i class="iconfont icon-shouye1"></i>
    <a href="<?php echo url('admin/default/welcome');?>">开始</a> / 
    <a href="javascript:void(0);">收件入库</a>
</div>
<form id="form" action="<?php echo url('counts/index');?>" method="post">
<div class="table-search fn-clear">
    <ul>
        <li>
            <span class="fn-tar">入库时间:</span>
            <span class="sinput"><input class="input ui-time" type="text" name="start_time" value="<?php echo $start_time; ?>" /></span>
        </li>
        <li>
            <span class="fn-tar">出库时间:</span>
            <span class="sinput"><input class="input ui-time" type="text" name="end_time" value="<?php echo $end_time; ?>" /></span>
        </li>
        <li><input type="submit" value="开始查找手机" class="button button-green submit" /></li>
    </ul>
</div>
</form>
<div class="table-head fn-clear"> 
    <div class="fn-l">
    <a href="<?php echo url('repair/counts/index',array(start_time=>date('Y-m-d')));?>" class="button"> 
        今天
    </a>
    <a href="<?php echo url('repair/counts/index',array(start_time=>date('Y-m-d',time()-7*24*60*60)));?>" class="button">
         一周 
    </a>
    <a href="<?php echo url('repair/counts/index',array(start_time=>date('Y-m-d',time()-30*24*60*60)));?>" class="button"> 
        一月 
    </a>
    <a href="<?php echo url('repair/counts/index',array(start_time=>date('Y-m-d',time()-90*24*60*60)));?>" class="button">
        一季 
    </a>
    <a href="<?php echo url('repair/counts/index',array(start_time=>date('Y-m-d',time()-180*24*60*60)));?>" class="button">
       半年 
    </a>
    <a href="<?php echo url('repair/counts/index',array(start_time=>date('Y-m-d',time()-365*24*60*60)));?>" class="button">
       一年
    </a>
    </div>
</div>
<div class="m10">
    <?php
        $a = intval($counts[0])?intval($counts[0]):1;
        $b = intval($counts[1]);
        $b_i = round(($b/$a)*100);
        $c = intval($counts[2]);
        $c_i = round(($c/$a)*100);
        $d = intval($counts[3]);
        $d_i = round(($d/$a)*100);
        $e = intval($counts[4]);
        $e_i = round(($e/$a)*100);
        $f = intval($counts[5]);
        $f_i = round(($f/$a)*100);
        $g = intval($counts[6]);
        $g_i = round(($g/$a)*100); 
    ?>
<table class="table table-border">
    <tr>
        <th class="fn-tac w100">项目</th>
        <th class="fn-tac w60">数量</th>
        <th>百分比</td>
        <th class="fn-tac w40"></td>
    </tr>
    <tr>
        <td class="fn-tac w100">订单数</td>
        <td class="fn-tac w60"><?php echo $a; ?></td>
        <td>
            <div class="graph"><span class="color0" style="width:100%">&nbsp;</span></div>
        </td>
        <td class="fn-tac">100%</td>
    </tr>
    <tr>
        <td class="fn-tac w100">预入库</td>
        <td class="fn-tac w60"><?php echo $b; ?></td>
        <td>
            <div class="graph"><span class="color5" style="width:<?php echo $b_i; ?>%;">&nbsp;</span></div>
        </td>
        <td class="fn-tac"><?php echo $b_i; ?>%</td>
    </tr>
    <tr>
        <td class="fn-tac w100">已入库</td>
        <td class="fn-tac w60"><?php echo $c; ?></td>
        <td>
            <div class="graph"><span class="color1" style="width:<?php echo $c_i; ?>%;">&nbsp;</span></div>
        </td>
        <td class="fn-tac"><?php echo $c_i; ?>%</td>
    </tr>
    <tr>
        <td class="fn-tac w100">维修中</td>
        <td class="fn-tac w60"><?php echo $d; ?></td>
        <td>
            <div class="graph"><span class="color2" style="width:<?php echo $d_i; ?>%;">&nbsp;</span></div>
        </td>
        <td class="fn-tac"><?php echo $d_i; ?>%</td>
    </tr>
    <tr>
        <td class="fn-tac w100">质检中</td>
        <td class="fn-tac w60"><?php echo $e; ?></td>
        <td>
            <div class="graph"><span class="color3" style="width:<?php echo $e_i; ?>%;">&nbsp;</span></div>
        </td>
        <td class="fn-tac"><?php echo $e_i; ?>%</td>
    </tr>
    <tr>
        <td class="fn-tac w100">待出库</td>
        <td class="fn-tac w60"><?php echo $f; ?></td>
        <td>
            <div class="graph"><span class="color4" style="width:<?php echo $f_i; ?>%;">&nbsp;</span></div>
        </td>
        <td class="fn-tac"><?php echo $f_i; ?>%</td>
    </tr>
    <tr>
        <td class="fn-tac w100">已出库</td>
        <td class="fn-tac w60"><?php echo $g; ?></td>
        <td>
            <div class="graph"><span class="color5" style="width:<?php echo $g_i; ?>%;">&nbsp;</span></div>
        </td>
        <td class="fn-tac"><?php echo $g_i; ?>%</td>
    </tr>
</tbody>
</div>
<style type="text/css">
.graph{
    position:relative;
    font-size:13px;
    width:100%;
}
.color0,.color1, .color2, .color3, .color4, .color5{
    position:relative;
    text-align:left;
    color:#ffffff;
    height:18px;
    line-height:18px;
    font-family:Arial;
    display:block;
}
.graph .color0{background-color:#FF0000;}
.graph .color1{background-color:#669999;}
.graph .color2{background-color:#6699FF;}
.graph .color3{background-color:#FF9900;}
.graph .color4{background-color:#FF3333;}
.graph .color5{background-color:#990000;}
</style>

";s:12:"compile_time";i:1501824348;}";