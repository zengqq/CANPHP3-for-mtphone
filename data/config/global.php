<?php 
return array (
    'ENV' => 'development', //开发配置
	//'ENV' => 'production',  //线上配置
    //默认模板
    'DEFAULT_APP' => 'admin',
    'DEFAULT_CONTROLLER' => 'default',
    'DEFAULT_ACTION' => 'login',
    'ERROR_URL' => '', //出错跳转地址
    'URL_BASE' => '', //设置网址域名
    //模板设置
    'TPL'=>array(
        'TPL_DEPR' => '/',
    ),
);