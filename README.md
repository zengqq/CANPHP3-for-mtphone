# 功能简述


###  MPMS是Mobile Phone Manage System的缩写
##### MPMS是一个完整的多门店手机维修管理系统  

1. 仓库管理
1. BOM管理
1. 运营商管理
1. 门店管理
1. 换新管理
1. 手机商管理

完整的返修->收货->入库->维修->质检->发货的维修流程


# 安装需求


- 基于PHP+Myslq的手机维修管理系统
- 操作系统：win/mac/linux
- 运行环境：Apache/Nginx
- PHP版本：5.6 ~ 7.x
- Mysql版本：5.5+
- #Apache伪静态规则
- 需要自己导入数据库和配置链接

